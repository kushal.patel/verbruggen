<form role="search" method="get" id="searchform" class="woocommerce-product-search" action="<?php echo home_url('/'); ?>">
    <div>
        <input type="text" value="<?php the_search_query(); ?>" name="s" id="s" placeholder="Zoeken in ons assortiment">
        <input type="hidden" name="post_type" value="product">
        <i class="oval"></i>
    </div>
</form>