
<a href="#" class="scrollup" style="display: inline;"><i class="fa fa-angle-up"></i></a>
<footer>
	<div class="quick-contact">
    	<div class="container">
       	  <ul class="m-0 p-0">
              <li><strong>Verbruggen is op werkdagen bereikbaar van 08:30 tot 19:00</strong></li>
              <li><i class="icon-call"></i> <strong>Bel</strong> <font> 0485 451 220 </font></li>
              <li><i class="icon-mail"></i> <strong>Mail</strong> <a href="mailto:info@verbruggen-mill.nl">
          </ul>
        </div>
    </div>

    <div class="main-footer">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-7">
                	<div class="widget">
						<?php
						if(is_active_sidebar('footer-sidebar-1')){
							dynamic_sidebar('footer-sidebar-1');
						} ?>
                    </div>

                    <div class="widget">
						<?php
    					if(is_active_sidebar('footer-sidebar-2')){
    					   dynamic_sidebar('footer-sidebar-2');
    					} ?>
                    </div>

                    <div class="widget">
						<?php
						if(is_active_sidebar('footer-sidebar-3')){
							dynamic_sidebar('footer-sidebar-3');
						} ?>
                    </div>
                </div>

                <div class="col-md-5">
                	<div class="widget f-news">
                    	<h5>Nieuwsbrief</h5>
                        <p>Schrijf je in voor de wekelijkse niewsbrief</p>
                        <div class="subscribe-now">
                            <?php echo do_shortcode('[contact-form-7 id="494" title="newsletter"]'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container wide">
    	<div class="legal-links">
        	<ul class="list-inline">
            <li><a href="#">Terms and conditions</a></li>
            <li><a href="#">Legal</a></li>
            <li><a href="#">Pattents</a></li>
		</ul>
      </div>
  </div>
</footer>
<div id="cookies" style="display:none">
    <div class="container">
        <div class="cookie-box hidden-print">
            <div class="row">
                <div class="col-sm-9">
                    <h1>Verbruggen maakt gebruik van Cookies</h1>
                    <p>Om u zo goed mogelijk van dienst te kunnen zijn maakt onze website gebruik van cookies.
                        Voor meer informatie <a href="javascript:void()">klik hier</a>.</p>
                </div>
                <div class="col-sm-3 text-sm-right">
                    <a href="javascript:void(0)" onClick="acceptCookies()" onClick="acceptCookies()" class="btn btn-secondary">Akkoord</a>
                </div>
            </div>
        </div>
    </div>
</div>

<?php wp_footer(); ?>
<!--<script src="plugins/jquery/jquery.min.js" type="text/javascript"></script>
<script src="plugins/bootstrap/bootstrap.js" type="text/javascript"></script>

<script src="plugins/owl-slider/owl.js" type="text/javascript"></script>

<script type="text/javascript" src="plugins/isotope/isotope.min.js"></script>
<script src="plugins/custom.js" type="text/javascript"></script>

<script src="plugins/jquery.easing.min.js"></script>

<script src="plugins/sticky-menu.js"></script>



<script src="plugins/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>


<script src="plugins/select/selectize.min.js"></script>-->
<script>
jQuery('.select').selectize({
    create: true,
    sortField: 'text'
});
</script>

<script>
    /*jQuery(function() {
        jQuery( '#dl-menu' ).dlmenu();
    });*/
</script>

<!-- hide collapse -->
<script type="text/javascript">
    jQuery(document).click(function (event) {
        if (jQuery(event.target).parents(".navbar-collapse").length < 1) {
            var clickover = jQuery(event.target);
            var $navbar = jQuery(".navbar-collapse");
            var _opened = $navbar.hasClass("show");
            if (_opened === true && !clickover.hasClass("navbar-toggle")) {
                $navbar.collapse('hide');
            }
        }
    });
</script>


<script>
    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }
    document.getElementsByTagName("body")[0].classList.add("cookies-space");
    function acceptCookies() {
        document.getElementById("cookies").remove();
        document.getElementsByTagName("body")[0].classList.remove("cookies-space");
        setCookie("agreedToCookies",true,365);
    }
    if(document.cookie.indexOf("agreedToCookie")==-1) {
        document.getElementById("cookies").style.display="block";
    }else{
        document.getElementById("cookies").style.display="none";
    }
</script>
<style>
    rad3::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
        color: red;

    }
</style>

<script>
jQuery(document).ready(function(jQuery) {
    var Body = jQuery('body');
    Body.addClass('preloader-site');
    jQuery("input[name='s']").keyup(function () {
        var type = jQuery("input[name='s']").val();
        var noOfChr = type.length;
        console.info(noOfChr);
        if(noOfChr >= 1){
            jQuery('#serch_icon').css('display','none');
        }else{
            console.info('asd');
            jQuery('#serch_icon').css('display','block');
        }
    });
});
jQuery(window).load(function(){
    jQuery('.preloader-wrapper').fadeOut();
    jQuery('body').removeClass('preloader-site');
    jQuery('.aws-search-field').addClass('form-control');
    jQuery('.aws-search-field').addClass('serch-txt');
    jQuery('.form-control').removeClass('aws-search-field');
//    jQuery('.form-control').css('-webkit-text-fill-color','#7e868c');
//    jQuery("input[name='s']").css('-webkit-text-fill-color','rgb(177, 181, 183,1)');

    jQuery("input[name='s']").after('<i id="serch_icon" class="oval"></i>');
    jQuery("input[name='s']").css('font-weight','bold');
//    jQuery("input[name='s']").css('background-color','#fff');


    jQuery(".wpcf7-response-output").css('border','unset');
    jQuery(".wpcf7-response-output").css('color','white');
    jQuery(".wpcf7-response-output").css('margin-left','-29px');
    jQuery("#ft-news-submit").css('height','50px');
    if( screen.width <= 480 ) {
        jQuery("input[name='s']").attr("placeholder", "Zoeken");
    }
//    jQuery('#navbarDropdown').hover(function(){
//        console.info('ajskdhk');
//        jQuery('.dropdown-menu').addClass('show');
//        jQuery('.dropdown').addClass('show');
//    });
//    jQuery('#navbarDropdown').mouseout(function(){
//        jQuery('.dropdown-menu').removeClass('show');
//        jQuery('.dropdown').removeClass('show');
//    });
//
//    jQuery(document).click(function (event) {
//        jQuery('.dropdown-menu').removeClass('show');
//        jQuery('.dropdown').removeClass('show');
//    });
});
// wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control
// wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email input-style
// jQuery('input').removeClass('wpcf7-text');
</script>

<script>

    // initialize menu. Call this after menu markup:
    ddfullscreenmenu.init()

</script>
<!-- responsive tab -->
<script>
    jQuery('.nav-tabs-dropdown').each(function(i, elm) {

        jQuery(elm).text(jQuery(elm).next('ul').find('li.active a').text());

    });

    jQuery('.nav-tabs-dropdown').on('click', function(e) {

        e.preventDefault();

        jQuery(e.target).toggleClass('open').next('ul').slideToggle();

    });

    jQuery('#nav-tabs-wrapper a[data-toggle="tab"]').on('click', function(e) {

        e.preventDefault();

        jQuery(e.target).closest('ul').hide().prev('a').removeClass('open').text(jQuery(this).text());

    });
</script>

</body>
</html>
