<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Verbuggen</title>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" name="viewport">
<link rel="stylesheet" type="text/css" href="plugins/bootstrap/bootstrap.css">
<link rel="stylesheet" type="text/css" href="plugins/fonts/style.css">
<link rel="stylesheet" type="text/css" href="style.css">
<link rel="stylesheet" type="text/css" href="plugins/owl-slider/owl.css">
<link rel="stylesheet" type="text/css" href="css/animate.css">
<link rel="stylesheet" type="text/css" href="css/responsive.css">
<link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="plugins/select/selectize.css">
<link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
<link rel="stylesheet" href="plugins/scrollbar/jquery.mCustomScrollbar.css">
</head>

<body class="inner-page">
<?php include 'header.php';?>


<div class="blank-banner">
	
</div>
<!-- end banner -->

<section class="bg-white pb-4 pt-3">
	<div class="container">
    	<ol class="breadcrumb">
        	<li class="breadcrumb-item"><a href="product-category.php">Terung</a></li>
            <li class="breadcrumb-item"><a href="">Drank</a></li>
            <li class="breadcrumb-item"><a href="">Frisdrank</a></li>
            <li class="breadcrumb-item"><a href="">koolzuurhouden</a></li>
            <li class="breadcrumb-item active">blik</li>
        </ol>
    </div>
</section>

<section class="bg-white pad80 pb-5">
	<div class="container">
    	<div class="single-product-wrap">
        	<div class="row">
            	<div class="col-sm-5">
                	<div class="pro-slide-bl">
                    	<div class="pro-tag">OP =OP</div>	
                    	<figure class="owl-carousel product-gallery">
                       <div class="item">
                       		<img src="images/pro-sl-img.png" class="img-responsive"/>
                       </div> 
                       <div class="item">
                       		<img src="images/pro-sl-img.png" class="img-responsive"/>
                       </div>    	
                    </figure>
                    </div>
                </div>
                
                    <div class="col-sm-7">
                        <div class="product-summary">
                            <div class="summary">
                                <div class="title-area">
                                    <h1 class="sum-pro-title">D&G Old Jamaica <br />ginger beer</h1>
                                    <h5 class="sum-sub-title">Keuze per krat of per pallet</h5>
                                </div>
                                
                                <div class="product-status d-flex justify-content-between">
                                    <div class="krat-box">
                                    	<h3>Krat: 24 x 0,33cl blik </h3>
                                        <div class="count-input left">
                                            <a class="incr-btn" data-action="decrease" href="#"></a>
                                            <input class="quantity" type="text" name="quantity" value="1">
                                            <a class="incr-btn" data-action="increase" href="#"></a>
                                        </div>
                                    </div>
                                    
                                    <div class="krat-box text-sm-right">
                                    	<h3><strong>p/krat €4,99</strong></h3>
                                        <div class="btn-grp d-flex">
                                            <a href="" class="menu-bar"><i></i></a>
                                            <a href="" class="btn btn-primary btn-sm">Voeg toe</a>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="product-status d-flex justify-content-between">
                                    <div class="krat-box">
                                    	<h3>Krat: 24 x 0,33cl blik </h3>
                                        <div class="count-input left">
                                            <a class="incr-btn" data-action="decrease" href="#"></a>
                                            <input class="quantity" type="text" name="quantity" value="1">
                                            <a class="incr-btn" data-action="increase" href="#"></a>
                                        </div>
                                    </div>
                                    
                                    <div class="krat-box text-sm-right">
                                    	<h3><strong>p/krat €4,99</strong></h3>
                                        <div class="btn-grp d-flex">
                                            <a href="" class="menu-bar"><i></i></a>
                                            <a href="" class="btn btn-primary btn-sm">Voeg toe</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            <div class="single-product-info pad80">
            	<h1>Product informatie</h1>
                <h3>Dit product bevat de volgende allergenen:</h3>
                <ul class="list-pro-info clearfix">
                    <li><i class="icon-ingreient"></i><h3><span class="m-bold">Ingredienten:</span></h3></li>
                    <li><i class="icon-allergy"></i><h3><span class="m-bold">Allergenen: Bevat geen melk</span></h3></li>
                    <li><i class="icon-halal"></i><h3><span class="m-bold">Extra’s:</span> Halal product</h3>
                        <blockquote>Halal staat is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,</blockquote>
                    </li>
                </ul>
            </div>
            
            
            <div class="related-products">
            	<div class="section-heading mb-5">
                	<h1>Wij denken met u mee</h1>
                	<h3>Daarom maakt verbruggen een selectie van producten<br />
                	waarvan wij denken dat deze anasluit bij dit product</h3>
                </div>
                <figure class="owl-carousel rel-pro-gallery3">
                    
                   
                   <div class="product-block">
                    <div class="pro-img">
                        <a href=""><img src="images/products/product-2.png" class="img-responsive"></a>
                    </div>
                    <div class="product-info">
                        <h4 class="light"><a href="">Zwarte kip Advocaat</a></h4>
                        <h4 class="product-qty">0,75cl</h4>
                    </div>
                    <div class="product-title">
                        <h4><a href="">Zwarte kip</a></h4>
                    </div>
                    <div class="it-input-qt">
                        <div class="count-input">
                        <a class="incr-btn" data-action="decrease" href="#"></a>
                        <input class="quantity" type="text" name="quantity" value="1">
                        <a class="incr-btn" data-action="increase" href="#"></a>
                    </div>
                    </div>
                </div>
                   
                   <div class="product-block">
                    <div class="pro-img">
                        <a href=""><img src="images/products/product-2.png" class="img-responsive"></a>
                    </div>
                    <div class="product-info">
                        <h4 class="light"><a href="">Zwarte kip Advocaat</a></h4>
                        <h4 class="product-qty">0,75cl</h4>
                    </div>
                    <div class="product-title">
                        <h4><a href="">Zwarte kip</a></h4>
                    </div>
                    <div class="it-input-qt">
                        <div class="count-input">
                        <a class="incr-btn" data-action="decrease" href="#"></a>
                        <input class="quantity" type="text" name="quantity" value="1">
                        <a class="incr-btn" data-action="increase" href="#"></a>
                    </div>
                    </div>
                </div>    	
                
                    <div class="product-block">
                    <div class="pro-img">
                        <a href=""><img src="images/products/product-2.png" class="img-responsive"></a>
                    </div>
                    <div class="product-info">
                        <h4 class="light"><a href="">Zwarte kip Advocaat</a></h4>
                        <h4 class="product-qty">0,75cl</h4>
                    </div>
                    <div class="product-title">
                        <h4><a href="">Zwarte kip</a></h4>
                    </div>
                    <div class="it-input-qt">
                        <div class="count-input">
                        <a class="incr-btn" data-action="decrease" href="#"></a>
                        <input class="quantity" type="text" name="quantity" value="1">
                        <a class="incr-btn" data-action="increase" href="#"></a>
                    </div>
                    </div>
                </div>
                </figure>
                
            </div>    
            
         </div>
         
    </div>
    
</section>

<?php include 'footer.php';?>
