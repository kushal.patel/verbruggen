(function($){
    'use strict';


    // gallery
    jQuery(window).on('load',function() {
        //Isotope activation js codes
        var $gellary_img = $('.gallery-items').isotope({
          itemSelector: '.gallery-item',
          percentPosition: true,
          transitionDuration: '0.5s',
          masonry: {
            // use outer width of grid-sizer for columnWidth
            columnWidth: '.gallery-item',
            gutter:0
          },
          getSortData: {
            name: '.name',
            symbol: '.symbol',
            number: '.number parseInt',
            category: '[data-category]',
            weight: function( itemElem ) {
              var weight = $( itemElem ).find('.weight').text();
              return parseFloat( weight.replace( /[\(\)]/g, '') );
            }
          }
        });


        // filter functions
        var filterFns = {
          // show if number is greater than 50
          numberGreaterThan50: function() {
            var number = $(this).find('.number').text();
            return parseInt( number, 10 ) > 50;
          },
          // show if name ends with -ium
          ium: function() {
            var name = $(this).find('.name').text();
            return name.match( /ium$/ );
          }
        };

        // bind filter button click
        $('.gallery-menu').on( 'click', 'li', function() {
          var filterValue = $( this ).attr('data-filter');
          // use filterFn if matches value
          filterValue = filterFns[ filterValue ] || filterValue;
          $gellary_img.isotope({ filter: filterValue });
        });


        // change is-checked class on buttons
        $('.gallery-menu').each( function( i, liList ) {
          var $liList = $( liList );
          $liList.on( 'click', 'li ', function() {
            $liList.find('.active').removeClass('active');
            $( this ).addClass('active');
          });
        });
    });


    // header small //
    $(window).scroll(function() {
    var scroll = $(window).scrollTop();

    if (scroll >= 20) {
        $(".fixed-top").addClass("smallpad");
    } else {
        $(".fixed-top").removeClass("smallpad");
    }
});




    // scroll up
    $(document).ready(function () {

        /* for the cart overlay effects cart open */
        $(".nav-cart").on('click',function () {
            jQuery("#myNav").slideToggle( "slow",function () {
                $('body').css('overflow-y','hidden');
                $("#myNav").css("height","calc(100% - 133px)");
                $("#myNav").css("display","block");

                var wphomeurl = $("#wphomeurl").val();
                console.info(wphomeurl);
                $.ajax({
                    url : wphomeurl + '/wp-admin/admin-ajax.php',
                    method : 'post',
                    data : {
                        'action' : 'cart_overlay',
                    }
                }).done(function(response){
                    $("#mycontent").html(response);
                    $(".wc-backward").addClass("btn");
                    $(".wc-backward").addClass("btn-primary");
                    $(".wc-backward").removeClass("button");
                });
            });
        });

        /* for the cart overlay effects close cart */
        $(document).on('click','.close-cart-overlay', function () {
            // $('body').css('overflow-y','visible !important');
            $('body').removeAttr('style');
            $('body').css('overflow-y','scroll');
            $("#myNav").css("height","0%");
        });






        var orderid = $("#dyna_order_id").val();
        $("#dynamic_orderid").text(orderid);
        $('.edit-profile').hide();
        $('.edit-address').hide();

        var guestuserhtml =  $("#guestuserhtml").val();
        if(guestuserhtml == 'true'){
            var html = "<h1 class='heading1'>Nog geen klant ?</h1><h1 class='heading1'>Registreer u vandaag nog.</h1><br><br><h3 class='heading3'>Om gebruik te kunnen maken van de diensten van Verbruggen dient u geregistreert te staan als klant.</h3><h3 class='heading3'>Als klant van Verbruggen geniet u van een van de grootste en meest uitgebreiden assortimenten :</h3><br><ul style='color:red;'><li> - Altijd inzicht in de meeste actuele prijzen.</li><li> - Online bestellijsten aanmaken.</li><li> - Bestellingen op factuur.</li></ul><br><h4>Volg onderstaande stappen en registreer u vaandag nog als klant bij Verbruggen !</h4>";
            $("#myhtml").html(html);
        }


        function getCookie(cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for(var i = 0; i <ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }


        $(window).scroll(function () {
        if ($(this).scrollTop() > 500) {
            $('.scrollup').fadeIn();
        } else {
            $('.scrollup').fadeOut();
        }
    });

    $('.scrollup').click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });

    /* for the category sub category listing */
    $('.all').css('display','block');
    $('.cat_list_gallary').click(function(){
      // console.info($(this).data('value'));
      var cat = $(this).data('value');
      $('.gallery-items').css('display','none');
      if(cat == 'all'){
        $('.'+cat).css('display','none');
        $('.all').css('display','block');
      }else{
        $('.'+cat).css('display','block');
        $('.all').css('display','none');
      }
    });

     /* product slider with offer sections category wise */
    $('.all_prods').css('display','block');
    $('.offers_cat').click(function(){
      console.info($(this).data('value'));
      var cat = $(this).data('value');
      $('.tab-pane').css('display','none');
      $('.offers_cat').removeClass('active');
      $('.'+cat).addClass('active');
      if(cat == 'all_prods'){
        $('.offers_'+cat).css('display','none');
        $('.all_prods').css('display','block');
      }else{
        $('.offers_'+cat).css('display','block');
        $('.all_prods').css('display','none');
      }
    });

    // $("#navbarDropdown").click(function(){
    //     $("p").toggle();
    // });

    /* login functionality for the user section */

        $('.login-form').submit(function () {
            $('#div_error_login').css('display','none');
            var username = $('#username').val();
            var password = $('#password').val();
            if(username.length == 0 && password.length == 0){
                $('#username').css('border','1px solid #f35b3f');
                $('#password').css('border','1px solid #f35b3f');
                $('#div_error_login').css('display','block');
                $('#div_error_login').html('Gebruikersnaam en wachtwoord vereist');
                return false;
            }

            if(username.length == 0){
                $('#username').css('border','1px solid #f35b3f');
                return false;
            }else{
                $('#username').css('border','1px solid #32393E');
            }if(password.length == 0){
                $('#password').css('border','1px solid #f35b3f');
                return false;
            }else{
                $('#password').css('border','1px solid #32393E');
            }
            $.post(ajaxurl, { action: 'user_login', username: username,password:password }, function(output) {
                console.info(jQuery.parseJSON(output));
                var res = jQuery.parseJSON(output);
                if(res.token == 1){
                    window.location.href = res.url;
                    return false;
                }else if(res.token == 2){
                    $('#div_error_login').css('display','block');
                    $('#div_error_login').html('je account is niet geactiveerd');
                    return false;
                }else if(res.token == 0){
                    $('#div_error_login').css('display','block');
                    $('#div_error_login').html('Gebruikersnaam en wachtwoord komen niet overeen');
                    return false;
                }
                return false;
            });
            return false;
        });

        /* end login functionality */

        $('#resetPwd').submit(function () {
            var email =  $('#user_email').val();
            var username = $('#user_login').val();

            if(email.length == 0){
                if(username.length == 0){
                    $('#error_forgot').html('e-mailadres of gebruikersnaam vereist');
                    $('#error_forgot').css('display','block');
                    return false;
                }
            }if(username.length == 0){
                if(email.length == 0){
                    $('#error_forgot').html('e-mailadres of gebruikersnaam vereist');
                    $('#error_forgot').css('display','block');
                    return false;
                }
            }

            if(email.length != 0){
                console.log(email.length);
                var re = /^\w+([-+.'][^\s]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
                var emailFormat = re.test($("#user_email").val());// this return result in boolean type
                console.log(emailFormat);
                if(emailFormat  == false){
                    $('#user_email').css('border','1px solid #f35b3f');
                    return false;
                }
                // else{
                //     $('#user_email').css('border','1px solid #32393E');
                //     $.post(ajaxurl, { action: 'user_email_check', email: email}, function(output) {
                //         console.info(output);
                //         if(output == 'false'){
                //             $('#error_forgot').html('Gebruiker niet beschikbaar');
                //             $('#error_forgot').css('display','block');
                //             return false;
                //         }else{
                //             $('#error_forgot').css('display','none');
                //         }
                //         return false;
                //     });
                //     return false;
                // }
                if(emailFormat == true){
                    $.post(ajaxurl, { action: 'user_email_check', email: email}, function(output) {
                        if(output == 'false'){
                            $('#error_forgot').html('Gebruiker niet beschikbaar');
                            $('#error_forgot').css('display','block');
                            return false;
                        }
                    });
                }
            }

            if(username == '') {
                $('#user_login').css('color','white');
                var login = $('#user_login').val(email);
            }
        });

        /*for the reset password validations*/

        $('#password_2').keyup(function(e){
            //get values

            var pass = $('#password_1').val();
            var confpass = $(this).val();

            //check the strings
            if(pass == confpass){
                //if both are same remove the error and allow to submit
                $('.error').text('');

            }else{
                //if not matching show error and not allow to submit
                $('.error').text('Wachtwoord komt niet overeen');

            }
        });


        //jquery form submit
        $('#lost_pwd').submit(function(){

            var pass = $('#password_1').val();
            var confpass = $('#password_2').val();

            if(pass.length == 0 && confpass.length == 0){
                $('.error').text('Wachtwoord en wachtwoord bevestigen vereist');
                return false;
            }else{
                $('.error').text('');
            }
            //just to make sure once again during submit
            if(pass != confpass){
                return false;
            }

        });

        /*for the use oof design changes in related file uploads*/

        ;(function($) {

            // Browser supports HTML5 multiple file?
            var multipleSupport = typeof $('<input/>')[0].multiple !== 'undefined',
                isIE = /msie/i.test( navigator.userAgent );

            $.fn.customFile = function() {

                return this.each(function() {

                    var $file = $(this).addClass('custom-file-upload-hidden'), // the original file input
                        $wrap = $('<div class="file-upload-wrapper">'),
                        $input = $('<input type="text"  placeholder="Choose file" class="file-upload-input" />'),
                        // Button that will be used in non-IE browsers
                        $button = $('<button type="button" class="file-upload-button">Bestand kiezen</button>'),
                        // Hack for IE
                        $label = $('<label class="file-upload-button" for="'+ $file[0].id +'">Select a File</label>');

                    // Hide by shifting to the left so we
                    // can still trigger events
                    $file.css({
                        position: 'absolute',
                        left: '-9999px'
                    });

                    $wrap.insertAfter( $file )
                        .append( $file, $input, ( isIE ? $label : $button ) );

                    // Prevent focus
                    $file.attr('tabIndex', -1);
                    $button.attr('tabIndex', -1);

                    $button.click(function () {
                        $file.focus().click(); // Open dialog
                    });

                    $file.change(function() {

                        var files = [], fileArr, filename;

                        // If multiple is supported then extract
                        // all filenames from the file array
                        if ( multipleSupport ) {
                            fileArr = $file[0].files;
                            for ( var i = 0, len = fileArr.length; i < len; i++ ) {
                                files.push( fileArr[i].name );
                            }
                            filename = files.join(', ');

                            // If not supported then just take the value
                            // and remove the path to just show the filename
                        } else {
                            filename = $file.val().split('\\').pop();
                        }

                        $input.val( filename ) // Set the value
                            .attr('title', filename) // Show filename in title tootlip
                            .focus(); // Regain focus

                    });

                    $input.on({
                        blur: function() { $file.trigger('blur'); },
                        keydown: function( e ) {
                            if ( e.which === 13 ) { // Enter
                                if ( !isIE ) { $file.trigger('click'); }
                            } else if ( e.which === 8 || e.which === 46 ) { // Backspace & Del
                                // On some browsers the value is read-only
                                // with this trick we remove the old input and add
                                // a clean clone with all the original events attached
                                $file.replaceWith( $file = $file.clone( true ) );
                                $file.trigger('change');
                                $input.val('');
                            } else if ( e.which === 9 ){ // TAB
                                return;
                            } else { // All other keys
                                return false;
                            }
                        }
                    });

                });

            };

            // Old browser fallback
            if ( !multipleSupport ) {
                $( document ).on('change', 'input.customfile', function() {

                    var $this = $(this),
                        // Create a unique ID so we
                        // can attach the label to the input
                        uniqId = 'customfile_'+ (new Date()).getTime(),
                        $wrap = $this.parent(),

                        // Filter empty input
                        $inputs = $wrap.siblings().find('.file-upload-input')
                            .filter(function(){ return !this.value }),

                        $file = $('<input type="file" id="'+ uniqId +'" name="'+ $this.attr('name') +'"/>');

                    // 1ms timeout so it runs after all other events
                    // that modify the value have triggered
                    setTimeout(function() {
                        // Add a new input
                        if ( $this.val() ) {
                            // Check for empty fields to prevent
                            // creating new inputs when changing files
                            if ( !$inputs.length ) {
                                $wrap.after( $file );
                                $file.customFile();
                            }
                            // Remove and reorganize inputs
                        } else {
                            $inputs.parent().remove();
                            // Move the input so it's always last on the list
                            $wrap.appendTo( $wrap.parent() );
                            $wrap.find('input').focus();
                        }
                    }, 1);

                });
            }

        }(jQuery));

        $('input[type=file]').customFile();


    jQuery(".pro-tag").parent().parent().parent().addClass('aanbied');

    jQuery(".woocommerce-widget-layered-nav-list").addClass('list-sm mCustomScrollbar');

    jQuery("li.woocommerce-widget-layered-nav-list__item").each(function(){
        var html = jQuery(this).html();
        var mytitle = jQuery(this).find(" a ").text();
        mytitle = mytitle.toLowerCase();
        mytitle = mytitle.replace(" ","-");
        mytitle = mytitle.replace(".","-");
        var url      = window.location.href;     // Returns full URL (https://example.com/path/example.html)
        var substring = 'filter_brand=';
        var checked = '';

        if(url.includes(substring)){
            var filters = url.split('filter_brand=');

            if(filters[1].includes('&')){
                var newfilter = filters[1].split('&');
                newfilter = newfilter[0];
            }
            else{
                var newfilter = filters[1];
            }
            if(newfilter.includes(',')){
                filters = newfilter.split(',');
                jQuery.each( filters , function(index,value){
                    if(mytitle == value){
                        checked = 'checked';
                    }
                });
            }
            else{
                if(newfilter == mytitle){
                    checked = 'checked';
                }
            }
        }

        if(url.includes('filter_drink_type')){
            var filters = url.split('filter_drink_type=');

            if(filters[1].includes('&')){
                var newfilter = filters[1].split('&');
                newfilter = newfilter[0];
            }
            else{
                var newfilter = filters[1];
            }
            if(newfilter.includes(',')){
                filters = newfilter.split(',');
                jQuery.each( filters , function(index,value){
                    if(mytitle == value){
                        checked = 'checked';
                    }
                });
            }
            else{
                if(newfilter == mytitle){
                    checked = 'checked';
                }
            }
        }


        var newhtml = "<label class='radio-bl'>"+html+"<input type='checkbox' "+ checked +" ><span class='checkmark'></span></label>";
        jQuery(this).html(newhtml);
    });


        var relatedproductshtml = "<div class='section-heading mb-5'><h1>Wij denken met u mee</h1><h3>Daarom maakt verbruggen een selectie van producten<br />waarvan wij denken dat deze anasluit bij dit product</h3></div>";
        jQuery(".related.products h2:first-child").html(relatedproductshtml);



        $("h2.woocommerce-loop-product__title").each(function(){
           var oldhtml = $(this).html();
           var newhtml = "<div class='product-title'>"+oldhtml+"</div>";
           $(this).replaceWith(newhtml);
        });

        $("button.single_add_to_cart_button").removeClass("button");
        $("a.add_to_cart_button").removeClass("button");
        $("button.single_add_to_cart_button").addClass("btn btn-primary element-1");
        $("a.add_to_cart_button").addClass("btn btn-primary element-1");



        jQuery(".add_to_cart_button").hide();

        $(".woocommerce-notices-wrapper").hide();



    });


    // quantity //
    $(document).on("click", ".incr-btn", function (e) {
        var wphomeurl = $("#wphomeurl").val();
        var homepage = $("#homepage").val();
        var aanbiedingen = "";
        aanbiedingen = $("#aanbiedingen").val();

        var assortiment = "";
        assortiment = $("#assortiment").val();

        var $button = $(this);
        var oldValue = $button.parent().find('.quantity').val();
        $button.parent().find('.incr-btn[data-action="decrease"]').removeClass('inactive');
        var productid = "";
        if(homepage == 'false' && aanbiedingen != 'true' && assortiment != 'true'){
            productid = $button.parent().parent().parent().parent().find('.add_to_cart_button').attr('data-product_id');
        }

        if(productid == "" || productid == null){
            productid = $button.parent().find('.add_to_cart_button').attr('data-product_id');
        }

        var cart = $("#iscart").val();

        var variationid = $button.parent().find('.add_to_cart_button').attr('variationid');

        if ($button.data('action') == "increase") {

            var newVal = parseFloat(oldValue) + 1;

            $.ajax({
                url: wphomeurl + '/wp-admin/admin-ajax.php',
                method: 'post',
                data: {
                    'action': 'myajax',
                    'productid' : productid,
                    'quantity' : newVal,
                    'variationid' : variationid,
                }
            }).done( function (response) {
                if( response.error != 'undefined' && response.error ){
                    //some kind of error processing or just redirect to link
                    // might be a good idea to link to the single product page in case JS is disabled
                    return true;
                } else {
                    //window.location.href = SO_TEST_AJAX.checkout_url;
                    $.ajax({
                        url : wphomeurl + '/wp-admin/admin-ajax.php',
                        method : 'post',
                        data  :{
                            'action' : 'count_cart',
                        }
                    }).done(function(response){
                        var count = parseFloat(response)/10;
                        $(".menu-bar").css('display','block');
                        $("#cartcount").text(count );

                        // if(cart == 'true'){
                        //getting updated subtotal for individual product
                        var qty = newVal;
                        $.ajax({
                            url : wphomeurl + '/wp-admin/admin-ajax.php',
                            method : 'post',
                            data :{
                                'action' : 'getproductprice',
                                'productid' : productid
                            }
                        }).done(function(price){
                            // console.info(price);
                            if(variationid){
                                window.location.reload();
                            }
                            else{
                                var newprice = (parseFloat(price)) * parseFloat(qty);
                                console.log(newprice);
                                newprice = "&euro;" + newprice.toFixed(2);
                                $("h2#subtotal_"+productid).html(newprice);
                                // $("h2#mb_subtotal_"+productid).html(newprice);
                                console.info('sub-total');
                                console.info(newprice);
                            }
                        });

                        //getting total for cart
                        $.ajax({
                            url : wphomeurl + '/wp-admin/admin-ajax.php',
                            method : 'post',
                            data : {
                                'action' : 'getcarttotal',
                            }
                        }).done(function(total){
                            var total = parseFloat(total);
                            // $("#carttotal").text(total);
                            $("#carttotal").text(total);
                            console.info('total');
                            console.info(total);
                        });
                        // }

                    });
                    //alert("yeah product added check it out!");
                }
            });

        } else {

            // Don't allow decrementing below 1
            if (oldValue > 0) {
                var newVal = parseFloat(oldValue) - 1;

                $.ajax({
                    type: "POST",
                    url: wphomeurl + '/wp-admin/admin-ajax.php',
                    data: {action : 'remove_item_from_cart','product_id' : productid , 'quantity' : newVal,'variationid' : variationid},
                    success: function (res) {

                        $.ajax({
                            url : wphomeurl + '/wp-admin/admin-ajax.php',
                            method : 'post',
                            data  :{
                                'action' : 'count_cart',
                            }
                        }).done(function(response){
                            var count = parseFloat(response)/10;
                            $("#cartcount").text(count );


                            // if(cart == 'true'){
                            //getting updated subtotal for individual product
                            var qty = newVal;
                            $.ajax({
                                url : wphomeurl + '/wp-admin/admin-ajax.php',
                                method : 'post',
                                data :{
                                    'action' : 'getproductprice',
                                    'productid' : productid
                                }
                            }).done(function(price){
                                var newprice = (parseFloat(price)) * parseFloat(qty);
                                newprice = "&euro;" + newprice.toFixed(2);
                                console.info(newprice);
                                console.info(productid);
                                $("h2#subtotal_"+productid).html(newprice);
                                // $("h2#subtotal_"+productid).html(newprice);
                                console.info('sub-totals');
                                console.info(newprice);
                            });

                            //getting total for cart
                            $.ajax({
                                url : wphomeurl + '/wp-admin/admin-ajax.php',
                                method : 'post',
                                data : {
                                    'action' : 'getcarttotal',
                                }
                            }).done(function(total){
                                var total = parseFloat(total);
                                // $("#carttotal").text(total);
                                console.info('total');
                                console.info(total);
                                $("#carttotal").text(total);
                            });
                            // }

                        });

                        //alert('Cart updated !');
                    }
                });

            } else {
                newVal = 0;
                $button.addClass('inactive');
            }
        }


        $button.parent().find('.quantity').val(newVal);
        e.preventDefault();

    });


    $(".quantity").on("keyup",function(){
        var value = $(this).val();
        var wphomeurl = $("#wphomeurl").val();
        var productid = $(this).parent().parent().parent().parent().find('.add_to_cart_button').attr('data-product_id');
        var cart = $("#iscart").val();

        $.ajax({
            url: wphomeurl + '/wp-admin/admin-ajax.php',
            method: 'post',
            data: {
                'action': 'myajax',
                'productid' : productid,
                'quantity' : value,
            }
        }).done( function (response) {
            if( response.error != 'undefined' && response.error ){
                //some kind of error processing or just redirect to link
                // might be a good idea to link to the single product page in case JS is disabled
                return true;
            } else {
                //window.location.href = SO_TEST_AJAX.checkout_url;
                $.ajax({
                    url : wphomeurl + '/wp-admin/admin-ajax.php',
                    method : 'post',
                    data  :{
                        'action' : 'count_cart',
                    }
                }).done(function(response){
                    var count = parseFloat(response)/10;
                    $("#cartcount").text(count);

                    if(cart == 'true'){
                        //getting updated subtotal for individual product
                        var qty = value;
                        $.ajax({
                            url : wphomeurl + '/wp-admin/admin-ajax.php',
                            method : 'post',
                            data :{
                                'action' : 'getproductprice',
                                'productid' : productid
                            }
                        }).done(function(price){
                            var newprice = (parseFloat(price)) * parseFloat(qty);
                            newprice = "&euro;" + newprice.toFixed(2);
                            $("h2#subtotal_"+productid).html(newprice);
                            // $("h2#subtotal_"+productid).html(newprice);
                        });

                        //getting total for cart
                        $.ajax({
                            url : wphomeurl + '/wp-admin/admin-ajax.php',
                            method : 'post',
                            data : {
                                'action' : 'getcarttotal',
                            }
                        }).done(function(total){
                            var total = parseFloat(total);
                            $("#carttotal").text(total);
                        });
                    }

                });

                //alert("Cart updated!");
            }
        });


    });

    $("#edit-profile-detail").on("click",function(){
        $('#success_update').css('display','none');
        $('.non-edit-profile').hide();
        $('.edit-profile').show();
    });
    $("#save-profile-detail").on("click",function(){
        var name = $('#name').val();
        var email = $('#email').val();
        var telephone = $('#telephone').val();
        var userid = $('#userid').val();
        //validation custom for required fields.
        if(name.length == 0){
            $('#name').css('border','1px solid #f35b3f');
            return false;
        }else{
            $('#name').css('border','1px solid #32393E');
        }
        if(telephone.length == 0){
            $('#telephone').css('border','1px solid #f35b3f');
            return false;
        }else{
            $('#telephone').css('border','1px solid #32393E');
        }
        var filter = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
        if (filter.test(telephone)) {
        }else{
            $('#telephone').css('border','1px solid #f35b3f');
            return false;
        }
        if(email.length == 0){
            $('#email').css('border','1px solid #f35b3f');
            return false;
        }else{
            $('#email').css('border','1px solid #32393E');
            var re = /^\w+([-+.'][^\s]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;

            var emailFormat = re.test($("#email").val());// this return result in boolean type
            // console.log(emailFormat);
            if(emailFormat  == false){
                $('#email').css('border','1px solid #f35b3f');
                return false;
            }
            else {
                $.post(ajaxurl, { action: 'current_user_email_check', email: email, userid : userid}, function(output) {
                    console.info(output);
                    if(output == 'false'){
                        $('#email').css('border','1px solid #f35b3f');
                        $('#email_exist_error').css('display','block');
                        return false;
                    }
                    else {
                        var form_data = $(".profile-edit").serializeArray();
                        console.log(form_data);
                        $.post(ajaxurl, { action: 'save_profile', form_data: form_data }, function(output) {
                            if(output){
                                $('#success_update').css('display','block');
                                $('.edit-profile').hide();
                                $('#username').text(output.name);
                                $('#displayname').text(output.name);
                                $('#useremail').text(output.email);
                                $('#displaymail').text(output.email);
                                $('#userphone').text(output.phone);
                                $('#displayphone').text(output.phone);
                                $('.non-edit-profile').show();
                                $('#email_exist_error').css('display','none');
                            }
                        });
                    }
                });
            }
        }
    });


    $("#edit-address-detail").on("click",function(){
        $('#success_update').css('display','none');
        $('.non-edit-address').hide();
        $('.edit-address').show();
    });

    $("#save-address-detail").on("click",function(){
        $('.non-edit-address').hide();
        $('.edit-address').show();
        var form_data = $(".address-edit").serializeArray();
        $.post(ajaxurl, { action: 'save_address', form_data: form_data }, function(output) {
            if(output){
                $('#success_update').css('display','block');
                $('.edit-address').hide();
                $('#billing_address').text(output.billing_address);
                $('#billing_address_edit').text(output.billing_address);
                $('.non-edit-address').show();
            }
        });
    });


    $(document).on("click",".removeitem",function(){
       var $removeitem = $(this);
       var productid = $(this).attr('data-product-id');
       var variationid = $(this).attr('variationid');

        var wphomeurl = $("#wphomeurl").val();

        $.ajax({
            url : wphomeurl + '/wp-admin/admin-ajax.php',
            method : 'post',
            data : {
                'action' : 'removeitem',
                'productid' : productid,
                'variationid' : variationid,
            }
        }).done(function(total){
            var ismyaccount = $("#myaccountpage").val();
            if(ismyaccount == 'true'){
                $.ajax({
                    url : wphomeurl + '/wp-admin/admin-ajax.php',
                    method : 'post',
                    data  :{
                        'action' : 'count_cart',
                    }
                }).done(function(response){
                    var count = parseFloat(response)/10;
                    $("#cartcount").text(count);
                    $removeitem.parent().parent().remove();
                    $('#cart-' + productid).remove();
                });
            }
            else{
                window.location.reload();
            }
        });


    });


    //creating orde in woocommerce
    $("#createorder").on("click",function(){

        var wphomeurl = $("#wphomeurl").val();
        var isloggedin = $("#isloggedin").val();

        if(isloggedin == 'true'){
            $.ajax({
                url : wphomeurl + '/wp-admin/admin-ajax.php',
                method : 'post',
                data : {
                    'action' : 'create_order',
                }
            }).done(function(orderid){
                var thankyouurl = wphomeurl + '/thankyou';
                window.location.href = thankyouurl;
            });
        }
        else{
            $.ajax({
                url : wphomeurl + '/wp-admin/admin-ajax.php',
                method : 'post',
                data : {
                    'action' : 'guest_user_regi',
                }
            }).done(function(orderid){
                var registerurl = wphomeurl + '/registration';
                window.location.href = registerurl;
            });
        }
    });


    //creating orde in woocommerce from my-account page
    $(".place-order").on("click",function(){

        var wphomeurl = $("#wphomeurl").val();

        $.ajax({
            url : wphomeurl + '/wp-admin/admin-ajax.php',
            method : 'post',
            data : {
                'action' : 'create_order',
            }
        }).done(function(orderid){
            var thankyouurl = wphomeurl + '/thankyou';
            window.location.href = thankyouurl;
        });

    });


    // owl slider
    $(window).on('load', function (){
    $('.owl-carousel.product-slider').owlCarousel({
        loop:false,
        center:true,
        margin:10,
        nav:true,
        URLhashListener:true,
        autoplayHoverPause:true,
        startPosition: 1,
    responsive:{
       0:{
            items:1
        },
        550:{
            items:2
        },
        767:{
            items:3
        }
    }
})

    // customers slider
//     $('.product-gallery').owlCarousel({
//     autoplay: true,
//     margin:0,
//     smartSpeed:800,
//     nav:true,
//     responsive:{
//         0:{
//             items:1
//         }
//     }
// })
        // customers slider
        $('.product-gallery').owlCarousel({
            margin:0,
            smartSpeed:800,
            center:true,
            URLhashListener:true,
            startPosition: 'URLHash',
            nav:true,
            responsive:{
                0:{
                    items:1
                }
            }
        })


//     // customers slider
//     $('.rel-pro-gallery3').owlCarousel({
//     autoplay: true,
//     margin:0,
//     smartSpeed:800,
//     nav:true,
//     responsive:{
//         0:{
//             items:1
//         },
//         550:{
//             items:2
//         },
//         767:{
//             items:3
//         }
//     }
// })
        // customers slider
        $('.rel-pro-gallery3').owlCarousel({
            margin:0,
            smartSpeed:800,
            rtl: false,
            nav:true,
            responsive:{
                0:{
                    items:1,
                    margin:0
                },
                550:{
                    items:2
                },
                767:{
                    items:3
                },
                1200:{
                    items:4
                }
            }
        })


    // banner slider
    $('.owl-carousel.banner').owlCarousel({
      loop:true,
      autoplay: true,
      margin:0,
      smartSpeed: 800,
      nav:true,
      responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
      }
    })
  });



})(jQuery);


jQuery('document').on("ready",".ti-arrow-circle-right",function(){
    jQuery('.ti-arrow-circle-right').html(">");
});
