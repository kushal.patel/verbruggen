(function($){
	'use strict';
function scroll_to_class(element_class, removed_height) {
	var scroll_to = $(element_class).offset().top - removed_height;
	if($(window).scrollTop() != scroll_to) {
		$('html, body').stop().animate({scrollTop: scroll_to}, 0);
	}
}

function bar_progress(progress_line_object, direction) {
	var number_of_steps = progress_line_object.data('number-of-steps');
	var now_value = progress_line_object.data('now-value');
	var new_value = 0;
	if(direction == 'right') {
		new_value = now_value + ( 100 / number_of_steps );
	}
	else if(direction == 'left') {
		new_value = now_value - ( 100 / number_of_steps );
	}
	progress_line_object.attr('style', 'width: ' + new_value + '%;').data('now-value', new_value);
}

jQuery(document).ready(function($) {
	
    /*
        Fullscreen background
    */
    // $.backstretch("assets/img/backgrounds/1.jpg");

    $('#top-navbar-1').on('shown.bs.collapse', function(){
    	$.backstretch("resize");
    });
    $('#top-navbar-1').on('hidden.bs.collapse', function(){
    	$.backstretch("resize");
    });
    
    /*
        Form
    */
    $('.form-wizard fieldset:first').fadeIn('slow');

    $('.form-wizard input[type="text"], .form-wizard input[type="password"], .form-wizard textarea').on('focus', function() {
    	$(this).removeClass('input-error');
    });

	$('.bk-step-1').on('click', function() {
		$('.f3').css('display','none');
		$('.f2').css('display','none');
		$('.form-wizard fieldset:first').fadeIn('slow');
	});

    // next step
    $('.form-wizard .btn-next-step').on('click', function() {
		var myClass = $(this).attr('id');
		console.info(myClass);

    	var parent_fieldset = $(this).parents('fieldset');
    	var next_step = true;
    	// navigation steps / progress steps
    	var current_active_step = $(this).parents('.form-wizard').find('.form-wizard-step.active');
    	var progress_line = $(this).parents('.form-wizard').find('.form-wizard-progress-line');

		var name = $('#name').val();
		var email = $('#email').val();
		var telephone = $('#telephone').val();
		var company_number = $('#company_number').val();
		var vat = $('#vat').val();
		var bank_acc_no = $('#bank_acc_no').val();
		var bank_code = $('#bank_code').val();
		var eori = $('#eori').val();
		var company_name = $('#company_name').val();
		var street = $('#street').val();
		var house_no = $('#house_no').val();
		var addition = $('#addition').val();
		var postcode = $('#postcode').val();
		var city = $('#city').val();
		var landmark = $('#landmark').val();
		var shipping_street = $('#shipping_street').val();
		var shipping_house_no = $('#shipping_house_no').val();
		var shipping_addition = $('#shipping_addition').val();
		var shipping_postcode = $('#shipping_postcode').val();
		var shipping_city = $('#shipping_city').val();
		var shipping_landmark = $('#shipping_landmark').val();
		var industry = $('#industry').val();
		var message = $('textarea#message').val();

		//console.info(name.length);



		//for the last step display filled details.

		$('#user_name').html(name);
		$('#user_email').html(email);
		$('#user_telephone').html(telephone);
		$('#user_company_number').html(company_number);
		$('#user_btw').html(vat);
		$('#user_iban').html(bank_acc_no);
		$('#user_bic').html(bank_code);
		$('#user_eori').html(eori);
		$('#user_company_name').html(company_name);
		$('#user_address').html(street+' '+house_no+' '+addition+' '+postcode+' '+city+' '+landmark);
		//for the check address is same as or not
		if($("#keep-act").prop('checked') == true){
			$('#user_shipping_address').html(shipping_street+' '+shipping_house_no+' '+shipping_addition+' '+shipping_postcode+' '+shipping_city+' '+shipping_landmark);
		}else{
			$('#user_shipping_address').html(street+' '+house_no+' '+addition+' '+postcode+' '+city+' '+landmark);
		}
		$('#user_message').html(message);



		if(myClass == 'next-step-f2'){
			//validation custom for required fields.
			if(name.length == 0){
				$('#name').css('border','1px solid #f35b3f');
				next_step = false;
			}else{
				$('#name').css('border','1px solid #32393E');
			}if(email.length == 0){
				$('#email').css('border','1px solid #f35b3f');
				next_step = false;
			}else{
				$('#email').css('border','1px solid #32393E');

			}
			if(telephone.length == 0){
				$('#telephone').css('border','1px solid #f35b3f');
				next_step = false;
			}else{
				$('#telephone').css('border','1px solid #32393E');
			}


			var re = /^\w+([-+.'][^\s]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;

			var emailFormat = re.test($("#email").val());// this return result in boolean type
			// console.log(emailFormat);
			if(emailFormat  == false){
				$('#email').css('border','1px solid #f35b3f');
				return false;
				next_step = false;
			}

			if(emailFormat == true){
				console.info('ajax req');
				$.post(ajaxurl, { action: 'user_email_check', email: email}, function(output) {
					console.info('ajax response');
					if(output == 'true'){
						$('#email').css('border','1px solid #f35b3f');
						$('#email_error').css('display','block');
						$('.f2').css('display','none');
						$('.f1').css('display','block');
					}
				});
			}


			var filter = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
			if (filter.test(telephone)) {
				next_step = true;
			}else{
				$('#telephone').css('border','1px solid #f35b3f');
				next_step = false;
				return false;
			}
		}

    	// fields validation not working
    	parent_fieldset.find('input[type="text"],  textarea').on(function() {
    		if( $(this).val() == "" ) {
    			$(this).addClass('input-error');
    			next_step = false;
    		}
    		else {
    			$(this).removeClass('input-error');
    		}
    	});
    	// fields validation

    	if( next_step ) {
    		parent_fieldset.fadeOut(400, function() {
    			// change icons
    			current_active_step.removeClass('active').addClass('activated').next().addClass('active');
    			// progress bar
    			bar_progress(progress_line, 'right');
    			// show next step
	    		$(this).next().fadeIn();
	    		// scroll window to beginning of the form
    			scroll_to_class( $('.form-wizard'), 20 );
	    	});
    	}
    	
    });
    
    // previous step
    $('.form-wizard .btn-previous').on('click', function() {
    	// navigation steps / progress steps

    	var current_active_step = $(this).parents('.form-wizard').find('.form-wizard-step.active');
    	var progress_line = $(this).parents('.form-wizard').find('.form-wizard-progress-line');
    	console.info(current_active_step);
    	console.info(progress_line);
    	$(this).parents('fieldset').fadeOut(400, function() {
    		// change icons
    		current_active_step.removeClass('active').prev().removeClass('activated').addClass('active');
    		// progress bar
    		bar_progress(progress_line, 'left');
    		// show previous step
    		$(this).prev().fadeIn();
    		// scroll window to beginning of the form
			scroll_to_class( $('.form-wizard'), 20 );
    	});
    });
    
    // submit
    $('.form-wizard').on('submit', function(e) {
			console.info('submit');
		if($("#terms_cond").prop('checked') != true){
			console.info('not checked');
			$('#label_terms').css('color','#f35b3f');
			$('#span_terms_checkmark').css('border','1px solid #f35b3f');
			return false;
		}
        //
		var form_data = $(".form-wizard").serializeArray();
		// console.info(form_data);
		// for ajax method data save in database .
		$('.ajax-loader').css('display','block');
		$.post(ajaxurl, { action: 'create_new_user', form_data: form_data }, function(output) {
			$('#form_submit_loader').css('display','none');
			if(output){
				// $('.form-wizard').css('display','none');
				// $('#success_register').css('display','block');
				$('.ajax-loader').css('display','none');
				$( "#next-step-f4" ).click();
			}
		});

		return false;
    	// fields validation
    	// $(this).find('input[type="text"], input[type="password"], textarea').each(function() {
    	// 	if( $(this).val() == "" ) {
    	// 		e.preventDefault();
    	// 		$(this).addClass('input-error');
    	// 	}
    	// 	else {
        //
			// 	$(this).removeClass('input-error');
    	// 	}
    	// });
    	// fields validation
    	
    });
    
    
});
})(jQuery);