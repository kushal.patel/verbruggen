(function($){
    'use strict';

	
	// gallery 
	jQuery(window).on('load',function() {	
		//Isotope activation js codes
		var $gellary_img = $('.gallery-items').isotope({
		  itemSelector: '.gallery-item',
		  percentPosition: true,
		  transitionDuration: '0.5s',
		  masonry: {
			// use outer width of grid-sizer for columnWidth
			columnWidth: '.gallery-item',
			gutter:0
		  },
		  getSortData: {
			name: '.name',
			symbol: '.symbol',
			number: '.number parseInt',
			category: '[data-category]',
			weight: function( itemElem ) {
			  var weight = $( itemElem ).find('.weight').text();
			  return parseFloat( weight.replace( /[\(\)]/g, '') );
			}
		  }
		});
	

		// filter functions
		var filterFns = {
		  // show if number is greater than 50
		  numberGreaterThan50: function() {
			var number = $(this).find('.number').text();
			return parseInt( number, 10 ) > 50;
		  },
		  // show if name ends with -ium
		  ium: function() {
			var name = $(this).find('.name').text();
			return name.match( /ium$/ );
		  }
		};

		// bind filter button click
		$('.gallery-menu').on( 'click', 'li', function() {
		  var filterValue = $( this ).attr('data-filter');
		  // use filterFn if matches value
		  filterValue = filterFns[ filterValue ] || filterValue;
		  $gellary_img.isotope({ filter: filterValue });
		});


		// change is-checked class on buttons
		$('.gallery-menu').each( function( i, liList ) {
		  var $liList = $( liList );
		  $liList.on( 'click', 'li ', function() {
			$liList.find('.active').removeClass('active');
			$( this ).addClass('active');
		  });
		});	
	});
	
	
	// header small //
	$(window).scroll(function() {    
    var scroll = $(window).scrollTop();

    if (scroll >= 20) {
        $(".fixed-top").addClass("smallpad");
    } else {
        $(".fixed-top").removeClass("smallpad");
    }
});


	
	
	// scroll up
	$(document).ready(function () {

    $(window).scroll(function () {
        if ($(this).scrollTop() > 500) {
            $('.scrollup').fadeIn();
        } else {
            $('.scrollup').fadeOut();
        }
    });

    $('.scrollup').click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });

});
	
	
	// quantity //
	$(".incr-btn").on("click", function (e) {
        var $button = $(this);
        var oldValue = $button.parent().find('.quantity').val();
        $button.parent().find('.incr-btn[data-action="decrease"]').removeClass('inactive');
        if ($button.data('action') == "increase") {
            var newVal = parseFloat(oldValue) + 1;
        } else {
            // Don't allow decrementing below 1
            if (oldValue > 1) {
                var newVal = parseFloat(oldValue) - 1;
            } else {
                newVal = 1;
                $button.addClass('inactive');
            }
        }
        $button.parent().find('.quantity').val(newVal);
        e.preventDefault();
    });
	
	
	
	// owl slider 
	$(window).on('load', function (){
	$('.owl-carousel.product-slider').owlCarousel({
        loop:false,
        center:true,
		nav:true,
        margin:10,
        URLhashListener:true,
        autoplayHoverPause:true,
        startPosition: 'URLHash',
    responsive:{
       0:{
            items:1,
			margin:0
        },
        550:{
            items:2
        },
        767:{
            items:3
        }
    }
})
	
	// customers slider
	$('.product-gallery').owlCarousel({
    margin:0,
	smartSpeed:800,
	center:true,
	URLhashListener:true,
	 startPosition: 'URLHash',
    nav:true,
    responsive:{
        0:{
            items:1
        }
    }
})
	
	
	// customers slider
	$('.rel-pro-gallery3').owlCarousel({
    margin:0,
	smartSpeed:800,
	rtl: false,
    nav:true,
    responsive:{
        0:{
            items:1,
			margin:0
        },
        550:{
            items:2
        },
        767:{
            items:3
        },
		1200:{
            items:4
        }
    }
})
	
	
	
	// banner slider
	$('.owl-carousel.banner').owlCarousel({
    loop:true,
	autoplay: true,
    margin:0,
	smartSpeed: 800,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
})
});

})(jQuery);	  