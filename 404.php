<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WordPress
 * @subpackage verbruggen
 * @since 1.0.0
 */
wp_head();
get_header();
?>
	<script>
		jQuery(window).on('load', function () {
			jQuery('body').addClass('inner-page');
		});
	</script>
	<section id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="blank-banner">

			</div>
			<div class="error-404 not-found">
				<section class="bg-white pad80 pt-0">
					<div class="container">
						<div class=" pad-3"><h1 style="font-weight: 100;"><?php _e( 'Oops! Die pagina kan niet gevonden worden.', 'twentynineteen' ); ?></h1></div>
						<p><?php _e( 'Het lijkt erop dat er niets is gevonden op deze locatie. Misschien een zoekopdracht proberen?', 'verbruggen' ); ?></p>
					</div>
				</section>
				<h1 class="page-title"></h1>
				<div class="page-content">

				</div><!-- .page-content -->
			</div><!-- .error-404 -->

		</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_footer();
