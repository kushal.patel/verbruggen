<?php
/**
 * Created by PhpStorm.
 * User: Gopal
 * Date: 3/14/2019
 * Time: 3:16 PM
 * The template for displaying the homepage.
 *
 * This page template will display any functions hooked into the `homepage` action.
 * By default this includes a variety of product displays and the page content itself. To change the order or toggle these components
 * use the Homepage Control plugin.
 * https://wordpress.org/plugins/homepage-control/
 *
 * Template name: Template-Werken-bij-verbruggen
 *
 * @package storefront
 */

    wp_head();
    get_header();

    $image = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()),'full' , true);
    if(empty($image)){
    $image[0] = '';
    }
    ?>

    <title><?php echo get_bloginfo('name'); ?> - <?php echo the_title(); ?></title>
    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
            <div class="inner-banner-job" style="background-image:url(<?php echo $image[0]; ?>)">
                <div class="container">
                    <div class="banner-title">
                        <h1 class="text-white heading1 mb-4">Kom jij bij ons <br />werken?</h1>
                        <a href="<?php echo home_url('jobs'); ?>" class="btn btn-primary btn-lg">Bekijk alle vacatures</a>
                    </div>
                </div>
            </div>
            <?php
            /**
             * Functions hooked in to homepage action
             *
             * @hooked storefront_homepage_content      - 10
             * @hooked storefront_product_categories    - 20
             * @hooked storefront_recent_products       - 30
             * @hooked storefront_featured_products     - 40
             * @hooked storefront_popular_products      - 50
             * @hooked storefront_on_sale_products      - 60
             * @hooked storefront_best_selling_products - 70
             */
            // do_action( 'homepage' );

            while ( have_posts() ) :
                the_post();

                the_content();

            endwhile; // End of the loop.
            ?>

        </main><!-- #main -->
    </div><!-- #primary -->

<?php
get_footer();
?>
<style>
    .navbar-nav > li > a {
        color:#fff !important;
    }
</style>
