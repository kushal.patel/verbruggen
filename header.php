
<!-- preloader -->
<!--<div class="preloader-wrapper">-->
<!--    <div class="preloader">-->
<!--        <div class="lds-dual-ring"></div>-->
<!--    </div>-->
<!--</div>-->

<div class="preloader-wrapper">
    <div class="lds-css ng-scope"><div class="lds-spinner"><img class="galleryImg" src="https://verbruggen.primeprojects.be/wp-content/uploads/2019/04/loader.svg"></div></div>
</div>

<?php
    if(is_front_page()){?>
        <input type="hidden" value="true" id="homepage" />
    <?php }
    else { ?>
        <input type="hidden" value="false" id="homepage" />
    <?php }
?>
<?php
    if(is_user_logged_in()){ ?>
        <input type="hidden" value="true" id="isloggedin" />
    <?php } else{ ?>
        <input type="hidden" value="false" id="isloggedin" />
        <input type="hidden" value="<?php if(isset($_COOKIE['guestuserhtml'])){ echo $_COOKIE['guestuserhtml']; $_COOKIE['guestuserhtml'] = ''; } else{ echo ""; }  ?>" id="guestuserhtml" />
    <?php }
?>
<input type="hidden" value="<?php echo get_site_url(); ?>" id="wphomeurl" />

<input type="hidden" value="<?php if(is_cart()){ echo "true"; }else { echo "false"; } ?>" id="iscart" />


<input type="hidden" value="<?php if(isset($_COOKIE['orderid'])){ echo $_COOKIE['orderid']; unset($_COOKIE['orderid']); } else{ echo ""; }  ?>" id="dyna_order_id" />

<?php

function count_item_in_cart() {
    $count = 0;
    foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
        $count += $cart_item['quantity'];
    }
    return $count;
}

function get_nav_menu_items_by_location( $location, $args = [] ) {

    // Get all locations
    $locations = get_nav_menu_locations();

    // Get object id by location
    $object = wp_get_nav_menu_object( $locations[$location] );

    // Get menu items by menu name
    $menu_items = wp_get_nav_menu_items( $object->name, $args );

    // Return menu post objects
    return $menu_items;
}


$menuitems = get_nav_menu_items_by_location('primary');

?>

<?php
$mainparents = array();
foreach($menuitems as $key=>$menuitem){
    if($menuitem->menu_item_parent == 0){
        array_push($mainparents,$menuitem);
    }
}
/*echo "<Pre>";
print_r($mainparents);die;*/

$subchilds1 = array();
foreach($menuitems as $key=>$menuitem){
    if($menuitem->menu_item_parent != 0){
        foreach($menuitems as $key=>$mitem){
            if($mitem->ID == $menuitem->menu_item_parent && $mitem->menu_item_parent == 0){
                array_push($subchilds1,$menuitem);
            }
        }
    }
}

//echo "<Pre>";
//print_r($subchilds1);die;

$subchilds2 = array();
foreach($menuitems as $key=>$menuitem){
    if($menuitem->menu_item_parent != 0){
        foreach($subchilds1 as $key=>$sitem){
            if($sitem->ID == $menuitem->menu_item_parent){
                array_push($subchilds2,$menuitem);
            }
        }
    }
}



?>

<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri().'/images/favicon.png'; ?>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" name="viewport" />
<header class="fixed-top" data-spy="affix" data-offset-top="50">
    <div class="container">
        <nav class="navbar main-navbar-left ">
            <a href="<?php echo get_site_url(); ?>" class="logo"><img src="<?php echo get_theme_mod('verbruggen_logo'); ?>" class="img-responsive"/></a>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav d-block">
                    <?php
                    foreach($mainparents as $key=>$mainparent){
                        $class = '';
                        $submenu = false;
                        foreach($subchilds1 as $key=>$sitem){
                            if($mainparent->ID == $sitem->menu_item_parent){
                                $class = "nav-item dropdown mega-menu-li";
                                $submenu = true;
                            }
                        } ?>
                        <li class="<?php echo $class; ?>">
                            <?php
                            if($submenu == true){ ?>
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <?php echo $mainparent->title; ?>
                                </a>
                                <div class="dropdown-menu mega-menu-block" aria-labelledby="navbarDropdown">
                                    <ul class="row">
                                        <?php
                                        foreach($subchilds1 as $key=>$sitem){
                                            $ssubmenu = false;
                                            if($mainparent->ID == $sitem->menu_item_parent){
                                                foreach($subchilds2 as $key=>$ssitem){
                                                    if($ssitem->menu_item_parent == $sitem->ID){
                                                        $ssubmenu = true;
                                                    }
                                                }

                                                if($ssubmenu == true){ ?>

                                                    <li class="col-md-4">
                                                        <ul>
                                                            <li class="dropdown-header">
                                                                <h4><?php echo $sitem->title; ?></h4>
                                                            </li>
                                                            <?php
                                                            foreach($subchilds2 as $key=>$ssitem){
                                                                if($ssitem->menu_item_parent == $sitem->ID){ ?>
                                                                    <li><a href="<?php echo $ssitem->url; ?>"><?php echo $ssitem->title; ?></a></li>
                                                                <?php }
                                                            }
                                                            ?>
                                                        </ul>
                                                    </li>

                                                <?php }
                                                else{ ?>
                                                    <li class="col-md-4">
                                                        <ul>
                                                            <li class="dropdown-header">
                                                                <a href="<?php echo $sitem->url; ?>"><?php echo $sitem->title; ?></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                <?php }
                                            }
                                        }
                                        ?>
                                    </ul>
                                </div>
                            <?php } else{ ?>
                                <a  href="<?php echo $mainparent->url; ?>"><?php echo $mainparent->title; ?></a>
                            <?php }
                            ?>
                        </li>
                    <?php }
                    ?>
                    <li>
                        <div class="search-box d-flex">
                            <div class="dropdown mega-menu-li">
                                <!--data-toggle="dropdown"-->
<!--                                <a class="btn btn-primary dropdown-toggle" href="--><?php //echo home_url().'/assortiment'; ?><!--" id="navbarDropdown" data-toggle="dropdown" role="button"  aria-haspopup="true" aria-expanded="false" >assortiment</a>-->
                                <a class="btn btn-primary dropdown-toggle" href="<?php echo home_url().'/assortiment'; ?>" id="navbarDropdown"  role="button"   aria-haspopup="true" aria-expanded="false" >assortiment</a>
                                <div class="dropdown-menu mega-menu-block" id aria-labelledby="navbarDropdown">
                                    <div class="main-dropdown">
                                    	<ul class="row">
                                        <?php
                                        $args = array(
                                            'number'     => 10,
                                            'orderby'    => 'title',
                                            'orderby'    => 'name',
                                            'order'      => 'ASC',
                                            'hide_empty' => $hide_empty,
                                            'include'    => $ids,
                                            'parent'         => 0
                                        );
                                        $product_categories = get_terms( 'product_cat', $args );
                                        $mainCat = [];
                                        foreach ($product_categories as $key => $category) {
                                            if($category->slug != 'uncategorized' && $category->name != 'Accessories' && $category->name != 'Hoodies' && $category->name != 'Tshirts'){
                                                $args = array(
                                                    'number'     => 5,
                                                    'orderby'    => 'title',
                                                    'order'      => 'ASC',
                                                    'hide_empty' => $hide_empty,
                                                    'include'    => $ids,
                                                    'parent'         => $category->term_id
                                                );
                                                $subcategories = get_terms( 'product_cat', $args );
                                                array_push($mainCat,array('category_id'=>$category->term_id,'category_name'=>$category->name,'slug'=>$category->slug,'subcategory'=>$subcategories));
                                            }
                                        }
                                        foreach ($mainCat as $key => $category){

                                            ?>
                                            <li class="col-md-4" style="margin-bottom: -25px;">
                                                <ul>
                                                    <li class="dropdown-header">
                                                        <a style="border-bottom: unset;" href="<?php echo home_url('product-category/'.$category['slug']) ?>" ><h4 style="font-weight: bold;"><?php echo $category['category_name']; ?></h4></a></li>
                                                    <?php foreach ($category['subcategory'] as $key => $subcat) {
                                                        $url = home_url('product-category/'.$subcat->slug);
                                                        ?>
                                                        <li class="dr-sub-menu"><a href="<?php echo $url;?>"><?php echo $subcat->name; ?></a></li>
                                                    <?php } ?>
                                                </ul>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="search">
                                <?php get_search_form(); ?>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>

            <div class="user-panel-right d-flex">
                <?php
                if(is_user_logged_in()){
                    $user = wp_get_current_user();
                    ?>
                    <a href="<?php echo get_site_url()."/my-account"; ?>" class="btn btn-default btn-user hide-mobe no-border-class-red"><span><?php echo $user->user_nicename; ?></span></a>
<!--                    <a href="--><?php //echo get_site_url()."/cart"; ?><!--" class="menu-bar no-border-class"><span id="cartcount">--><?php //echo count_item_in_cart(); ?><!--</span><i></i></a>-->
                    <a href="javascript:void(0)" class="menu-bar no-border-class nav-cart"><span id="cartcount"><?php echo count_item_in_cart(); ?></span><i></i></a>
                <?php }else{ ?>
                    <a href="<?php echo home_url('login'); ?>" class="btn btn-default btn-user hide-mobe no-border-class-red"><span>Inloggen</span></a>
                    <a href="<?php echo home_url('registration'); ?>" class="btn btn-primary btn-user no-border-class"><span>Klant worden</span></a>
                    <?php if(count_item_in_cart() >= 1){
                        $display = "block";
                     }
                     else{
                         $display = "block";
//                         $display = "none";
                     }
                     ?>
<!--                    <a href="--><?php //echo get_site_url()."/cart"; ?><!--" class="menu-bar" style="margin-left: 10px;display:--><?php //echo $display; ?><!--"><span id="cartcount">--><?php //echo count_item_in_cart(); ?><!--</span><i></i></a>-->
                    <a href="javascript:void(0)" class="menu-bar nav-cart" style="display:<?php echo $display; ?>"><span id="cartcount"><?php echo count_item_in_cart(); ?></span><i></i></a>
                <?php } ?>
                <!-- The overlay -->
                <div id="myNav" class="overlay">

                    <!-- Button to close the overlay navigation -->
<!--                    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>-->

                    <!-- Overlay content -->
                    
                    <div class="overlay-content" id="mycontent" style="height:70vh;position: unset;">

                        <?php echo do_shortcode( '[woocommerce_cart]' );?>
                    </div>

                </div>

                <!-- Use any element to open/show the overlay navigation menu -->
<!--                <button onclick="openNav()">open</button>-->
<!--                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">-->
<!--                    <span class="navbar-toggler-icon"></span>-->
<!--                </button>-->
                <a id="navtoggler" onClick="ddfullscreenmenu.togglemenu()" class="m-menu" >
                    <span class="menu-toggler-icon"></span>
                </a>
            </div>
        </nav>
        <nav id="ddfullscreenmenu">
            <div class="mobile-header">
                <a href="<?php echo get_site_url(); ?>" class="logo-mobile"></a>
                <a href="javascript:void()" id="closex" class="close-menu"></a>
            </div>
            <div id="ulwrapper">
                <!-- Define your UL (nested) menu below -->
                <ul id="fullscreenmenu-ul">
                	<li class="mob-search">
                        <div class="search">
<!--                            <input type="text" placeholder="Zoeken" class="form-control"><i class="oval"></i>-->
                            <?php get_search_form(); ?>
                        </div>
                    </li>
                    <li class="mobile-assort"><a href="#">Assortiment</a>
                        <ul id="fullscreenmenu-ul">
                            <?php foreach ($mainCat as $key => $category) {?>
                                <li><a href="<?php echo home_url('product-category/'.$category['slug']) ?>"><h4><?php echo $category['category_name']; ?></h4></a>
                                <ul>
                                    <li><span class="sub-title"><?php echo $category['category_name']; ?></span></li>
                                <?php foreach ($category['subcategory'] as $key => $subcat) {
                                        $url = home_url('product-category/'.$subcat->slug);
                                        ?>
                                        <li><a href="<?php echo $url;?>"><?php echo $subcat->name; ?></a></li>
                                    <?php } ?>
                                    <li class="sub-title-li"><a href="<?php echo get_site_url().'/assortiment'; ?>"><span class="sub-title"><?php echo $category['category_name']; ?> assortiment</span></a></li>
                                </ul></li>
                            <?php } ?>
                            <li class="sub-title-li"><a href="<?php echo get_site_url().'/assortiment'; ?>"><span class="sub-title">Volledig assortiment</span></a></li>
                        </ul>
                    </li>
                    <?php
                    foreach($mainparents as $key=>$mainparent){
                        $class = '';
                        $submenu = false;
                        foreach($subchilds1 as $key=>$sitem){
                            if($mainparent->ID == $sitem->menu_item_parent){
                                $class = "nav-item dropdown mega-menu-li";
                                $submenu = true;
                            }
                        } ?>
                        <li class="mob-aanbiend">
                            <?php
                            if($submenu == true){ ?>
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <?php echo $mainparent->title; ?>
                                </a>
                                <div class="dropdown-menu mega-menu-block" aria-labelledby="navbarDropdown">
                                    <ul class="row">
                                        <?php
                                        foreach($subchilds1 as $key=>$sitem){
                                            $ssubmenu = false;
                                            if($mainparent->ID == $sitem->menu_item_parent){
                                                foreach($subchilds2 as $key=>$ssitem){
                                                    if($ssitem->menu_item_parent == $sitem->ID){
                                                        $ssubmenu = true;
                                                    }
                                                }

                                                if($ssubmenu == true){ ?>

                                                    <li class="col-md-4">
                                                        <ul>
                                                            <li class="dropdown-header">
                                                                <h4><?php echo $sitem->title; ?></h4>
                                                            </li>
                                                            <?php
                                                            foreach($subchilds2 as $key=>$ssitem){
                                                                if($ssitem->menu_item_parent == $sitem->ID){ ?>
                                                                    <li><a href="<?php echo $ssitem->url; ?>"><?php echo $ssitem->title; ?></a></li>
                                                                <?php }
                                                            }
                                                            ?>
                                                        </ul>
                                                    </li>

                                                <?php }
                                                else{ ?>
                                                    <li class="col-md-4">
                                                        <ul>
                                                            <li class="dropdown-header">
                                                                <a href="<?php echo $sitem->url; ?>"><?php echo $sitem->title; ?></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                <?php }
                                            }
                                        }
                                        ?>
                                    </ul>
                                </div>
                            <?php } else{ ?>
                                <a  href="<?php echo $mainparent->url; ?>"><?php echo $mainparent->title; ?></a>
                            <?php }
                            ?>
                        </li>
                    <?php }
                    ?>
                    

                    
                    <?php
                    if(is_user_logged_in()){
                        $user = wp_get_current_user();
                        ?>
                        <li class="mob-login"><a href="<?php echo get_site_url()."/my-account"; ?>" class="login"><?php echo $user->user_nicename; ?></a></li>
                    <?php }else{ ?>
                        <li class="mob-login"><a href="<?php echo home_url('login'); ?>" class="login">Inloggen</a></li>
                    <?php } ?>
                    <li class="mob-contact">
                        <a href="<?php echo home_url('contact'); ?>" class="contact">Contact</a>
                        <div class="mob-content">
                            <!--<span>Bel of mail ons:</span>-->
                            <a class="box" href="tel:0485 451 220"><font>0485 451 220</font></a>
                            <a class="box" href="mailto:info@verbruggen-mill.nl">info@verbruggen-mill.nl</a>
                        </div>
                    </li>


                </ul>
            </div>
        </nav>
    </div>
</header>
<!--- end header-->
<script>
    jQuery(document).ready(function () {
        jQuery(".wc-backward").addClass("btn");
        jQuery(".wc-backward").addClass("btn-primary");
        jQuery(".wc-backward").removeClass("button");
        jQuery("#myNav").hide();
        document.getElementById("myNav").style.height = "0%";
    });
</script>



