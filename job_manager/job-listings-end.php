<?php
/**
 * Content shown after job listings in `[jobs]` shortcode.
 *
 * This template can be overridden by copying it to verbruggen/job_manager/job-listings-end.php.
 *
 * @see         https://wpjobmanager.com/document/template-overrides/
 * @author      Automattic
 * @package     WP Job Manager
 * @category    Template
 * @version     1.15.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>

</ul>
</div>
</section>
<script>
	jQuery(document).ready(function(){
		jQuery('.bg-white').removeClass('pad80');

		/* for the job types and search display none */
		jQuery('.search_jobs').css('display','none');
		jQuery('.job_types').css('display','none');

		/* for the blank jobs */
		jQuery('.load_more_jobs').html('Meer vacatures');

		/* for jobs filter changes hide show */
		jQuery(".radio-bl").click(function(){
			var radioValue = jQuery("input[name='job_type']:checked").val();
			var count = jQuery('.radio_'+radioValue).attr("data-id");
			if(count == 0){
				jQuery('.noJobs').remove();
				jQuery('.no_jobs').append('<div class="noJobs" ><center>Geen vacatures beschikbaar ... !!!</center></div>');
			}else{
				jQuery('.noJobs').remove();
			}
			if(radioValue == 'allemaal'){
				jQuery('.all').css('display','block');
			}else{
				jQuery('.all').css('display','none');
				jQuery('.'+radioValue).css('display','block');
			}
		});



	});












</script>