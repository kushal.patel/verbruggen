<?php
/**
 * Job listing in the loop.
 *
 * This template can be overridden by copying it to verbruggen/job_manager/content-job_listing.php.
 *
 * @see         https://wpjobmanager.com/document/template-overrides/
 * @author      Automattic
 * @package     WP Job Manager
 * @category    Template
 * @since       1.0.0
 * @version     1.27.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

global $post;
?>



<div class="col-md-12 all <?php if ( get_option( 'job_manager_enable_types' ) ) {  $types = wpjm_get_the_job_types(); if ( ! empty( $types ) ) : foreach ( $types as $type ) : ?>
					  <?php echo esc_html( $type->slug ); ?>
				<?php endforeach; endif; ?>
			<?php } ?>">
	<div class="heading-block-red pad-3">
		<h1 class="f-30">
			<?php wpjm_the_job_title(); ?> -
			<?php if ( get_option( 'job_manager_enable_types' ) ) { ?>
				<?php $types = wpjm_get_the_job_types(); ?>
				<?php if ( ! empty( $types ) ) : foreach ( $types as $type ) : ?>
					  <?php echo esc_html( $type->name ); ?>
				<?php endforeach; endif; ?>
			<?php } ?>
		</h1>
	</div>

	<div class="vacancy-block">
		<div class="row">
			<div class="col-md-8">
				<?php wpjm_the_job_description(); ?>
			</div>
            <?php

            $post_meta = get_post_meta($post->ID);
            if($post_meta['_pw_hours']){
                $hrs = $post_meta['_pw_hours'][0];
            }else{
                $hrs = 10;
            }
            ?>
			<div class="col-md-4">
				<ul class="job-side-li">
					<li>
						<h3>Locatie</h3>
						<h4><?php the_job_location( false ); ?></h4>
					</li>

					<li>
						<h3>Aantal uren</h3>
						<h4><?php if ( get_option( 'job_manager_enable_types' ) ) { ?>
                                <?php $types = wpjm_get_the_job_types(); ?>
                                <?php if ( ! empty( $types ) ) : foreach ( $types as $type ) : ?>
                                    <?php echo esc_html( $type->name ); ?>
                                <?php endforeach; endif; ?>
                            <?php } ?> - <?php echo $hrs; ?> uur p/w</h4>
					</li>

					<li>
						<h3>Publicatiedatum</h3>

						<h4><?php the_job_publish_date(); ?></h4>
					</li>
				</ul>
			</div>
		</div>

		<div class="row">
			<div class="col-md-8 col-6">
				<a href="<?php the_job_permalink(); ?>" class="btn-line-hover h3 a-link">Meer informatie</a>
			</div>
			<div class="col-md-4 col-6">
				<a href="<?php the_job_permalink(); ?>" class="btn btn-primary btn-md">Bekijk</a>
			</div>
		</div>
	</div>
</div>

