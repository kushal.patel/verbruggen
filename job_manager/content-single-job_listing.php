<?php
/**
 * Single job listing.
 *
 * This template can be overridden by copying it to yourtheme/job_manager/content-single-job_listing.php.
 *
 * @see         https://wpjobmanager.com/document/template-overrides/
 * @author      Automattic
 * @package     WP Job Manager
 * @category    Template
 * @since       1.0.0
 * @version     1.28.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

global $post;

?>
<style>
	.pad80{
		padding: 0;
	}
</style>

<?php if ( get_option( 'job_manager_hide_expired_content', 1 ) && 'expired' === $post->post_status ) : ?>
	<div class="job-manager-info"><?php _e( 'This listing has expired.', 'wp-job-manager' ); ?></div>
<?php else : ?>
<?php
/**
 * single_job_listing_start hook
 *
 * @hooked job_listing_meta_display - 20
 * @hooked job_listing_company_display - 30
 */
do_action( 'single_job_listing_start' );
?>

<div class="heading-block-red ">
	<h1 class="heading1 m-0" id="head_text_form">
		<?php wpjm_the_job_title(); ?>
	</h1>
</div>
<div class="vacancy-block details_vacancy_jobs">
	<div class="row">
		<div class="col-md-8">
<!--			<h3 class="txt-medium">Baan omschrijving</h3>-->

			<?php wpjm_the_job_description(); ?>
			<div class="pb-5 pt-0"></div>
			<?php
			$postmeta = get_post_meta($post->ID);
			if($postmeta['_job_expires']){

				$expire = date("d - m - Y", strtotime($postmeta['_job_expires'][0]));
			}else{
				$expire = '';
			}?>
			<h3 class="txt-medium">Net even anders</h3>
			<p><?php echo $expire; ?></p>

			<a href="javascript:void(0)" class="btn btn-primary btn-lg mb-3 apply_jobs_sollicitern">Solliciteren</a>

			<p><em  class="text-d-grey">Vanwege het aantal sollicitaties dat we ontvangen, kan Verbruggen de ontvangst van uw inzending niet rechtstreeks beantwoorden of bevestigen. </em></p>

			<p><em  class="text-d-grey">Geslaagde sollicitaties krijgen bericht dat ze zijn geselecteerd voor een interview binnen twee weken na ontvangsdatum. Als u niet bent geselecteerd krijgt u ook een bericht. </em></p>
		</div>

		<div class="col-md-4">
			<aside class="vacancy-aside">
				<a href="javascript:void(0)" class="btn btn-primary btn-lg apply_jobs_sollicitern">Direct solliciteren</a>

				<div class="vacancy-form pt-5">
					<h3 class="mb-3">Vragen over deze vacature?</h3>
					<?php echo do_shortcode('[contact-form-7 id="469" title="Jobs-Question"]'); ?>
				</div>
			</aside>
		</div>
	</div>
</div>

	<?php
	/**
	 * single_job_listing_end hook
	 */
	do_action( 'single_job_listing_end' );
	?>
<?php endif; ?>

<div class="app_form_sollicitern_submit_data">
	<?php echo do_shortcode('[contact-form-7 id="812" title="Vacancy Form"]');?>
</div>
<div class="row vacancy-submited-success">
	<div class="col-md-12">
		<h2 class="f-30 pt-5 red-bdr">Bevestiging verzending</h2>
		<h1 class="mt-5 mb-5">Bedankt voor het solliciteren bij Verbruggen!</h1>
		<h3>Wij nemen spoedig contact met u op.</h3>
	</div>
</div>

<script>
	/* for the add of the apply application show */
	jQuery(document).ready(function () {
		jQuery('.app_form_sollicitern_submit_data').css('display','none');
		jQuery('.vacancy-submited-success').css('display','none');
		jQuery('.apply_jobs_sollicitern').on("click", function () {
			jQuery('html,body').scrollTop(0);
			jQuery('.job-name').val('<?php wpjm_the_job_title(); ?>');
			jQuery('#head_text_form').html('Je solliciteert nu voor:<br>Heftruck specialist');
			jQuery('.app_form_sollicitern_submit_data').css('display','block');
			jQuery('.details_vacancy_jobs').css('display','none');
//			jQuery('.vacancy-submited-success').css('display','block');
		});
	});


</script>