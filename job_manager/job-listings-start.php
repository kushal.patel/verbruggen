<?php
/**
 * Content shown before job listings in `[jobs]` shortcode.
 *
 * This template can be overridden by copying it to yourtheme/job_manager/job-listings-start.php.
 *
 * @see         https://wpjobmanager.com/document/template-overrides/
 * @author      Automattic
 * @package     WP Job Manager
 * @category    Template
 * @version     1.15.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>
<div class="heading-block-red pad-3"><h1>vacatures</h1></div>
<div class="spacer pad80 pt-0"></div>
<div class="row">
	<aside class="col-lg-3">
		<div class="filters filter-bl" style="width: 230px;z-index: 1;left: 10px;background: #fff;overflow-x: hidden;padding: 8px 0;position: sticky;top: 130px;">
			<h3 class="fl-title">Filters</h3>
			<?php if ( ! is_tax( 'job_listing_type' ) && empty( $job_types ) ) : ?>
				<ul class="list-sm">
					<?php $count_posts = wp_count_posts($type = 'job_listing');
					if ( $count_posts ) {
						$published_posts = $count_posts->publish;
					}
					?>
<!--					<li><label class="radio-bl" >Allemaal <i>--><?php //echo $published_posts; ?><!--</i>-->
<!--							<input type="radio" name="job_type" selected class="radio_allemaal" value="allemaal" data-id="--><?php //echo $published_posts; ?><!--">-->
<!--							<span class="checkmark"></span>-->
<!--						</label>-->
<!--					</li>-->
					<?php foreach ( get_job_listing_types() as $type ) : ?>
						<li><label class="radio-bl" ><?php echo esc_html( $type->name ); ?> <i><?php echo esc_html( $type->count ); ?></i>
								<input type="radio" name="job_type" class="radio_<?php echo esc_attr( $type->slug ); ?>" value="<?php echo esc_attr( $type->slug ); ?>" data-id="<?php echo esc_attr( $type->count ); ?>">
								<span class="checkmark"></span>
							</label>
						</li>
					<?php endforeach; ?>
				</ul>
			<?php endif; ?>
		</div>
	</aside>
	<section class="col-lg-9">
		<div class="row">
			<div class="col-md-12 no_jobs"></div>
			<ul class="job_listings">
				
