<?php
/**
 * The template for displaying the homepage.
 *
 * This page template will display any functions hooked into the `homepage` action.
 * By default this includes a variety of product displays and the page content itself. To change the order or toggle these components
 * use the Homepage Control plugin.
 * https://wordpress.org/plugins/homepage-control/
 *
 * Template name: assortiment-page
 *
 * @package storefront
 */
 wp_head();
get_header(); ?>
    <style>
        @media (max-width: 575.98px){
            .count-input a.incr-btn[data-action="increase"], .count-input a.incr-btn {
                /*top:0px;*/
            }
            /*.slider-product-images {*/
            /*width: 100%;*/
            /*}*/
        }
        /*assortiment hover effect*/
        /*.list.style-type2 li {
            border-bottom: unset;
            /*border-bottom: 2px solid #BDBDBD; */
        }
        .list.style-type2 li a {
            line-height: 20px;
            font-size: 1rem;
            padding: 4px 0;
            display: block;
            position: relative;
            /*border-bottom: 2px solid transparent !important;*/
        }*/
        /*.list.style-type2 li:hover {*/
        /*border-bottom: 2px solid #BDBDBD;*/
        /*}*/
        /*.list.style-type2 li a:hover:before{
            position: relative;
            visibility: visible;
            opacity: 1;
            right: 0px;
            float: right;
        }
        .list.style-type2 li a:before {
            content: "";
            width: 13px;
            height: 19px;
            display: inline-block;
            position: absolute;
            right: 10px;
            transition: all 0.3s ease-out 0s;
            -webkit-transition: all 0.3s ease-out 0s;
            visibility: hidden;
            opacity: 0;
        }
        .list.style-type2 li a:hover:after {
            visibility: visible;
            opacity: 1;
            width: 100%;
        }
        .list.style-type2 li a:after {
            width: 0;
            content: "";

            height: 2px;
            top: 6px;
            background-color: #EE3124;
            position: inherit;
            bottom: 0;
            display: block;
            visibility: hidden;
            opacity: 0;
            transition: all 0.3s ease-out 0s;
            -webkit-transition: all 0.3s ease-out 0s;
        }
        .list.style-type2 li a:hover {
         color: #32393E;
        }*/
    </style>
<input type="hidden" id="assortiment" value="true" />
<title><?php echo get_bloginfo(); ?> - <?php echo the_title(); ?></title>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
      <?php
      $image = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()),'full' , true); ?>
      <div class="inner-banner" style="background-image:url(<?php echo $image[0]; ?>);">
         <div class="container">
            <h1 class="text-white">Assortiment</h1>
       </div>
      </div>

      <?php
      $content = get_the_content();

  if(trim($content) == "") // Check if the string is empty or only whitespace
  {
  }
  else
  {
  }
  while ( have_posts() ) :
    the_post();

    the_content();

  endwhile;

  ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php


get_footer();
