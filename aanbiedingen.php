<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Verbuggen</title>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" name="viewport">
<link rel="stylesheet" type="text/css" href="plugins/bootstrap/bootstrap.css">
<link rel="stylesheet" type="text/css" href="plugins/fonts/style.css">
<link rel="stylesheet" type="text/css" href="style.css">
<link rel="stylesheet" type="text/css" href="plugins/owl-slider/owl.css">
<link rel="stylesheet" type="text/css" href="css/animate.css">
<link rel="stylesheet" type="text/css" href="css/responsive.css">
<link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="plugins/select/selectize.css">
<link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
<link rel="stylesheet" href="plugins/scrollbar/jquery.mCustomScrollbar.css">
</head>

<body>
<?php include 'header.php';?>


<div class="inner-banner" style="background-image:url(images/bg-aanbiedingen-banner.jpg);">
	<div class="container">
    	<h1 class="text-grey">Aanbiedingen</h1>
    </div>
</div>
<!-- end banner -->


<section class="bg-white pad80">
	<div class="container">
    	<div class="category-wrapper">
            <div class="shop-filter d-flex justify-content-end">
                <div class="order-by align-content-end">
                    
                    <form>
                        <select class="select">
                            <option value="1">Sorteer A-Z</option>
                            <option value="2">Sorteer 1</option>
                            <option value="3">Sorteer 2</option>
                            <option value="4">Sorteer 3</option>
                        </select>
                    </form>

                </div>
            </div>
            
            <div class="pro-cat-cols">
            	<div class="row">
            	<aside class="col-md-3">
                	<div class="filters filter-bl">
                    	<h3 class="mb-3"><strong>Zoetwaren</strong></h3>
                        <ul class="list-sm">
                          <li>
                          	<label class="radio-bl">Snoep
                          	  <input type="checkbox">
                              <span class="checkmark"></span>
                            </label>
                          </li>
                          <li>
                          	<label class="radio-bl">Koek
                          	  <input type="checkbox">
                              <span class="checkmark"></span>
                            </label>                          
                          </li>
                          <li>
                          	<label class="radio-bl">Chips en Snacks
                          	  <input type="checkbox">
                              <span class="checkmark"></span>
                            </label>                          
                          </li>
                        </ul>
                    </div>
                    
                </aside>
                <!--end filters -->
                
                <section class="col-md-9">
                	<div class="products-full-wrap clearfix">
                    	<div class="products row owl-carousel rel-pro-gallery3 btn-nxt-prev">
                        	<div class="product-block">
                                    <div class="pro-img">
                                        <a href="product-detail.php"><img src="images/products/product-6.png" class="img-responsive"/></a>                                    </div>
                                    <div class="product-info">
                                        <h4 class="light"><a href="product-detail.php">Bacardi white rum</a></h4>
                                        <h4 class="product-qty">0,75cl</h4>
                                    </div>
                                    <div class="product-title">
                                        <h4><a href="">Bar le Duc</a></h4>
                                    </div>
                                    <div class="it-input-qt">
                                        <div class="count-input">
                                        <a class="incr-btn" data-action="decrease" href="#"></a>
                                        <input class="quantity" type="text" name="quantity" value="1">
                                        <a class="incr-btn" data-action="increase" href="#"></a>                                    </div>
                                    </div>
                                 </div>
                            
                            <div class="product-block">
                                    <div class="pro-img">
                                        <a href="product-detail.php"><img src="images/products/product-5.png" class="img-responsive"/></a>                                    </div>
                                    <div class="product-info">
                                        <h4 class="light"><a href="product-detail.php">Hero Fruitontbijt Sinaasappel & kaneel</a></h4>
                                        <h4 class="product-qty">1L</h4>
                                    </div>
                                    <div class="product-title">
                                        <h4><a href="">Hero</a></h4>
                                    </div>
                                    <div class="it-input-qt">
                                        <div class="count-input">
                                        <a class="incr-btn" data-action="decrease" href="#"></a>
                                        <input class="quantity" type="text" name="quantity" value="1">
                                        <a class="incr-btn" data-action="increase" href="#"></a>                                    </div>
                                    </div>
                                </div>
                            
                            <div class="product-block">
                                    <div class="pro-img">
                                        <a href="product-detail.php"><img src="images/products/product-7.png" class="img-responsive"/></a>                                    </div>
                                    <div class="product-info">
                                        <h4 class="light"><a href="product-detail.php">Red Bull Zero sugar</a></h4>
                                        <h4 class="product-qty">0,33cl</h4>
                                    </div>
                                    <div class="product-title">
                                        <h4><a href="">Red Bull</a></h4>
                                    </div>
                                    <div class="it-input-qt">
                                        <div class="count-input">
                                        <a class="incr-btn" data-action="decrease" href="#"></a>
                                        <input class="quantity" type="text" name="quantity" value="1">
                                        <a class="incr-btn" data-action="increase" href="#"></a>                                    </div>
                                    </div>
                                </div>
                            
                            <div class="product-block">
                                    <div class="pro-img">
                                        <a href="product-detail.php"><img src="images/products/product-2.png" class="img-responsive"/></a>                                    </div>
                                    <div class="product-info">
                                        <h4 class="light"><a href="product-detail.php">Zwarte kip Advocaat</a></h4>
                                        <h4 class="product-qty">0,75cl</h4>
                                    </div>
                                    <div class="product-title">
                                        <h4><a href="">Zwarte kip</a></h4>
                                    </div>
                                    <div class="it-input-qt">
                                        <div class="count-input">
                                        <a class="incr-btn" data-action="decrease" href="#"></a>
                                        <input class="quantity" type="text" name="quantity" value="1">
                                        <a class="incr-btn" data-action="increase" href="#"></a>                                    </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </section>
            </div>
            </div>
            <!-- end product row -->
            
            <div class="pro-cat-cols pt-0">
            	<div class="row">
            	<aside class="col-md-3">
                	<div class="filters filter-bl">
                    	<h3 class="mb-3"><strong>Drank</strong></h3>
                        <ul class="list-sm">
                          <li>
                          	<label class="radio-bl">Wijnen
                          	  <input type="checkbox">
                              <span class="checkmark"></span>
                            </label>
                          </li>
                          <li>
                          	<label class="radio-bl">Bieren
                          	  <input type="checkbox">
                              <span class="checkmark"></span>
                            </label>                          
                          </li>
                          <li>
                          	<label class="radio-bl">Sappen
                          	  <input type="checkbox">
                              <span class="checkmark"></span>
                            </label>                          
                          </li>
                          <li>
                          	<label class="radio-bl">Frisdranken
                          	  <input type="checkbox">
                              <span class="checkmark"></span>
                            </label>                          
                          </li>
                        </ul>
                    </div>
                    
                </aside>
                <!--end filters -->
                
                <section class="col-md-9">
                	<div class="products-full-wrap clearfix">
                    	<div class="products row owl-carousel rel-pro-gallery3 ">
                        	<div class="product-block">
                                    <div class="pro-img">
                                        <a href="product-detail.php"><img src="images/products/product-6.png" class="img-responsive"/></a>                                    </div>
                                    <div class="product-info">
                                        <h4 class="light"><a href="product-detail.php">Bacardi white rum</a></h4>
                                        <h4 class="product-qty">0,75cl</h4>
                                    </div>
                                    <div class="product-title">
                                        <h4><a href="">Bar le Duc</a></h4>
                                    </div>
                                    <div class="it-input-qt">
                                        <div class="count-input">
                                        <a class="incr-btn" data-action="decrease" href="#"></a>
                                        <input class="quantity" type="text" name="quantity" value="1">
                                        <a class="incr-btn" data-action="increase" href="#"></a>                                    </div>
                                    </div>
                                 </div>
                            
                            <div class="product-block">
                            	<div class="pro-tag">OP =OP</div>
                                    <div class="pro-img">
                                        <a href="product-detail.php"><img src="images/products/product-5.png" class="img-responsive"/></a>                                    </div>
                                    <div class="product-info">
                                        <h4 class="light"><a href="product-detail.php">Hero Fruitontbijt Sinaasappel & kaneel</a></h4>
                                        <h4 class="product-qty">1L</h4>
                                    </div>
                                    <div class="product-title">
                                        <h4><a href="">Hero</a></h4>
                                    </div>
                                    <div class="it-input-qt">
                                        <div class="count-input">
                                        <a class="incr-btn" data-action="decrease" href="#"></a>
                                        <input class="quantity" type="text" name="quantity" value="1">
                                        <a class="incr-btn" data-action="increase" href="#"></a>                                    </div>
                                    </div>
                                </div>
                            
                            <div class="product-block">
                                    <div class="pro-img">
                                        <a href="product-detail.php"><img src="images/products/product-7.png" class="img-responsive"/></a>                                    </div>
                                    <div class="product-info">
                                        <h4 class="light"><a href="product-detail.php">Red Bull Zero sugar</a></h4>
                                        <h4 class="product-qty">0,33cl</h4>
                                    </div>
                                    <div class="product-title">
                                        <h4><a href="">Red Bull</a></h4>
                                    </div>
                                    <div class="it-input-qt">
                                        <div class="count-input">
                                        <a class="incr-btn" data-action="decrease" href="#"></a>
                                        <input class="quantity" type="text" name="quantity" value="1">
                                        <a class="incr-btn" data-action="increase" href="#"></a>                                    </div>
                                    </div>
                                </div>
                            
                        </div>
                    </div>
                </section>
            </div>
            </div>
            <!-- end product row -->
            
            <div class="pro-cat-cols pt-0">
            	<div class="row">
            	<aside class="col-md-3">
                	<div class="filters filter-bl">
                    	<h3 class="mb-3"><strong>Dagvers</strong></h3>
                        <ul class="list-sm">
                          <li>
                          	<label class="radio-bl">Dagvers
                          	  <input type="checkbox">
                              <span class="checkmark"></span>
                            </label>
                          </li>
                          <li>
                          	<label class="radio-bl">Zuivel vers
                          	  <input type="checkbox">
                              <span class="checkmark"></span>
                            </label>                          
                          </li>
                        </ul>
                    </div>
                    
                </aside>
                <!--end filters -->
                
                <section class="col-md-9">
                	<div class="products-full-wrap clearfix">
                    	<div class="products row owl-carousel rel-pro-gallery3">
                        	<div class="product-block">
                                    <div class="pro-img">
                                        <a href="product-detail.php"><img src="images/products/product-6.png" class="img-responsive"/></a>                                    </div>
                                    <div class="product-info">
                                        <h4 class="light"><a href="product-detail.php">Bacardi white rum</a></h4>
                                        <h4 class="product-qty">0,75cl</h4>
                                    </div>
                                    <div class="product-title">
                                        <h4><a href="">Bar le Duc</a></h4>
                                    </div>
                                    <div class="it-input-qt">
                                        <div class="count-input">
                                        <a class="incr-btn" data-action="decrease" href="#"></a>
                                        <input class="quantity" type="text" name="quantity" value="1">
                                        <a class="incr-btn" data-action="increase" href="#"></a>                                    </div>
                                    </div>
                                 </div>
                            
                            <div class="product-block">
                            	<div class="pro-tag">OP =OP</div>
                                    <div class="pro-img">
                                        <a href="product-detail.php"><img src="images/products/product-5.png" class="img-responsive"/></a>                                    </div>
                                    <div class="product-info">
                                        <h4 class="light"><a href="product-detail.php">Hero Fruitontbijt Sinaasappel & kaneel</a></h4>
                                        <h4 class="product-qty">1L</h4>
                                    </div>
                                    <div class="product-title">
                                        <h4><a href="">Hero</a></h4>
                                    </div>
                                    <div class="it-input-qt">
                                        <div class="count-input">
                                        <a class="incr-btn" data-action="decrease" href="#"></a>
                                        <input class="quantity" type="text" name="quantity" value="1">
                                        <a class="incr-btn" data-action="increase" href="#"></a>                                    </div>
                                    </div>
                                </div>
                            
                            <div class="product-block">
                                    <div class="pro-img">
                                        <a href="product-detail.php"><img src="images/products/product-7.png" class="img-responsive"/></a>                                    </div>
                                    <div class="product-info">
                                        <h4 class="light"><a href="product-detail.php">Red Bull Zero sugar</a></h4>
                                        <h4 class="product-qty">0,33cl</h4>
                                    </div>
                                    <div class="product-title">
                                        <h4><a href="">Red Bull</a></h4>
                                    </div>
                                    <div class="it-input-qt">
                                        <div class="count-input">
                                        <a class="incr-btn" data-action="decrease" href="#"></a>
                                        <input class="quantity" type="text" name="quantity" value="1">
                                        <a class="incr-btn" data-action="increase" href="#"></a>                                    </div>
                                    </div>
                                </div>
                            
                        </div>
                    </div>
                </section>
            </div>
            </div>
            <!-- end product row -->
            
            <div class="regiter-now mb-5">
            	<h1>Nog geen klant?</h1>
                <div class="become-client bg-food-vect">
                	<div class="regis-form-widgt m-auto">
                        <h5 class="mb-3">Registreer u vandaag als klant en krijg toegang tot het grootste en meest exclusive assortiment voor uw onderneming.</h5>
                        <form>
                            <div class="row">
                                <div class="col-md-12">
                                    <input class="form-control" type="text" placeholder="Naam">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <input class="form-control" type="text" placeholder="E-mail">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <button type="button" class="btn btn-primary element-1">klant worden</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            
            <div class="pro-cat-cols">
            	<div class="row">
            	<aside class="col-md-3">
                	<div class="filters filter-bl">
                    	<h3 class="mb-3"><strong>Non-Food</strong></h3>
                        <ul class="list-sm">
                          <li>
                          	<label class="radio-bl">Huishouden
                          	  <input type="checkbox">
                              <span class="checkmark"></span>
                            </label>
                          </li>
                          <li>
                          	<label class="radio-bl">Dierenvoeding
                          	  <input type="checkbox">
                              <span class="checkmark"></span>
                            </label>                          
                          </li>
                        </ul>
                    </div>
                    
                </aside>
                <!--end filters -->
                
                <section class="col-md-9">
                	<div class="products-full-wrap clearfix">
                    	<div class="products row owl-carousel rel-pro-gallery3 btn-nxt-prev">
                        	<div class="product-block">
                                    <div class="pro-img">
                                        <a href="product-detail.php"><img src="images/products/product-6.png" class="img-responsive"/></a>                                    </div>
                                    <div class="product-info">
                                        <h4 class="light"><a href="product-detail.php">Bacardi white rum</a></h4>
                                        <h4 class="product-qty">0,75cl</h4>
                                    </div>
                                    <div class="product-title">
                                        <h4><a href="">Bar le Duc</a></h4>
                                    </div>
                                    <div class="it-input-qt">
                                        <div class="count-input">
                                        <a class="incr-btn" data-action="decrease" href="#"></a>
                                        <input class="quantity" type="text" name="quantity" value="1">
                                        <a class="incr-btn" data-action="increase" href="#"></a>                                    </div>
                                    </div>
                                 </div>
                            
                            <div class="product-block">
                                    <div class="pro-img">
                                        <a href="product-detail.php"><img src="images/products/product-5.png" class="img-responsive"/></a>                                    </div>
                                    <div class="product-info">
                                        <h4 class="light"><a href="product-detail.php">Hero Fruitontbijt Sinaasappel & kaneel</a></h4>
                                        <h4 class="product-qty">1L</h4>
                                    </div>
                                    <div class="product-title">
                                        <h4><a href="">Hero</a></h4>
                                    </div>
                                    <div class="it-input-qt">
                                        <div class="count-input">
                                        <a class="incr-btn" data-action="decrease" href="#"></a>
                                        <input class="quantity" type="text" name="quantity" value="1">
                                        <a class="incr-btn" data-action="increase" href="#"></a>                                    </div>
                                    </div>
                                </div>
                            
                            <div class="product-block">
                                    <div class="pro-img">
                                        <a href="product-detail.php"><img src="images/products/product-7.png" class="img-responsive"/></a>                                    </div>
                                    <div class="product-info">
                                        <h4 class="light"><a href="product-detail.php">Red Bull Zero sugar</a></h4>
                                        <h4 class="product-qty">0,33cl</h4>
                                    </div>
                                    <div class="product-title">
                                        <h4><a href="">Red Bull</a></h4>
                                    </div>
                                    <div class="it-input-qt">
                                        <div class="count-input">
                                        <a class="incr-btn" data-action="decrease" href="#"></a>
                                        <input class="quantity" type="text" name="quantity" value="1">
                                        <a class="incr-btn" data-action="increase" href="#"></a>                                    </div>
                                    </div>
                                </div>
                            
                            <div class="product-block">
                                    <div class="pro-img">
                                        <a href="product-detail.php"><img src="images/products/product-2.png" class="img-responsive"/></a>                                    </div>
                                    <div class="product-info">
                                        <h4 class="light"><a href="product-detail.php">Zwarte kip Advocaat</a></h4>
                                        <h4 class="product-qty">0,75cl</h4>
                                    </div>
                                    <div class="product-title">
                                        <h4><a href="">Zwarte kip</a></h4>
                                    </div>
                                    <div class="it-input-qt">
                                        <div class="count-input">
                                        <a class="incr-btn" data-action="decrease" href="#"></a>
                                        <input class="quantity" type="text" name="quantity" value="1">
                                        <a class="incr-btn" data-action="increase" href="#"></a>                                    </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </section>
            </div>
            </div>
            <!-- end product row -->
            
            <div class="pro-cat-cols pt-0">
            	<div class="row">
            	<aside class="col-md-3">
                	<div class="filters filter-bl">
                    	<h3 class="mb-3"><strong>Koffie, Thee, Cacao</strong></h3>
                        <ul class="list-sm">
                          <li>
                          	<label class="radio-bl">Pads en Capsules
                          	  <input type="checkbox">
                              <span class="checkmark"></span>
                            </label>
                          </li>
                          <li>
                          	<label class="radio-bl">Koffiebonen
                          	  <input type="checkbox">
                              <span class="checkmark"></span>
                            </label>                          
                          </li>
                          <li>
                          	<label class="radio-bl">Filtermaling
                          	  <input type="checkbox">
                              <span class="checkmark"></span>
                            </label>                          
                          </li>
                          <li>
                          	<label class="radio-bl">Oploskoffie
                          	  <input type="checkbox">
                              <span class="checkmark"></span>
                            </label>                          
                          </li>
                          <li>
                          	<label class="radio-bl">Thee
                          	  <input type="checkbox">
                              <span class="checkmark"></span>
                            </label>                          
                          </li>
                          <li>
                          	<label class="radio-bl">Cacao
                          	  <input type="checkbox">
                              <span class="checkmark"></span>
                            </label>                          
                          </li>
                        </ul>
                    </div>
                    
                </aside>
                <!--end filters -->
                
                <section class="col-md-9">
                	<div class="products-full-wrap clearfix">
                    	<div class="products row owl-carousel rel-pro-gallery3">
                        	<div class="product-block">
                                    <div class="pro-img">
                                        <a href="product-detail.php"><img src="images/products/product-6.png" class="img-responsive"/></a>                                    </div>
                                    <div class="product-info">
                                        <h4 class="light"><a href="product-detail.php">Bacardi white rum</a></h4>
                                        <h4 class="product-qty">0,75cl</h4>
                                    </div>
                                    <div class="product-title">
                                        <h4><a href="">Bar le Duc</a></h4>
                                    </div>
                                    <div class="it-input-qt">
                                        <div class="count-input">
                                        <a class="incr-btn" data-action="decrease" href="#"></a>
                                        <input class="quantity" type="text" name="quantity" value="1">
                                        <a class="incr-btn" data-action="increase" href="#"></a>                                    </div>
                                    </div>
                                 </div>
                            
                            <div class="product-block">
                            	<div class="pro-tag">OP =OP</div>
                                    <div class="pro-img">
                                        <a href="product-detail.php"><img src="images/products/product-5.png" class="img-responsive"/></a>                                    </div>
                                    <div class="product-info">
                                        <h4 class="light"><a href="product-detail.php">Hero Fruitontbijt Sinaasappel & kaneel</a></h4>
                                        <h4 class="product-qty">1L</h4>
                                    </div>
                                    <div class="product-title">
                                        <h4><a href="">Hero</a></h4>
                                    </div>
                                    <div class="it-input-qt">
                                        <div class="count-input">
                                        <a class="incr-btn" data-action="decrease" href="#"></a>
                                        <input class="quantity" type="text" name="quantity" value="1">
                                        <a class="incr-btn" data-action="increase" href="#"></a>                                    </div>
                                    </div>
                                </div>
                            
                            <div class="product-block">
                                    <div class="pro-img">
                                        <a href="product-detail.php"><img src="images/products/product-7.png" class="img-responsive"/></a>                                    </div>
                                    <div class="product-info">
                                        <h4 class="light"><a href="product-detail.php">Red Bull Zero sugar</a></h4>
                                        <h4 class="product-qty">0,33cl</h4>
                                    </div>
                                    <div class="product-title">
                                        <h4><a href="">Red Bull</a></h4>
                                    </div>
                                    <div class="it-input-qt">
                                        <div class="count-input">
                                        <a class="incr-btn" data-action="decrease" href="#"></a>
                                        <input class="quantity" type="text" name="quantity" value="1">
                                        <a class="incr-btn" data-action="increase" href="#"></a>                                    </div>
                                    </div>
                                </div>
                            
                        </div>
                    </div>
                </section>
            </div>
            </div>
            <!-- end product row -->
            
            <div class="pro-cat-cols pt-0">
            	<div class="row">
            	<aside class="col-md-3">
                	<div class="filters filter-bl">
                    	<h3 class="mb-3"><strong>DKW</strong></h3>
                        <ul class="list-sm">
                          <li>
                          	<label class="radio-bl">Kant- en Klaar
                          	  <input type="checkbox">
                              <span class="checkmark"></span>
                            </label>
                          </li>
                          <li>
                          	<label class="radio-bl">Beleg
                          	  <input type="checkbox">
                              <span class="checkmark"></span>
                            </label>                          
                          </li>
                          <li>
                          	<label class="radio-bl">Internationale keuken
                          	  <input type="checkbox">
                              <span class="checkmark"></span>
                            </label>                          
                          </li>
                          <li>
                          	<label class="radio-bl">Vetten & Olie
                          	  <input type="checkbox">
                              <span class="checkmark"></span>
                            </label>                          
                          </li>
                          <li>
                          	<label class="radio-bl">Soepen & sauzen
                          	  <input type="checkbox">
                              <span class="checkmark"></span>
                            </label>                          
                          </li>
                          <li>
                          	<label class="radio-bl">Zuivel houdbaar
                          	  <input type="checkbox">
                              <span class="checkmark"></span>
                            </label>                          
                          </li>
                          <li>
                          	<label class="radio-bl">Conserven
                          	  <input type="checkbox">
                              <span class="checkmark"></span>
                            </label>                          
                          </li>
                        </ul>
                    </div>
                    
                </aside>
                <!--end filters -->
                
                <section class="col-md-9">
                	<div class="products-full-wrap clearfix">
                    	<div class="products row owl-carousel rel-pro-gallery3">
                        	<div class="product-block">
                            	<div class="pro-tag">OP =OP</div>
                                    <div class="pro-img">
                                        <a href="product-detail.php"><img src="images/products/product-6.png" class="img-responsive"/></a>                                    </div>
                                    <div class="product-info">
                                        <h4 class="light"><a href="product-detail.php">Bacardi white rum</a></h4>
                                        <h4 class="product-qty">0,75cl</h4>
                                    </div>
                                    <div class="product-title">
                                        <h4><a href="">Bar le Duc</a></h4>
                                    </div>
                                    <div class="it-input-qt">
                                        <div class="count-input">
                                        <a class="incr-btn" data-action="decrease" href="#"></a>
                                        <input class="quantity" type="text" name="quantity" value="1">
                                        <a class="incr-btn" data-action="increase" href="#"></a>                                    </div>
                                    </div>
                                 </div>
                            
                            <div class="product-block">
                                    <div class="pro-img">
                                        <a href="product-detail.php"><img src="images/products/product-5.png" class="img-responsive"/></a>                                    </div>
                                    <div class="product-info">
                                        <h4 class="light"><a href="product-detail.php">Hero Fruitontbijt Sinaasappel & kaneel</a></h4>
                                        <h4 class="product-qty">1L</h4>
                                    </div>
                                    <div class="product-title">
                                        <h4><a href="">Hero</a></h4>
                                    </div>
                                    <div class="it-input-qt">
                                        <div class="count-input">
                                        <a class="incr-btn" data-action="decrease" href="#"></a>
                                        <input class="quantity" type="text" name="quantity" value="1">
                                        <a class="incr-btn" data-action="increase" href="#"></a>                                    </div>
                                    </div>
                                </div>
                            
                            <div class="product-block">
                                    <div class="pro-img">
                                        <a href="product-detail.php"><img src="images/products/product-7.png" class="img-responsive"/></a>                                    </div>
                                    <div class="product-info">
                                        <h4 class="light"><a href="product-detail.php">Red Bull Zero sugar</a></h4>
                                        <h4 class="product-qty">0,33cl</h4>
                                    </div>
                                    <div class="product-title">
                                        <h4><a href="">Red Bull</a></h4>
                                    </div>
                                    <div class="it-input-qt">
                                        <div class="count-input">
                                        <a class="incr-btn" data-action="decrease" href="#"></a>
                                        <input class="quantity" type="text" name="quantity" value="1">
                                        <a class="incr-btn" data-action="increase" href="#"></a>                                    </div>
                                    </div>
                                </div>
                            
                        </div>
                    </div>
                </section>
            </div>
            </div>
            <!-- end product row -->
        </div>
    </div>
</section>

<?php include 'footer.php';?>
<script src="plugins/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>