<?php
/**
 * Created by PhpStorm.
 * User: kushal
 * Date: 12/02/19
 * Time: 12:01 PM
 */


function awesome_script_enqueue(){
    $version = '1.0.1';
    wp_enqueue_style('bootstrap1',get_template_directory_uri().'/plugins/bootstrap/bootstrap.css',array(),$version,'');
    wp_enqueue_style('style-plugins',get_template_directory_uri().'/plugins/fonts/style.css',array(),$version,'');
    wp_enqueue_style('style',get_template_directory_uri().'/style.css',array(),$version,'');
    wp_enqueue_style('custom',get_template_directory_uri().'/custom/custom.css',array(),$version,'');
    wp_enqueue_style('owl1',get_template_directory_uri().'/plugins/owl-slider/owl.css',array(),$version,'');
    wp_enqueue_style('animate',get_template_directory_uri().'/css/animate.css',array(),$version,'');
    wp_enqueue_style('responsive',get_template_directory_uri().'/css/responsive.css',array(),$version,'');
    wp_enqueue_style('googleapi','https://fonts.googleapis.com/css?family=Quicksand:300,400,500',array(),$version,'');
    wp_enqueue_style('select',get_template_directory_uri().'/plugins/select/selectize.css',array(),$version,'');
    wp_enqueue_style('scrollbar',get_template_directory_uri().'/plugins/scrollbar/jquery.mCustomScrollbar.css',array(),$version,'');
    wp_enqueue_style('menu',get_template_directory_uri().'/plugins/dd-menu/ddfullscreenmenu.css',array(),$version,'');



    wp_enqueue_script('jquery',get_template_directory_uri().'/plugins/jquery/jquery.min.js',array(),$version,true);
    wp_enqueue_script('bootstrap2',get_template_directory_uri().'/plugins/bootstrap/bootstrap.js',array(),$version,true);
    wp_enqueue_script('owl2',get_template_directory_uri().'/plugins/owl-slider/owl.js',array(),$version,true);
    wp_enqueue_script('isotope',get_template_directory_uri().'/plugins/isotope/isotope.min.js',array(),$version,true);
    wp_enqueue_script('custom',get_template_directory_uri().'/custom/custom.js',array(),$version,true);
    wp_enqueue_script('jqueryeasing',get_template_directory_uri().'/plugins/jquery.easing.min.js',array(),$version,true);
    wp_enqueue_script('stickeymenu',get_template_directory_uri().'/plugins/sticky-menu.js',array(),$version,true);
    wp_enqueue_script('scrollbar',get_template_directory_uri().'/plugins/scrollbar/jquery.mCustomScrollbar.concat.min.js',array(),$version,true);
    wp_enqueue_script('selectize',get_template_directory_uri().'/plugins/select/selectize.min.js',array(),$version,true);
    wp_enqueue_script('scriptwizard', get_template_directory_uri() . '/plugins/step-wizard/jquery.backstretch.min.js', array(), $version, true);
    wp_enqueue_script('menu', get_template_directory_uri() . '/plugins/dd-menu/ddfullscreenmenu.js', array(), $version, true);
    wp_enqueue_script('script', get_template_directory_uri() . '/custom/scripts.js', array(), $version, true);
}
add_action('wp_enqueue_scripts','awesome_script_enqueue');


function awesome_theme_setup(){
    add_theme_support('menus');
    register_nav_menu('primary','Primary Header Navigation');
}
add_action('init','awesome_theme_setup');


/**
* for the category wise least.
*/
function category_list() {

    $html = '<ul class="nav gallery-menu item-tab-nav mb-4 justify-content-between">
                <li class="active cat_list_gallary" data-filter="*" data-value="all"><a href="javascript:void()">Alle producten</a></li>';

        $args = array(
            'number'     => 8,
            'orderby'    => 'title',
            'order'      => 'ASC',
            'hide_empty' => $hide_empty,
            'include'    => $ids,
            'parent'     => 0
        );
        $product_categories = get_terms( 'product_cat', $args );
        $mainCat = [];
        foreach ($product_categories as $key => $category) {

            if($category->slug != 'uncategorized'){
                $args = array(
                    'number'     => 6,
                    'orderby'    => 'title',
                    'order'      => 'ASC',
                    'hide_empty' => $hide_empty,
                    'include'    => $ids,
                    'parent'     => $category->term_id
                );
                $subcategories = get_terms( 'product_cat', $args );
                array_push($mainCat,array('category_id'=>$category->term_id,'category_name'=>$category->name,'slug'=>$category->slug,'subcategory'=>$subcategories));
                $html .= '<li data-filter=".zoet" class="cat_list_gallary" data-value="'.$category->slug.'"><a href="javascript:void(0)">'.$category->name.'</li>';
            }
        }
            $html .='</ul>';
            //  echo "<pre>";
            $allCat = [];
            foreach ($mainCat as $key => $value) {
                // print_r($value);die;
                $catname = $value['category_name'];
                $html .= '<div class="gallery-items row '.$value['slug'].'" style="display:none;">';
                // if(!empty($value['subcategory'])){
                        // $html .= 'no data available....!!!';
                // }else{

                    foreach ($value['subcategory'] as $key => $subcat) {
                        $thumbnail_id = get_woocommerce_term_meta($subcat->term_id, 'thumbnail_id', true );
                        $image = wp_get_attachment_url( $thumbnail_id );
                        if(empty($image)){
                            $image = get_template_directory_uri().'/images/no-image.jpg';
                        }
                        $url = home_url('product-category/'.$subcat->slug);
                        $html .= '<div class="gallery-item zoet col-md-4 col-sm-6 col-6">
                        <div class="gallery-image">
                        <a href="'.$url.'"><img src="'.$image.'" alt="gallery image" class="img-responsive "></a>
                        </div>
                        <h3>'.$subcat->name.'</h3>
                        </div>';
                        array_push($allCat,array('subcat_name'=>$subcat->name,'image'=>$image));
                    }
                    $html .='</div>';
                // }
            }
            $html .= '<div class="gallery-items row all">';
//            foreach ($allCat as $key => $value) {
//                $html .= '<div class="gallery-item zoet col-md-4 col-sm-6 col-6">
//                <div class="gallery-image">
//                        <a href=""><img src="'.$value['image'].'" alt="gallery image" class="img-responsive" style="height:380px;width:390px;"></a>
//                </div>
//                <h3>'.$value['subcat_name'].'</h3>
//            </div>';
//            }
            foreach ($mainCat as $key => $value){
                $thumbnail_id = get_woocommerce_term_meta($value['category_id'], 'thumbnail_id', true );
                $image = wp_get_attachment_url( $thumbnail_id );
                if(empty($image)){
                    $image = get_template_directory_uri().'/images/no-image.jpg';
                }
                $html .= '<div class="gallery-item zoet col-md-4 col-sm-6 col-6">
                <div class="gallery-image">
                        <a href="'.home_url('product-category/'.$value['slug']).'"><img src="'.$image.'" alt="gallery image" class="img-responsive "></a>
                </div>
                <h3>'.$value['category_name'].'</h3>
            </div>';
            }
            $html .='</div>';
    return $html;
}
add_shortcode('category_list','category_list');


/*Logo customizer function*/
function verbruggen_customizer_register($wp_customize){

$wp_customize->add_section('verbruggen_header_top_logo',array(
    'title' => __('Header Area','verbruggen'),
    'description' => 'If you need to update logo.You can update this. It is easy.'
));

$wp_customize->add_setting('verbruggen_logo',array(
    'default'=> get_bloginfo('template_directory').'/images/logo.png'
));

$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize,'verbruggen_logo',array(
    'label' => 'logo',
    'description' => 'Upload your logo here',
    'section' => 'verbruggen_header_top_logo',
    'settings' => 'verbruggen_logo',
    'extensions' => array( 'jpg', 'jpeg', 'gif', 'png', 'svg' ),
)));

}
add_action('customize_register','verbruggen_customizer_register');

/*
 * for add svg extension images
 */
function cc_mime_types($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');


/*
* offers related items category wise products sliders
*/
function offers_time(){
    global $woocommerce;
$html = '<ul class="nav item-tab-nav mb-4 justify-content-between">
   <li class="nav-item">
    <a class="nav-link active offers_cat all_prod" data-value="all_prods" data-toggle="tab" href="#pro-item1">Alle aanbiedingen</a>
   </li>';
$args = array(
'taxonomy'   => "product_cat",
'number'     => 6,
'orderby'    => 'title',
'order'      => 'ASC',
'hide_empty' => $hide_empty,
'include'    => $ids,
 'parent'     => 0
);

$product_categories = get_terms($args);
$product_cats = [];
foreach ($product_categories as $key => $value) {
    $args = array(
        'post_type'      => 'product',
        'posts_per_page' => 10,
        'product_cat'    => $value->slug
    );

    $loop = new WP_Query( $args );
    $mycount = count($loop->posts);
if($value->slug != "uncategorized"){
    if($mycount > 0){
        $html .= '<li data-filter=""><a href="javascript:void(0)" class="offers_cat '.$value->slug.'" data-value="'.$value->slug.'">'.$value->name.'</a></li>';
    }
    else{
        $html .= '<li data-filter=""><a href="javascript:void(0);">'.$value->name.'</a></li>';
    }
}
}
$html .= '</ul>';

    $tags_list = get_terms( 'product_tag' );
    $tags = array();
    $lists = '';
    if($tags_list){
        foreach ($tags_list as $tag){
            $tags[] = $tag->slug;
        }
        $lists = implode (", ", $tags);
    }
$args = array(
    'post_type' => 'product',
    'paged' => $paged,
    'posts_per_page' => '6',
    'product_tag' 	 => $lists
);
$wp_query = new WP_Query($args);

if($wp_query->posts){
  // all products display here
 $html .= '
 <div class="tab-pane active all_prods">
     <div class="category-pro-bl">
        <h1>All Products</h1>
        <h3>'.count($wp_query->posts).' aanbiedingen</h3>
        <div class="owl-carousel product-slider btn-nxt-prev">';
        foreach ($wp_query->posts as $key => $value) {
            $cat = wp_get_post_terms( $value->ID, 'product_cat' );
            $image = get_the_post_thumbnail_url($value->ID);
            $product = wc_get_product( $value->ID );
//            echo "<pre>";print_r($product->regular_price);die;
            $targeted_id = $product->get_id();  //product id
            $featured  = $product->is_featured();
            $brand = array();
            $brands_list = get_the_terms( $value->ID,'pa_brand' );
            if($brands_list){
                foreach ($brands_list as $brands){
                    if(!empty($brands)) {
                        $brand[] = '<a href="' . get_site_url() . '/shop/?filter_brand=' . $brands->slug . '">' . $brands->name . '</a>';
                    }
                }
                $brands = implode (", ", $brand);

            }else{
                $brands = '&nbsp;';
            }
//
            $quantity = get_the_terms( $value->ID,'pa_quantity');
            $qtys = array();
            if($quantity){
                foreach ($quantity as $qty){
                    if(!empty($qty)) {
                        $qtys[] = $qty->name;
                    }
                }
                $quanty = implode (", ", $qtys);
            }else{
                $quanty = '&nbsp;';
            }


            $size_list = get_the_terms( $value->ID,'pa_size');
            $sizes = array();
            if($size_list){
                foreach ($size_list as $size){
                    if(!empty($size)){
                        $sizes[] = $size->name;
                    }
                }
                $size = implode (", ", $sizes);
            }else{
                $size = '&nbsp;';
            }
//            echo "<pre>";
//            print_r($size);die;
            $count = 0;
            if(!is_null(WC()->cart)) {
               foreach (WC()->cart->get_cart() as $cart_item) {
                   if ($cart_item['product_id'] == $targeted_id) {
                       $count = $cart_item['quantity'];
                       break; // stop the loop if product is found
                   }
               }
            }
            $tags = wp_get_post_terms( $value->ID , 'product_tag' );
            $class_offer = '';
        //           if($featured == 1){
        //               $class_offer = 'offer';
        //           }
            if($tags[0]->slug == 'opop'){
                $class_offer = 'offer';
            }else if($tags[0]->slug == 'aanbieding'){
                $class_offer = 'aanbied';
            }
           $html .= '<div class="product-block '.$class_offer.'">';
           $Stockstatus = $product->get_stock_status();
           if($tags[0]->slug == 'opop'){
               $html .= '<div class="pro-tag">'.$tags[0]->name.'</div>';
           }else if($tags[0]->slug == 'aanbieding'){
               $html .= '<div class="pro-tag">'.$tags[0]->name.'</div>';
           }
            $html .= '<div class="pro-img"><a href="'.home_url('product/'.$product->slug).'"><img src="'.$image.'" class="img-responsive slider-product-images"/></a></div>
             <div class="product-info">
              <h4 class="light"><a href="'.home_url('product/'.$product->slug).'">'.$value->post_title.'</a></h4>
             <h4 class="product-qty">'.$quanty.'</h4></div>
             <div class="product-title"><h4>'.$brands.'</h4></div>
             <div class="product-size"><h4>'.$size.'</h4></div>';
            if( !$product->has_child() && $Stockstatus == "instock") {
                $html .= '<div class="it-input-qt"><div class="count-input">
                 <input type="hidden" class="add_to_cart_button" data-product_id="'.$value->ID.'" />
                 <a class="incr-btn" data-action="decrease" href="#"></a>
                 <input class="quantity" type="text" name="quantity" value="'.$count.'">
                 <a class="incr-btn" data-action="increase" href="#"></a>';
                $html .= '</div></div>';
            }
            $html .= '<span class="item-price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">€</span>'.number_format((float)$product->regular_price, 2, '.', '').'</span></span>
             ';
//            <h4><a href="'.home_url('product/'.$product->slug).'">'.$cat[0]->name.'</a></h4>

           $html .= '</div>';
         }
    $html .= '</div>
   </div>
 </div>';
}
//category wise products display below
$args = array(
   'number'     => $number,
   'orderby'    => 'title',
   'order'      => 'ASC',
   'hide_empty' => $hide_empty,
   'include'    => $ids
);
$product_categories = get_terms( 'product_cat', $args );
$count = count($product_categories);
if ( $count > 0 ){
    $tags_list = get_terms( 'product_tag' );
    $tags = array();
    $lists = '';
    if($tags_list){
        foreach ($tags_list as $tag){
            $tags[] = $tag->slug;
        }
        $lists = implode (", ", $tags);
    }
 foreach ( $product_categories as $product_category ) {
    $args = array(
         'posts_per_page' => 6,
         'tax_query' => array(
          'relation' => 'AND',
             array(
                 'taxonomy' => 'product_cat',
                 'field' => 'slug',
                 // 'terms' => 'white-wines'
                 'terms' => $product_category->slug
             )
         ),
         'post_type' => 'product',
         'orderby' => 'title,',
         'product_tag' 	 => $lists
    );
     $products = new WP_Query( $args );
     if($products->posts){
//         echo "<pre>";print_r($products->posts);
         $html .= '<div class="tab-pane offers_'.$product_category->slug.'" style="display:none;">
             <div class="category-pro-bl">
                 <h1>'.$product_category->name.'</h1>
               <h3>'.count($products->posts).'  aanbiedingen</h3>
               <div class="owl-carousel product-slider btn-nxt-prev">';
                foreach ($products->posts as $key => $value) {
                  $image  = get_the_post_thumbnail_url($value->ID);
                  $product = wc_get_product( $value->ID );
                  $targeted_id = $product->get_id();  //product id

                    $count = 0;
                    if(!is_null(WC()->cart)) {
                        foreach ( WC()->cart->get_cart() as $cart_item ) {
                            if($cart_item['product_id'] == $targeted_id ){
                                $count =  $cart_item['quantity'];
                                break; // stop the loop if product is found
                            }
                        }
                    }
                    $brand = array();
                    $brands_list = get_the_terms( $value->ID,'pa_brand' );
                    if($brands_list){
                        foreach ($brands_list as $brands){
                            if(!empty($brands)) {
                                $brand[] = '<a href="' . get_site_url() . '/shop/?filter_brand=' . $brands->slug . '">' . $brands->name . '</a>';
                            }
                        }
                        $brands = implode (", ", $brand);

                    }else{
                        $brands = '&nbsp;';
                    }
//
                    $quantity = get_the_terms( $value->ID,'pa_quantity');
                    $qtys = array();
                    if($quantity){
                        foreach ($quantity as $qty){
                            if(!empty($qty)) {
                                $qtys[] = $qty->name;
                            }
                        }
                        $quanty = implode (", ", $qtys);
                    }else{
                        $quanty = '&nbsp;';
                    }


                    $size_list = get_the_terms( $value->ID,'pa_size');
                    $sizes = array();
                    if($size_list){
                        foreach ($size_list as $size){
                            if(!empty($size)){
                                $sizes[] = $size->name;
                            }
                        }
                        $size = implode (", ", $sizes);
                    }else{
                        $size = '&nbsp;';
                    }
//                    $featured  = $product->is_featured();
//
//                    if($featured == 1){
//                        $class_offer = 'offer';
//                    }
                    $class_offer = '';
                    $tags = wp_get_post_terms( $value->ID , 'product_tag' );
                    if($tags[0]->slug == 'opop'){
                        $class_offer = 'offer';
                    }else if($tags[0]->slug == 'aanbieding'){
                        $class_offer = 'aanbied';
                    }

                    $html .= '<div class="product-block '.$class_offer.'">';
                    $Stockstatus = $product->get_stock_status();
                    if($tags[0]->slug == 'opop'){
                        $html .= '<div class="pro-tag">'.$tags[0]->name.'</div>';
                    }else if($tags[0]->slug == 'aanbieding'){
                        $html .= '<div class="pro-tag">'.$tags[0]->name.'</div>';
                    }
                   $html .= '
                       <div class="pro-img"><a href="'.home_url('product/'.$product->slug).'"><img src="'.$image.'" class="img-responsive slider-product-images"/></a></div>
                       <div class="product-info">
                        <h4 class="light"><a href="'.home_url('product/'.$product->slug).'">'.$value->post_title.'</a></h4>
                       <h4 class="product-qty">'.$quanty.'</div>
                        <div class="product-title"><h4>'.$brands.'</h4></div>
                       <div class="product-size"><h4>'.$size.'</h4></div>';
//                    <h4><a href="'.home_url('product/'.$product->slug).'">'.$product_category->name.'</a></h4>
                    if( !$product->has_child() && $Stockstatus == "instock") {
                        $html .= '<div class="it-input-qt">
                       <div class="count-input">
                           <input type="hidden" class="add_to_cart_button" data-product_id="' . $value->ID . '" />
                           <a class="incr-btn" data-action="decrease" href="#"></a>
                           <input class="quantity" type="text" name="quantity" value="' . $count . '">
                           <a class="incr-btn" data-action="increase" href="#"></a>
                       </div></div>';
                    }
                    $html .= '<span class="item-price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">€</span>'.number_format((float)$myproduct->$product->regular_price, 2, '.', '').'</span></span>';
                    $html .= '</div>';

                }
                $html .= '</div>
               </div>
             </div>';
      }
   }
 }
 return $html;
}
add_shortcode('offers_time','offers_time');

/*
* function for the aanbidegen page, categoryry wise display list sub category
* category wise products sliders
*/
function category_subcat_list_product_slider(){
    $html = '<section class="bg-white pad80">
    <div class="container">
        <div class="category-wrapper">
              <div class="shop-filter d-flex justify-content-end">
                <h4 class="result-count"><strong>Aanbiedingen:</strong> 4 t/m 10 maart </h4>
            </div>';
    $args = array(
        'number'     => $number,
        'orderby'    => 'title',
        'order'      => 'ASC',
        'hide_empty' => $hide_empty,
        'include'    => $ids,
        'parent'     => 0
    );
    $product_categories = get_terms( 'product_cat', $args );
    $mainCat = [];
    $i = 0;
    foreach ($product_categories as $key => $category) {

        if($category->slug != 'uncategorized'){
            $html .= '
                <div class="pro-cat-cols">
                    <div class="row" '.$i.'>
                        <section class="col-md-12">
                            <h3 class="fl-title">'.$category->name.'</h3>';
//                           $gtml .= '<ul class="list-sm">';
            $args = array(
                'number'     => $number,
                'orderby'    => 'title',
                'order'      => 'ASC',
                'hide_empty' => $hide_empty,
                'include'    => $ids,
                'parent'     => $category->term_id
            );
            $subcategories = get_terms( 'product_cat', $args );
//            foreach ($subcategories as $key => $value) {
//
//                $html .= '<li>
//                              <label class="radio-bl">'.$value->name.'
//                              <input type="checkbox">
//                              <span class="checkmark"></span>
//                              </label>
//                          </li>';
//            }
//            $html .= '</ul>';
            $html .= '';
            //end filters


            $tags_list = get_terms( 'product_tag' );
            $tags = array();
            $lists = '';
            if($tags_list){
                foreach ($tags_list as $tag){
                    $tags[] = $tag->slug;
                }
                $lists = implode (", ", $tags);
            }
            //category wise products display below
            $args = array(
                'posts_per_page' => -1,
                'tax_query' => array(
                    'relation' => 'AND',
                    array(
                        'taxonomy' => 'product_cat',
                        'field' => 'slug',
                        // 'terms' => 'white-wines'
                        'terms' => $category->slug
                    )
                ),
                'post_type' => 'product',
                'orderby' => 'title,',
                'product_tag'    => $lists
            );
            $products = new WP_Query( $args );
            if($products->posts){
                $html .= '<div class="products-full-wrap clearfix">
                           <div class="owl-carousel rel-pro-gallery3 ">';
//                $html .= '<div class="products row owl-carousel rel-pro-gallery3 btn-nxt-prev">';
                foreach ($products->posts as $key => $prod) {
                    $image  = get_the_post_thumbnail_url($prod->ID);
//                    $quantity = get_the_terms( $prod, 'pa_quantity');
//                    if(!empty($quantity)){
//                        $quantity = $quantity[0]->name;
//                    }else{
//                        $quantity = 'NA';
//                    }
                    $brand = array();
                    $brands_list = get_the_terms( $prod->ID,'pa_brand' );
                    if($brands_list){
                        foreach ($brands_list as $brands){
                            $brand[] = '<a href="'.get_site_url().'/shop/?filter_brand='.$brands->slug.'">'.$brands->name.'</a>';
                        }
                        $brands = implode (", ", $brand);
                    }
                    $quantity = get_the_terms( $prod->ID,'pa_quantity');
                    $qtys = array();
                    if($quantity){
                        foreach ($quantity as $qty){
                            $qtys[] = $qty->name;
                        }
                        $quanty = implode (", ", $qtys);
                    }

                    $size_list = get_the_terms( $prod->ID,'pa_size');
                    $sizes = array();
                    if($size_list){
                        foreach ($size_list as $size){
                            $sizes[] = $size->name;
                        }
                        $size = implode (", ", $sizes);
                    }

                    $product = wc_get_product( $prod->ID );
                    $targeted_id = $prod->ID;  //product id

                    $count = 0;
                    if(!is_null(WC()->cart)) {
                        foreach (WC()->cart->get_cart() as $cart_item) {
                            if ($cart_item['product_id'] == $targeted_id) {
                                $count = $cart_item['quantity'];
                                break; // stop the loop if product is found
                            }
                        }
                    }

                    $class_offer = '';
                    $tags = wp_get_post_terms( $prod->ID , 'product_tag' );
                    if($tags[0]->slug == 'opop'){
                        $class_offer = 'offer';
                    }else if($tags[0]->slug == 'aanbieding'){
                        $class_offer = 'aanbied';
                    }

                    $Stockstatus = $product->get_stock_status();
                    $html .= '
                                <div class="product-block '.$class_offer.'">';
                    $tags = wp_get_post_terms( $prod->ID , 'product_tag' );
                    if($tags[0]->slug == 'opop'){
                        $html .= '<div class="pro-tag">'.$tags[0]->name.'</div>';
                    }else if($tags[0]->slug == 'aanbieding'){
                        $html .= '<div class="pro-tag">'.$tags[0]->name.'</div>';
                    }
//                                if($Stockstatus != "instock") {
//                                    $html .= '<div class="pro-tag">OP =OP</div>';
//                                }
//                                if($featured == 1){
//                                    $html .= '<div class="cat-op">AANBIEDING</div>';
//                                }
                    $html .= '<div class="pro-img">
                                          <a href="'.home_url('product/'.$prod->post_name).'"><img src="'.$image.'" class="img-responsive slider-product-images"/></a>
                                      </div>
                                      <div class="product-info">
                                          <h4 class="light"><a href="'.home_url('product/'.$prod->post_name).'">'.$prod->post_title.'</a></h4>
                                          <h4 class="product-qty">'.$quanty.'</h4>
                                      </div>
                                      <div class="product-title">
                                          <h4>'.$brands.'</h4>
                                      </div>
                                      <div class="product-size">
                                          <h4>'.$size.'</h4>
                                      </div>';
                    if( !$product->has_child() && $Stockstatus == "instock") {
                        $html .= '<div class="it-input-qt">
                                       <div class="count-input">
                                           <input type="hidden" class="add_to_cart_button" data-product_id="' . $prod->ID . '" />
                                           <a class="incr-btn" data-action="decrease" href="#"></a>
                                           <input class="quantity" type="text" name="quantity" value="' . $count . '">
                                           <a class="incr-btn" data-action="increase" href="#"></a>
                                       </div>
                                       </div>';
                    }
                    $html .= '<span class="item-price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">€</span>'.number_format((float)$product->regular_price, 2, '.', '').'</span></span>';
                    $html .= '</div>';
                    // end products for each loop
                }
                $html .= '</div>';
                $html .= '</div>';
                // end if for products post
            }else{
                $html .= '<div class="products-full-wrap clearfix">';
                $html .= '<div class="col-md-12 col-sm-6">
                    <p>Geen bruikbare producten</p>
                </div>';
                $html .= '</div>';
            }

            $html .= '
               </section>
               </div>
                </div>';
            if($i == 1){
                $html .= '<div class="regiter-now mb-5">
                      <h1>Nog geen klant?</h1>
                        <div class="become-client bg-food-vect">
                          <div class="regis-form-widgt m-auto">
                                <h5 class="mb-3">Registreer u vandaag als klant en krijg toegang tot het grootste en meest exclusive assortiment voor uw onderneming.</h5>
                                '.do_shortcode('[contact-form-7 id="62" title="register_home"]').'
                            </div>
                        </div>
                    </div>';
            }
            $i++;
            //end if
        }
        // end category foreach loop
    }
    // end product row
    $html .= '</div>
      </div>
  </section>';
    return $html;
}
add_shortcode('category_subcat_list_product_slider','category_subcat_list_product_slider');


/**
 * for the assortiment page , category wise least.
 */
function category_subcategory_list() {

    $html = '<ul class="nav gallery-menu item-tab-nav mb-4 justify-content-between">
              <li class="active cat_list_gallary" data-filter="*" data-value="all"><a href="javascript:void(0)">Alle producten</a></li>';

    $args = array(
        'number'     => 8,
        'orderby'    => 'title',
        'order'      => 'ASC',
        'hide_empty' => $hide_empty,
        'include'    => $ids,
        'parent'     => 0
    );
    $product_categories = get_terms( 'product_cat', $args );
    $mainCat = [];
    foreach ($product_categories as $key => $category) {
        if($category->slug != 'uncategorized'){
            $args = array(
                'number'     => 6,
                'orderby'    => 'title',
                'order'      => 'ASC',
                'hide_empty' => $hide_empty,
                'include'    => $ids,
                'parent'     => $category->term_id
            );
            $subcategories = get_terms( 'product_cat', $args );
            // array_push($mainCat,array('category_id'=>$category->term_id,'category_name'=>$category->name,'slug'=>$category->slug,'subcategory'=>$subcategories));
            // $html .= '<li data-filter=".zoet" class="cat_list_gallary" data-value="'.$category->name.'"><a href="javascript:void(0)">'.$category->name.'</li>';
            if(!empty($subcategories)){
                array_push($mainCat,array('category_id'=>$category->term_id,'category_name'=>$category->name,'slug'=>$category->slug,'subcategory'=>$subcategories));
                $html .= '<li data-filter=".'.$category->slug.'" class="cat_list_gallary" data-value="'.$category->slug.'"><a href="javascript:void(0)">'.$category->name.'</li>';
            }
        }
    }
    $html .='</ul>';
    //  echo "<pre>";
    $allCat = [];
    foreach ($mainCat as $key => $value) {
        // echo "<pre>";print_r($value);die;
        $thumbnail_id = get_woocommerce_term_meta($value['category_id'], 'thumbnail_id', true );
        $image = wp_get_attachment_url( $thumbnail_id );
        if(empty($image)){
            $image = get_template_directory_uri().'/images/no-image.jpg';
        }
        $catname = $value['category_name'];
        $html .= '<div class="gallery-items row '.$value['slug'].'" style="display:none;">
                <div class="gallery-item '.$value['slug'].' col-md-4 col-sm-6 col-6">
                  <div class="gallery-image">
                    <a href="'.home_url('product-category/'.$value['slug']).'"><img src="'.$image.'" alt="gallery image" class="img-responsive "></a>
                  </div>
                  <h2>'.$catname.'</h2>

                <ul class="list style-type2">';
        // if(!empty($value['subcategory'])){
        // $html .= 'no data available....!!!';
        // }else{
        foreach ($value['subcategory'] as $key => $subcat) {
            $url = home_url('product-category/'.$subcat->slug);
            $html .= '<li><a href="'.$url.'">'.$subcat->name.'</a></li>';
            array_push($allCat,array('subcat_name'=>$subcat->name,'image'=>$image,'category_name'=>$catname,'slug'=>$subcat->slug));
        }
        $html .='</ul>
      </div></div>';
        // }
    }
    // $html .= '';
    $html .= '<div class="gallery-items row all">';
    // echo "<pre>";print_r($allCat);die;
    foreach ($mainCat as $key => $value) {
        $catname = $value['category_name'];
        $thumbnail_id = get_woocommerce_term_meta($value['category_id'], 'thumbnail_id', true );
        $image = wp_get_attachment_url( $thumbnail_id,'medium' );
        if(empty($image)){
            $image = get_template_directory_uri().'/images/no-image.jpg';
        }
        $html .= '<div class="gallery-item zoet col-md-4 col-sm-6 col-6">
      <div class="gallery-image">
        <a href="'.home_url('product-category/'.$value['slug']).'"><img src="'.$image.'" alt="gallery image" class="img-responsive"></a>
      </div>
      <h2>'.$catname.'</h2>';

//    $html .= '<ul class="list style-type2">';
//        foreach ($value['subcategory'] as $key => $subcat) {
//            $url = home_url('product-category/'.$subcat->slug);
//            $html .= '<li><a href="'.$url.'">'.$subcat->name.'</a></li>';
//            array_push($allCat,array('subcat_name'=>$subcat->name,'image'=>$image,'category_name'=>$catname));
//        }
//        $html .='</ul>';
    $html .= '</div>';
    }
    $html .='</div>';
    return $html;
}
add_shortcode('category_subcategory_list','category_subcategory_list');



/*
* brand list of alphabtic A to z wise display.
*/
// function brand_listing(){

// $html = '<section class="bg-white pad80 pt-0">
//         <div class="container ">
//             <div class="row">
//                 <div class="col-md-12">
//                     <div class="section-heading">
//                         <h1>Merken</h1>
//                         <h3>De merken die<br />Verbruggen aanbiedt</h3>
//                       </div>
//                   </div>
//               </div>
//               <div class="row">
//                   <div class="col-md-12 text-center">
//                     <div class="heading-bdr-style mb-3 "><h2 class="bg-white">Op aflabetische volgorde</h2></div>

//                     <ul class="nav nav-pills item-tab-nav albat mb-4 justify-content-between">';
//               foreach (range('a', 'z') as $char) {
//                 $active = '';
//                 if($char == 'l'){
//                   $active = 'active in';
//                 }
//                  $html .= '<li class="nav-item">
//                     <a class="nav-link '.$active.'" data-toggle="tab" href="#mer-'.$char.'">'.$char.'</a>
//                   </li>';
//               }
//   $html .= '</ul>';

//   $html .= ' <div class="tab-content">';

//       $brands_list = get_terms('pa_brand');
//       foreach (range('a', 'z') as $char) {
//         $active = '';
//         if($char == 'l'){
//           $active = 'active in';
//         }
//         $html .= '<div class="tab-pane '.$active.'" id="mer-'.$char.'">
//                     <ul class="list-style2 row text-left">';
//         foreach ($brands_list as $key => $value) {
//             if($char == lcfirst($value->name[0])){
//                 $url = get_site_url().'/shop/?filter_brand='.$value->slug;
//               $html .= '<li class="col-md-2dot4 col-3"><a href='."$url".'>'.ucfirst($value->name).'</a></li>';
//             }
//         }
//         $html .= '</ul></div>';
//       }
//   $html .= '</div>';

//   $html .='</div>
//           </div>
//         </div>
//     </section>';
//   return $html;
// }
function brand_listing(){
    $html = '<section class="bg-white pad80 pt-0">
        <div class="container ">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-heading">
                        <h1>Merken</h1>
                        <h3>De merken die<br />Verbruggen aanbiedt</h3>
                      </div>
                  </div>
              </div>
              <div class="row">
                  <div class="col-md-12 text-center">
                    <div class="heading-bdr-style mb-3 "><h2 class="bg-white">Op aflabetische volgorde</h2></div>

                    <ul class="nav nav-pills item-tab-nav albat mb-4 justify-content-between">';
    $list = [];
    foreach (range('a', 'z') as $char) {
        $brands_list = get_terms('pa_brand');
        $brands_val = [];
        foreach ($brands_list as $brand) {
            if($char == lcfirst($brand->name[0])){
                $value = $brand->name;
                $slug = $brand->slug;
                array_push($brands_val,array('name'=>$value,'slug'=>$slug));
            }
        }
        $active = '';
        if($char == 'l'){
            $active = 'active in';
        }
        $html .= '<li class="nav-item">
                    <a class="nav-link show '.$active.'" data-toggle="tab" href="#mer-'.$char.'">'.$char.'</a>
                  </li>';
        array_push($list,array('char'=>$char,'val'=>$brands_val));
    }

    $html .= '</ul>';
    $html .= ' <div class="tab-content">';
    foreach ($list as $key => $value){
        $active = '';
        if($value['char'] == 'l'){
            $active = 'active in';
        }
        $html .= '<div class="tab-pane '.$active.'" id="mer-'.$value['char'].'">
                    <ul class="list style-type2 list-style2 row text-left">';
        if($value['val']){
            foreach ($value['val'] as $key1 => $item) {
                $url = get_site_url().'/shop/?filter_brand='.$item['slug'];
                $html .= '<li class="col-md-2dot4 col-4"><a href='."$url".'>'.ucfirst($item['name']).'</a></li>';
            }
        }else{
            $html .= '<li class="col-md-2dot4 col-4"><a href="#">Geen gegevens beschikbaar</a></li>';
        }

        $html .= '</ul></div>';
    }
    $html .= '</div>';

    $html .='</div>
          </div>
        </div>
    </section>';
    return $html;
}
add_shortcode('brand_listing','brand_listing');



function user_email_check(){
    if(isset($_POST['email'])){
        $email = $_POST['email'];
        $user_exist = email_exists($email);
        if($user_exist){
            $user = 'true';
        }else{
            $user = 'false';
        }
    }else{
        $user = 'false';
    }
    echo $user;exit;
}
add_action('wp_ajax_user_email_check', 'user_email_check');
add_action('wp_ajax_nopriv_user_email_check', 'user_email_check');
/*
* for registration of the customer
*/
function create_new_user()
{

    $userdata = [];
    foreach ($_POST['form_data'] as $key => $value) {
        $userdata[$value['name']] = $value['value'];
    }
    $password = "abc@123";
    $username = str_replace(' ', '', $userdata['name']);
    $email = $userdata['email'];
    $user_id = wp_create_user($username,$password,$email);
    $name = explode(" ", $userdata['name']);
    if($user_id){
//        add_user_meta($user_id,'first_name',$name[0]);
//        add_user_meta($user_id,'last_name',$name[1]);
        update_user_meta($user_id,'first_name',$name[0]);
        update_user_meta($user_id,'last_name',$name[1]);
        add_user_meta($user_id,'telephone',$userdata['telephone']);
        add_user_meta($user_id,'company_number',$userdata['company_number']);
        add_user_meta($user_id,'vat',$userdata['vat']);
        add_user_meta($user_id,'bank_acc_no',$userdata['bank_acc_no']);
        add_user_meta($user_id,'bank_code',$userdata['bank_code']);
        add_user_meta($user_id,'eori',$userdata['eori']);
        add_user_meta($user_id,'company_name',$userdata['company_name']);
        add_user_meta($user_id,'street',$userdata['street']);
        add_user_meta($user_id,'house_no',$userdata['house_no']);
        add_user_meta($user_id,'addition',$userdata['addition']);
        add_user_meta($user_id,'postcode',$userdata['postcode']);
        add_user_meta($user_id,'city',$userdata['city']);

        add_user_meta($user_id,'postcode',$userdata['billing_postcode']);
        add_user_meta($user_id,'city',$userdata['billing_city']);

        add_user_meta($user_id,'landmark',$userdata['landmark']);
        $address_1  = $userdata['house_no'].','.$userdata['street'].','.$userdata['addition'];
        $address_2  = $userdata['landmark'].','.$userdata['city'].'-'.$userdata['postcode'];
        add_user_meta($user_id,'billing_address_1', $address_1);
        add_user_meta($user_id,'billing_address_2', $address_2);
        add_user_meta($user_id,'user_status',0);
        if($userdata['different_address'] == 'on'){
            add_user_meta($user_id,'shipping_street',$userdata['shipping_street']);
            add_user_meta($user_id,'shipping_house_no',$userdata['shipping_house_no']);
            add_user_meta($user_id,'shipping_addition',$userdata['shipping_addition']);
            add_user_meta($user_id,'shipping_postcode',$userdata['shipping_postcode']);
            add_user_meta($user_id,'shipping_city',$userdata['shipping_city']);
            add_user_meta($user_id,'shipping_landmark',$userdata['shipping_landmark']);
            $ship_address_1  = $userdata['shipping_house_no'].','.$userdata['shipping_street'].','.$userdata['shipping_addition'];
            $ship_address_2  = $userdata['shipping_landmark'].','.$userdata['shipping_city'].'-'.$userdata['shipping_postcode'];
            add_user_meta($user_id,'shipping_address_1', $ship_address_1);
            add_user_meta($user_id,'shipping_address_2', $ship_address_2);
        }else{
            add_user_meta($user_id,'shipping_street',$userdata['street']);
            add_user_meta($user_id,'shipping_house_no',$userdata['house_no']);
            add_user_meta($user_id,'shipping_addition',$userdata['addition']);
            add_user_meta($user_id,'shipping_postcode',$userdata['postcode']);
            add_user_meta($user_id,'shipping_city',$userdata['city']);
            add_user_meta($user_id,'shipping_landmark',$userdata['landmark']);
            $address_1  = $userdata['house_no'].','.$userdata['street'].','.$userdata['addition'];
            $address_2  = $userdata['landmark'].','.$userdata['city'].'-'.$userdata['postcode'];
            add_user_meta($user_id,'shipping_address_1', $address_1);
            add_user_meta($user_id,'shipping_address_2', $address_2);
        }
        add_user_meta($user_id,'industry',$userdata['industry']);
        add_user_meta($user_id,'message',$userdata['message']);
    }
    return $user_id;
}
add_action('wp_ajax_create_new_user', 'create_new_user');
add_action('wp_ajax_nopriv_create_new_user', 'create_new_user');


/*
 * for the custom change status of user by admin send email of set password link.
 * create user activation key for reset password direct new user.
 */

add_action( 'show_user_profile', 'crf_show_extra_profile_fields' );
add_action( 'edit_user_profile', 'crf_show_extra_profile_fields' );

function crf_show_extra_profile_fields( $user ) {
    ?>
    <h3><?php esc_html_e( 'Personal Information', 'crf' ); ?></h3>

    <table class="form-table">
        <tr>
            <th><label for="status"><?php esc_html_e( 'Status', 'crf' ); ?></label></th>
            <td>

                <select name="user_status">
                    <?php
                    $usermeta = get_user_meta($user->ID);
                    // echo "<pre>";print_r($usermeta['user_status']);die;
                    if($usermeta['user_status'][0] == 0){ ?>
                        <option value="0" selected>Deactive</option>
                        <option value="1">Active</option>
                        <option value="2">Denied</option>
                    <?php }else if($usermeta['user_status'][0] == 1){ ?>
                        <option value="0">Deactive</option>
                        <option value="1" selected>Active</option>
                        <option value="2">Denied</option>
                    <?php }else if($usermeta['user_status'][0] == 2){ ?>
                        <option value="0">Deactive</option>
                        <option value="1">Active</option>
                        <option value="2" selected>Denied</option>
                    <?php } ?>

                </select>
            </td>
        </tr>
    </table>
    <?php
}

add_action( 'personal_options_update', 'crf_update_profile_fields' );
add_action( 'edit_user_profile_update', 'crf_update_profile_fields' );

function crf_update_profile_fields( $user_id ) {
    if ( ! current_user_can( 'edit_user', $user_id ) ) {
        return false;
    }

    if ( ! empty( $_POST['user_status'] )) {
        $OldUserMeta = get_user_meta($user_id);

        update_user_meta( $user_id, 'user_status', $_POST['user_status'] );
        $usermeta = get_user_meta($user_id);

        $user = get_userdata($user_id);

        if($usermeta['user_status'][0] == 1){

            if($OldUserMeta['user_status'][0] != 1){

                // retrieve_password_custom($user->data->user_login);

                $user_login = sanitize_text_field($user->data->user_login);

                if (retrieve_password_custom($user_login)) {
                    nocache_headers();

                    header('Content-Type: '.get_bloginfo('html_type').'; charset='.get_bloginfo('charset'));

                    if ( defined( 'RELOCATE' ) && RELOCATE ) { // Move flag is set
                        if ( isset( $_SERVER['PATH_INFO'] ) && ($_SERVER['PATH_INFO'] != $_SERVER['PHP_SELF']) )
                            $_SERVER['PHP_SELF'] = str_replace( $_SERVER['PATH_INFO'], '', $_SERVER['PHP_SELF'] );

                        $url = dirname( set_url_scheme( 'http://' .  $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] ) );
                        if ( $url != get_option( 'siteurl' ) )
                            update_option( 'siteurl', $url );
                    }


                    //Set a cookie now to see if they are supported by the browser.
                    $secure = ( 'https' === parse_url( wp_login_url(), PHP_URL_SCHEME ) );
                    setcookie( TEST_COOKIE, 'WP Cookie check', 0, COOKIEPATH, COOKIE_DOMAIN, $secure );
                    if ( SITECOOKIEPATH != COOKIEPATH )
                        setcookie( TEST_COOKIE, 'WP Cookie check', 0, SITECOOKIEPATH, COOKIE_DOMAIN, $secure );
                }
            }
        }
        if($usermeta['user_status'][0] == 2){

            /* for the send email to customer  */
            $mailer = WC()->mailer();
            $mails = $mailer->get_emails();

            if ( ! empty( $mails ) ) {
                foreach ( $mails as $mail ) {
                    if ( $mail->id == 'customer_new_account' ) {
                        $mail->trigger($user_id);
                    }
                }
            }

        }
        if ( is_wp_error( $usermeta ) ) {
            echo 'Error.';die;
        }
    }else{
        $user = get_userdata($user_id);
        if($user->roles[0] == 'administrator'){
            update_user_meta( $user_id, 'user_status', 1);
        }else{
            update_user_meta( $user_id, 'user_status', $_POST['user_status'] );
        }
    }
}



function retrieve_password_custom($user_login) {
    $errors = new WP_Error();
    $_POST['user_login'] = $user_login;
    if ( empty( $_POST['user_login'] ) || ! is_string( $_POST['user_login'] ) ) {
        $errors->add('empty_username', __('<strong>ERROR</strong>: Enter a username or email address.'));
    } elseif ( strpos( $_POST['user_login'], '@' ) ) {
        $user_data = get_user_by( 'email', trim( wp_unslash( $_POST['user_login'] ) ) );
        if ( empty( $user_data ) )
            $errors->add('invalid_email', __('<strong>ERROR</strong>: There is no user registered with that email address.'));
    } else {
        $login = trim($_POST['user_login']);
        $user_data = get_user_by('login', $login);
    }

    /**
     * Fires before errors are returned from a password reset request.
     *
     * @since 2.1.0
     * @since 4.4.0 Added the `$errors` parameter.
     *
     * @param WP_Error $errors A WP_Error object containing any errors generated
     *                         by using invalid credentials.
     */
    do_action( 'lostpassword_post', $errors );

    if ( $errors->get_error_code() )
        return $errors;

    if ( !$user_data ) {
        $errors->add('invalidcombo', __('<strong>ERROR</strong>: Invalid username or email.'));
        return $errors;
    }

    // Redefining user_login ensures we return the right case in the email.
    $user_login = $user_data->user_login;
    $user_email = $user_data->user_email;
    $key = get_password_reset_key( $user_data );

    if ( is_wp_error( $key ) ) {
        return $key;
    }

    if ( is_multisite() ) {
        $site_name = get_network()->site_name;
    } else {
        /*
         * The blogname option is escaped with esc_html on the way into the database
         * in sanitize_option we want to reverse this for the plain text arena of emails.
         */
        $site_name = wp_specialchars_decode( get_option( 'blogname' ), ENT_QUOTES );
    }

    // $message = __( 'Someone has requested a password reset for the following account:' ) . "\r\n\r\n";
    // /* translators: %s: site name */
    // $message .= sprintf( __( 'Site Name: %s'), $site_name ) . "\r\n\r\n";
    // /* translators: %s: user login */
    // $message .= sprintf( __( 'Username: %s'), $user_login ) . "\r\n\r\n";
    // $message .= __( 'If this was a mistake, just ignore this email and nothing will happen.' ) . "\r\n\r\n";
    // $message .= __( 'To reset your password, visit the following address:' ) . "\r\n\r\n";
    // $message .= '<' . network_site_url( "my-account/lost-password/?key=$key&id=" .$user_data->ID, 'login' ) . ">\r\n";

    // /* translators: Password reset email subject. %s: Site name */
    // $title = sprintf( __( '[%s] Password Reset' ), $site_name );

    /**
     * Filters the subject of the password reset email.
     *
     * @since 2.8.0
     * @since 4.4.0 Added the `$user_login` and `$user_data` parameters.
     *
     * @param string  $title      Default email title.
     * @param string  $user_login The username for the user.
     * @param WP_User $user_data  WP_User object.
     */
    $title = apply_filters( 'retrieve_password_title', $title, $user_login, $user_data );

    /**
     * Filters the message body of the password reset mail.
     *
     * If the filtered message is empty, the password reset email will not be sent.
     *
     * @since 2.8.0
     * @since 4.1.0 Added `$user_login` and `$user_data` parameters.
     *
     * @param string  $message    Default mail message.
     * @param string  $key        The activation key.
     * @param string  $user_login The username for the user.
     * @param WP_User $user_data  WP_User object.
     */
    // $message = apply_filters( 'retrieve_password_message', $message, $key, $user_login, $user_data );


    $mailer = WC()->mailer();
    $cust_url = home_url('my-account/lost-password/?key='.$key.'&id=' .$user_data->ID);


    $message_body = __( '<table style="margin:0 auto;width: 750px;border-collapse:collapse; background:#FFFFFF; color:#31383e;" width="100%" border="0" cellspacing="0" cellpadding="0">
     <tr>
        	<td colspan="3" height="25">&nbsp;</td>
        </tr>
		<tr>
        	<td colspan="3">
            	<table  width="100%" border="0" cellspacing="0" cellpadding="0">
                	<tr>
                        <td colspan="3"><a href="'.home_url().'"><img src="'.get_template_directory_uri().'/emails/images/logo.png"  border="0"/></a></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
        	<td colspan="3" height="45">&nbsp;</td>
        </tr>
        <tr>
        	<td bgcolor="#ed3024" style="background-image:url(images/bg-food-vector.png); background-repeat:no-repeat; padding:15px 9%; background-position:left center;background-size: cover;">
            	<h1 style="color:#fff; font-size:50px; margin:0; line-height:1.1;font-family: Circular Std Bold, sans-serif;">Registratie compleet</h1>
            </td>
        </tr>
        <tr>
        	<td colspan="3" height="45">&nbsp;</td>
        </tr>
        <tr>
        	<td colspan="3" style=" padding:15px 9%;">
            	<h2 style="font-family: Circular Std Book, sans-serif; font-size:40px; margin:0 0 30px;">Welkom!</h2>
                <p style="font-family: Circular Std Book, sans-serif; font-size:18px; margin:0 0 30px; line-height:1.6;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                
                <p style="font-family: Circular Std Book, sans-serif; font-size:18px; margin:0 0 30px; line-height:1.6;">
                	Bedankt,<br />
					Het Verbruggen team.
                </p>
                <p style="font-family: Circular Std Book, sans-serif; font-size:18px; margin:0 0 30px; line-height:1.6;">Heeft u vragen? Dan kunt ons <a href="http://example.com/" style="color:#ed3024; text-decoration:underline;">mailen</a> of bellen op werkdagen van 8:30 tot 19:00 op: <font style="color:#ed3024; text-decoration:underline;">0485 451 220</font></p>
            </td>
        </tr>
        
        <tr>
        	<td colspan="3" style=" padding:0 9%;">
            	<table style="width:auto; margin:0 0 50px;border-radius:5px; -o-border-radius:5px; -webkit-border-radius:5px; background-color:#ed3024;" border="0" cellspacing="0" cellpadding="0">
                          <tbody><tr>
                            <td><a href="'.$cust_url.'" style="font-family: Circular Std Book, sans-serif; font-size:18px;display:block; padding:12px 25px;   color:#ffffff; text-decoration:none;">Naar account</a></td>
                          </tr>
                        </tbody></table>
            </td>
        </tr>
	</table>' );

    $message = $mailer->wrap_message(
    // Message head and message body.
        sprintf( __( 'Registratie compleet' )), $message_body );


    // Cliente email, email subject and message.
    $mailer->send( $user_email, sprintf( __( 'Registratie compleet' ) ), $message );


    // if ( $message && !wp_mail( $user_email, wp_specialchars_decode( $title ), $message ) )
    //     wp_die( __('The email could not be sent.') . "<br />\n" . __('Possible reason: your host may have disabled the mail() function.') );

    return true;
}

add_filter( 'woocommerce_email_subject_customer_reset_password', 'customizing_customer_reset_password_email_subject', 10, 2 );
function customizing_customer_reset_password_email_subject( $formated_subject, $order ){
    return __("Wachtwoord wijzigingsverzoek", "woocommerce");
}

add_filter( 'woocommerce_email_subject_customer_new_account', 'customizing_customer_new_account_email_subject', 10, 2 );
function customizing_customer_new_account_email_subject( $formated_subject, $order ){
    return __("Registratie afgekeurd", "woocommerce");
}

add_filter( 'woocommerce_email_subject_customer_completed_order', 'change_customer_email_subject', 10, 2 );
function change_customer_email_subject( $formated_subject, $order ){
    return __("Uw bestelling gerecupereerd", "woocommerce");
}


/*
* for the hit function using ajax request
*/
add_action('wp_head','ajaxurl');
function ajaxurl() {
    echo '<script type="text/javascript">
       var ajaxurl = "' . admin_url('admin-ajax.php') . '";
     </script>';
}

/*
* for user login
*/
function user_login(){

    $username = $_POST['username'];
    $password = $_POST['password'];

    $login_data = array();
    $login_data['user_login'] = sanitize_user($_POST['username']);
    $login_data['user_password'] = esc_attr($_POST['password']);

//    $auth =
//    echo "<pre>";
//    print_r($auth);die;
//    $user = wp_signon( $login_data, false );
    $user = wp_authenticate($username,$password);
    if ( is_wp_error($user) ) {
        $result = [
            "token" => 0,
            "url" => ""
        ];
    }else{
        $meta = get_user_meta($user->ID);
        if( in_array( "administrator", $user->roles ) ) {
            wp_clear_auth_cookie();
            do_action('wp_login', $username,$password);
            wp_set_current_user($user->ID);
            wp_set_auth_cookie($user->ID, true);
            // $redirect_to = home_url();
            if( in_array( "administrator", $user->roles ) ) {
                $redirect_to = home_url('/wp-admin');
            } else {
                $redirect_to = home_url();
            }
            $result = [
                "token" => 1,
                "url" => $redirect_to
            ];
        }else{
            if($meta['user_status'][0] == 1){
                wp_clear_auth_cookie();
                do_action('wp_login', $username,$password);
                wp_set_current_user($user->ID);
                wp_set_auth_cookie($user->ID, true);
                // $redirect_to = home_url();
                if( in_array( "administrator", $user->roles ) ) {
                    $redirect_to = home_url('/wp-admin');
                } else {
                    $redirect_to = home_url();
                }
                $result = [
                    "token" => 1,
                    "url" => $redirect_to
                ];

            }else{
                wp_clear_auth_cookie();
                $result = [
                    "token" => 2,
                    "url" => ""
                ];
            }
        }
    }



    echo json_encode($result);
    exit;
}
add_action('wp_ajax_user_login', 'user_login');
add_action('wp_ajax_nopriv_user_login', 'user_login');



/*
 * for the remove the password strength parameters in reset passwords.
*/
function wc_ninja_remove_password_strength() {
    if ( wp_script_is( 'wc-password-strength-meter', 'enqueued' ) ) {
        wp_dequeue_script( 'wc-password-strength-meter' );
    }
}
add_action( 'wp_print_scripts', 'wc_ninja_remove_password_strength', 100 );

/*
 * for the use of the wp-login page to redirect our custom login page.
 * Hook the appropriate WordPress action
 */
// add_action('init', 'prevent_wp_login');

// function prevent_wp_login() {
//     // WP tracks the current page - global the variable to access it
//     global $pagenow;
//     // Check if a $_GET['action'] is set, and if so, load it into $action variable
//     $action = (isset($_GET['action'])) ? $_GET['action'] : '';
//     // Check if we're on the login page, and ensure the action is not 'logout'
//     if( $pagenow == 'wp-login.php' && ( ! $action || ( $action && ! in_array($action, array('logout', 'lostpassword', 'rp', 'resetpass'))))) {
//         // Load the home page url
//         $page = home_url('login');
//         // Redirect to the home page
//         wp_redirect($page);
//         // Stop execution to prevent the page loading for any reason
//         exit();
//     }
// }



function arphabet_widgets_init( $html ) {

register_sidebar( array(
'name'          => 'Home right sidebar',
'id'            => 'home_right_1',
'class'         => 'list-sm mCustomScrollbar',
'before_widget' => '<div>',
'after_widget'  => '</div>',
'before_title'  => '<h2 class="heading2">',
'after_title'   => '</h2>',
) );

register_sidebar(array(
'name' => 'Footer Sidebar 1',
'id' => 'footer-sidebar-1',
'description' => 'Appears in the footer area',
'before_widget' => '<ul><li>',
'after_widget' => '</ul></li>',
'before_title' => '<h5>',
'after_title' => '</h5>',
));
register_sidebar(array(
'name' => 'Footer Sidebar 2',
'id' => 'footer-sidebar-2',
'description' => 'Appears in the footer area',
'before_widget' => '<ul><li>',
'after_widget' => '</ul></li>',
'before_title' => '<h5>',
'after_title' => '</h5>',
));
register_sidebar(array(
'name' => 'Footer Sidebar 3',
'id' => 'footer-sidebar-3',
'description' => 'Appears in the footer area',
'before_widget' => '<ul><li>',
'after_widget' => '</ul></li>',
'before_title' => '<h5>',
'after_title' => '</h5>',
));


}
add_action( 'widgets_init', 'arphabet_widgets_init' );




function custom_sidebar( $html ){
    return str_replace( __( '</li>', 'woocommerce' ), __( '<input type="checkbox"><span class="checkmark"></li>', 'woocommerce' ), $html );
}
add_action('dynamic_sidebar','custom_sidebar');



function replacing_template_loop_product_thumbnail() {
    // Remove product images from the shop loop
    remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10 );
    // Adding something instead
    function wc_template_loop_product_replaced_thumb() {
        global $product;
        $featured_image_link = "";
        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $product->get_id() ), 'single-post-thumbnail' );
        $url = $product->get_permalink();

        if(!empty($image))
            $featured_image_link = $image[0];

        echo "<a href='$url'><img src='$featured_image_link' class='img-responsive'></a>";
    }
    add_action( 'woocommerce_before_shop_loop_item_title', 'wc_template_loop_product_replaced_thumb', 10 );
}
add_action( 'woocommerce_init', 'replacing_template_loop_product_thumbnail');



function imgwrap(){
    global $product;
    $myproduct = $product->get_data();

    if(empty($myproduct['tag_ids'])){
        $aanbiedingenclass = "";
        $opclass = "";
    }
    else{
        foreach($myproduct['tag_ids'] as $Key=>$tag_id){
            $tag = get_term( $tag_id, "product_tag");
            $tagslug = $tag->slug;
            if($tagslug == "opop"){
                $opclass = "offer";
            }

            if($tagslug == "aanbieding"){
                $aanbiedingenclass = "aanbied";
            }
        }
    }


    $overallclass = $opclass." ".$aanbiedingenclass;
    echo '<div class="product-block '.$overallclass.'" ><div class="pro-img">';

}
add_action( 'woocommerce_before_shop_loop_item_title', 'imgwrap', 5, 2);


function img2wrap( $html ){
    echo "</div>";
}
add_action( 'woocommerce_before_shop_loop_item_title','img2wrap', 12, 2);


add_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_stock', 10 );
function woocommerce_template_loop_stock() {
    global $product;
    $myproduct = $product->get_data();

    foreach($myproduct['tag_ids'] as $Key=>$tag_id){
        $tag = get_term( $tag_id, "product_tag");
        $tagname = strtoupper($tag->name);
        echo '<div class="pro-tag">'.$tagname.'</div>';
    }
    /*if ( ! $product->managing_stock() && ! $product->is_in_stock() ){
        echo '<div class="pro-tag">OP = OP</div>';
    }*/
}



function wc_custom_replace_sale_text( $html ) {
    if(!Is_product()){
        /*return str_replace( __( '<span class="onsale">Sale!</span>', 'woocommerce' ), __( '<div class="pro-tag">AANBIEDING</div>', 'woocommerce' ), $html );*/
        return false;
    }
}
add_filter( 'woocommerce_sale_flash', 'wc_custom_replace_sale_text' );


remove_action('woocommerce_shop_loop_item_title','woocommerce_template_loop_product_title',10);
add_action('woocommerce_shop_loop_item_title','custom_product_title',10);
function custom_product_title()
{
    global $product;
    $url = $product->get_permalink();
    $title = $product->get_title();
    $qty = $product->get_attribute( 'pa_quantity' );


    echo "<div class='product-info'>
	            <h4 class='light'><a href='$url'>$title</a></h4>
	            <h4 class='product-qty'>$qty</h4>
            </div>";


}


function woo_show_excerpt_shop_page() {
    global $product;
    $brand = $product->get_attribute( 'pa_brand' );
    $url = $product->get_permalink();
    $size = $product->get_attribute('pa_size');
    if(empty($brand)){
        echo "<div class='product-title'><h4>&nbsp;</h4></div>";
    }else{
        echo "<div class='product-title'><h4><a href='$url '>$brand</a></h4></div>";
    }
    if(empty($size)){
        echo "<div class='product-size'><h4>&nbsp;</h4></div>";
    }else{
        echo "<div class='product-size'><h4>$size</h4></div>";
    }

    $targeted_id = $product->get_id();  //product id

    $value = 0;
    foreach ( WC()->cart->get_cart() as $cart_item ) {
        if($cart_item['product_id'] == $targeted_id ){
            $value =  $cart_item['quantity'];
            break; // stop the loop if product is found
        }
    }

    $Stockstatus = $product->get_stock_status();

    if( !$product->has_child() && $Stockstatus == "instock") {
        echo "<div class='it-input-qt'><div class='count-input'><a class='incr-btn' data-action='decrease' href='#'></a><input class='quantity' type='text' name='quantity' pattern='[0-9]*' value='$value'><a class='incr-btn' data-action='increase' href='#'></a></div></div>";
        if ( $price_html = $product->get_price_html()) {
            echo "<span class='item-price'>$price_html</span>";
        }
    }


    //  echo $product->post->post_excerpt;echo "<p></p>";
}
add_action( 'woocommerce_shop_loop_item_title', 'woo_show_excerpt_shop_page', 10 );




function after_main_content(){
    echo "</div>";
}
add_action( 'woocommerce_after_main_content', 'after_main_content', 10 );




remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );


add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 15 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 20 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 20 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50 );



remove_action( 'woocommerce_product_thumbnails', 'woocommerce_show_product_thumbnails', 20 );


function bbloomer_add_below_prod_gallery() {
    global $post;
    $product = wc_get_product( $post->ID );
    $attachment_ids = $product->get_gallery_attachment_ids();

    $StockQ = $product->get_stock_quantity();

    $stockstatus = $product->get_stock_status();

    $myproduct = $product->get_data();

    $tags = "";
    foreach($myproduct['tag_ids'] as $Key=>$tag_id){
        $tag = get_term( $tag_id, "product_tag");
        $tagname = strtoupper($tag->name);
        $tags.= '<div class="pro-tag">'.$tagname.'</div>';
    }


    $featured_image_link = "";
    $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );

    if(!empty($image))
    $featured_image_link = $image[0];




    echo "<div class='col-sm-5'>
                        <div class='pro-slide-bl'>  
                        $tags
                        <figure class='owl-carousel product-gallery'>";

    if($featured_image_link != ""){
        echo "<div class='item'><img src= '$featured_image_link' class='img-responsive'/></div>";
    }

    foreach( $attachment_ids as $attachment_id ) {
        $image_link = wp_get_attachment_url( $attachment_id );
        echo "<div class='item'><img src= '$image_link' class='img-responsive'/></div>";
    }

    $path = get_stylesheet_directory_uri();
    echo "
                        </figure>
                        </div>
                    </div>";
}
//add_action( 'woocommerce_after_single_product_summary' , 'bbloomer_add_below_prod_gallery', 5 );
add_action( 'my_custom_gallery' , 'bbloomer_add_below_prod_gallery', 5 );



/*function bbloomer_custom_action() {
    echo 'TEST';
}
add_action( 'woocommerce_single_product_summary', 'bbloomer_custom_action', 60 );*/

remove_action( 'woocommerce_before_single_product', 'wc_print_notices', 10 );

add_action( 'woocommerce_before_single_product', 'bbloomer_custom_action2', 5 );
function bbloomer_custom_action2() {
    echo "<section class='bg-white pad80 pb-5'>
  <div class='container'>
      <div class='single-product-wrap'>
    ";
}



remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );

add_action( 'woocommerce_after_single_product_summary', 'before_description', 5 );
function before_description() {
    echo "<div class='pad80 pb-0'>";
}

add_action( 'woocommerce_after_single_product', 'end_single_product', 5 );
function end_single_product() {
    echo "</div>";
}



add_filter( 'woocommerce_product_description_heading', 'bbloomer_rename_description_tab_heading' );
function bbloomer_rename_description_tab_heading() {
    return 'Product informatie';
}






/**
 * Remove item from product on ajax call
 */
function remove_item_from_cart() {
    global $woocommerce;
    $cart = WC()->instance()->cart;
    $id = $_POST['product_id'];
    $quantity = $_POST['quantity'];

    if(isset($_POST['variationid'])){

        foreach( WC()->cart->cart_contents as $prod_in_cart ) {
            if($prod_in_cart['variation_id'] == $_POST['variationid']){
                wc()->cart->remove_cart_item($prod_in_cart['key']);
            }
        }

        $variationarray  = wc_get_product_variation_attributes($_POST['variationid']);
        $woocommerce->cart->add_to_cart( $id, $quantity, $_POST['variationid'],$variationarray);
        return true;
    }
    else{
        $cart_id = $cart->generate_cart_id($id);
        $cart_item_id = $cart->find_product_in_cart($cart_id);


        if($cart_item_id){
            $cart->set_quantity($cart_item_id, $quantity);
            return true;
        }
        return false;
    }
}

add_action('wp_ajax_remove_item_from_cart', 'remove_item_from_cart');
add_action('wp_ajax_nopriv_remove_item_from_cart', 'remove_item_from_cart');



/**
 * Ajax add to cart functionality
 */
add_action('wp_ajax_myajax', 'myajax_callback');
add_action('wp_ajax_nopriv_myajax', 'myajax_callback');

/**
 * AJAX add to cart.
 */
function myajax_callback() {
    ob_start();

    $cart = WC()->instance()->cart;
    $id = $_POST['productid'];
    $cart_id = $cart->generate_cart_id($id);
    $cart_item_id = $cart->find_product_in_cart($cart_id);

    if($cart_item_id){
        $cart->set_quantity($cart_item_id, 0);
    }

    //$product_id        = 264;
    $product_id        = $_POST['productid'];
    $quantity          = $_POST['quantity'];
    $variationid = '';
    if(isset($_POST['variationid'])){
        $variationid = $_POST['variationid'];
        foreach( WC()->cart->cart_contents as $prod_in_cart ) {
            if($prod_in_cart['variation_id'] == $_POST['variationid']){
                wc()->cart->remove_cart_item($prod_in_cart['key']);
            }
        }
    }


    $passed_validation = apply_filters( 'woocommerce_add_to_cart_validation', true, $product_id, $quantity );
    $product_status    = get_post_status( $product_id );

    if ( $passed_validation && WC()->cart->add_to_cart( $product_id, $quantity ,$variationid  ) && 'publish' === $product_status ) {

        do_action( 'woocommerce_ajax_added_to_cart', $product_id );

        wc_add_to_cart_message( $product_id );

    } else {

        // If there was an error adding to the cart, redirect to the product page to show any errors
        $data = array(
            'error'       => true,
            'product_url' => apply_filters( 'woocommerce_cart_redirect_after_error', get_permalink( $product_id ), $product_id )
        );

        wp_send_json( $data );

    }

    die();

}



/**
 * Counts items of cart
 */
function count_cart() {
    $count = 0;
    foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
        /*echo "<pre>";
        print_r($cart_item);die;*/
        $count += $cart_item['quantity'];
    }
    echo $count;
}
add_action('wp_ajax_count_cart','count_cart');
add_action('wp_ajax_nopriv_count_cart','count_cart');



/**
 * Gets product price when given product id
 */
function getproductprice(){
    $productid = $_POST['productid'];
    $product = wc_get_product($productid);
    echo $product->get_price();
    wp_die();
}
add_action('wp_ajax_getproductprice','getproductprice');
add_action('wp_ajax_nopriv_getproductprice','getproductprice');


/**
 * Get carttotal for ajax update cart
 */
function getcarttotal(){
    global $woocommerce;
    $total = 	floatval( preg_replace( '#[^\d.]#', '', $woocommerce->cart->get_total( ) ) );
    echo $total;
    wp_die();
}
add_action('wp_ajax_getcarttotal','getcarttotal');
add_action('wp_ajax_nopriv_getcarttotal','getcarttotal');


/**
 * Creates order in woocommerce
 */
function create_vip_order() {

    $user = wp_get_current_user();
    $usermeta = get_user_meta($user->data->ID);

    $billingaddress = array(
        'first_name' => $usermeta['billing_first_name'][0],
        'last_name'  => $usermeta['billing_last_name'][0],
        'company'    => $usermeta['billing_company'][0],
        'email'      => $usermeta['billing_email'][0],
        'phone'      => $usermeta['billing_phone'][0],
        'address_1'  => $usermeta['billing_address_1'][0],
        'address_2'  => $usermeta['billing_address_2'][0],
        'city'       => $usermeta['billing_city'][0],
        'state'      => $usermeta['billing_state'][0],
        'postcode'   => $usermeta['billing_postcode'][0],
        'country'    => $usermeta['billing_country'][0]
    );

    if(!empty($usermeta['shipping_address_1'][0])){
        $shippingaddress = array(
            'first_name' => $usermeta['shipping_first_name'][0],
            'last_name'  => $usermeta['shipping_last_name'][0],
            'company'    => $usermeta['shipping_company'][0],
            'address_1'  => $usermeta['shipping_address_1'][0],
            'address_2'  => $usermeta['shipping_address_2'][0],
            'city'       => $usermeta['shipping_city'][0],
            'state'      => $usermeta['shipping_state'][0],
            'postcode'   => $usermeta['shipping_postcode'][0],
            'country'    => $usermeta['shipping_country'][0]
        );
    }
    else{
        $shippingaddress = $billingaddress;
    }


    $address = array(
        'first_name' => 'Fresher',
        'last_name'  => 'StAcK OvErFloW',
        'company'    => 'stackoverflow',
        'email'      => 'test@test.com',
        'phone'      => '777-777-777-777',
        'address_1'  => '31 Main Street',
        'address_2'  => '',
        'city'       => 'Chennai',
        'state'      => 'TN',
        'postcode'   => '12345',
        'country'    => 'IN'
    );

    $order = wc_create_order(array('customer_id' => $user->data->ID));
    foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
        $productid = $cart_item['product_id'];
        $qty = $cart_item['quantity'];
        $order->add_product( wc_get_product( $productid ), $qty ); //(get_product with id and next is for quantity)
    }
    $order->set_address( $billingaddress, 'billing' );
    $order->set_address( $shippingaddress, 'shipping' );
    $order->calculate_totals();
    WC()->cart->empty_cart();

    $myorder = $order->get_data();
    $orderid = $myorder['id'];
    setcookie('orderid', $orderid, time() + (86400 * 30), "/"); // 86400 = 1 day


    /* for the send email to customer  */
    $mailer = WC()->mailer();
    $mails = $mailer->get_emails();
    if ( ! empty( $mails ) ) {
        foreach ( $mails as $mail ) {
            if ( $mail->id == 'customer_completed_order' ) {
                $mail->trigger( $orderid );
            }
        }
    }

}
add_action('wp_ajax_create_order', 'create_vip_order');





/**
 * Removes full item from cart
 */
function removeitem(){
    global $woocommerce;
    $cart = WC()->instance()->cart;
    $id = $_POST['productid'];

    if(isset($_POST['variationid'])){
        foreach( WC()->cart->cart_contents as $prod_in_cart ) {
            if($prod_in_cart['variation_id'] == $_POST['variationid']){
                wc()->cart->remove_cart_item($prod_in_cart['key']);
            }
        }
    }
    else{
        $cart_id = $cart->generate_cart_id($id);
        $cart_item_id = $cart->find_product_in_cart($cart_id);


        if($cart_item_id){
            $cart->set_quantity($cart_item_id, 0);
            return true;
        }
        return false;
    }
}
add_action('wp_ajax_removeitem','removeitem');
add_action('wp_ajax_nopriv_removeitem','removeitem');

/*
 *  for the remove company field from jobs create
 */

add_filter( 'submit_job_form_fields', 'gma_custom_submit_job_form_fields' );

function gma_custom_submit_job_form_fields( $fields ) {

    unset($fields['company']['company_name']);
    unset($fields['company']['company_website']);
    unset($fields['company']['company_tagline']);
    unset($fields['company']['company_video']);
    unset($fields['company']['company_twitter']);
    unset($fields['company']['company_logo']);

    return $fields;
}

/*
 *  for the feaured job sticky first to disable
 */

add_filter( 'submit_job_form_fields', 'frontend_add_salary_field' );
function frontend_add_salary_field( $fields ) {
    $fields['job']['pw_hours'] = array(
        'label'       => __( 'working hours p/w', 'job_manager' ),
        'type'        => 'text',
        'required'    => true,
        'placeholder' => 'e.g. 30 to 40 hrs.',
        'priority'    => 7
    );
    return $fields;
}

/*
* for the feaured job sticky first to disable
*/

add_filter ( 'get_job_listings_query_args', 'unstick_featured_jobs' );

function unstick_featured_jobs( $query_args ) {
    $query_args['orderby'] = 'date';
    $query_args['order'] = 'DESC';
    return $query_args;
}

/*
 * for the getting featured jobs display
 */
function featured_jobs_show_data() {
    $query = new WP_Query( array( 'post_type' => 'job_listing' ) );
    $posts = $query->posts;
    $html = '';
    foreach($posts as $post) {
        $meta = get_post_meta($post->ID);
        if($meta['_featured'][0] == 1){
            if($meta['_pw_hours']){
                $hours = $meta['_pw_hours'][0];
            }else{
                $hours = 10;
            }
            $type = wpjm_the_job_types();
            $html .= '<div class="row">
        	<div class="col-md-12">
            	<h1 class="mb-4">Uitgelichte vacature</h1>
                <div class="heading-block-red">
                	<h1 class="heading1">
                    	'.$post->post_title.'
                    </h1>
                </div>
                
                <div class="vacancy-block">
                	<div class="row">
                    	<div class="col-md-8">
                            <h4 class="text-li-black mb-5 paragraph">
                            '.$post->post_content.'
                            </h4>                          
                        </div>
                        
                        <div class="col-md-4">
                        	<ul class="job-side-li">
                            	<li>
                                	<h3>Locatie</h3>
		                            <h4>'.$meta['_job_location'][0].'</h4>
                                </li>
                                
                                <li>
                                	<h3>Aantal uren</h3>
		                            <h4>'.$type.' - '.$hours.' uur p/w</h4>
                                </li>
                                
                                <li>
                                	<h3>Publicatiedatum</h3>
		                            <h4>'.date('d-m-Y',strtotime($post->post_date)).'</h4>
                                </li>
                            </ul>
                        </div>
                    </div>
                    
                    <div class="row">
                    	<div class="col-md-8">
                        	<a href="'.home_url('job/'.$post->post_name).'" class="btn-line-hover h3 a-link">Meer informatie</a>
                        </div>
                        <div class="col-md-4">
                        	<a href="'.home_url('job/'.$post->post_name).'" class="btn btn-primary btn-lg">Direct solliciteren</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>';
        }else{
            $html .=  '';
        }

    }

    return $html;
}
add_shortcode('featured_jobs_show_data','featured_jobs_show_data');

/*
 * for the getting limited latest jobs display.
 */
//function latest_jobs_show_data() {
//    $query = new WP_Query( array( 'post_type' => 'job_listing','posts_per_page' => 2 ));
//    $posts = $query->posts;
//    $html = '';
//    if($posts){
//        foreach($posts as $post) {
//            $meta = get_post_meta($post->ID);
//            if($meta['_pw_hours']){
//                $hours = $meta['_pw_hours'][0];
//            }else{
//                $hours = 10;
//            }
//            $type = wpjm_the_job_types();
//            $html .= '<div class="vacancy-view-block">
//                	<h2 class="mb-4"><strong>'.$post->post_title.'</strong> - '.$type.'</h2>
//                    '.$post->post_content.'
//                    <div class="row">
//                        <div class="col-md-4">
//                            <h3 class="bold">Locatie</h3>
//                            <h4>'.$meta['_job_location'][0].'</h4>
//                        </div>
//
//                        <div class="col-md-4">
//                            <h3 class="bold">Aantal uren</h3>
//                            <h4>'.$type.' - '.$hours.' uur p/w</h4>
//                        </div>
//
//                        <div class="col-md-4 text-sm-right">
//                        <a href="'.home_url('job/'.$post->post_name).'" class="btn btn-primary btn-md">Bekijk</a></div>
//                    </div>
//                </div>';
//        }
//    }else{
//        $html .=  '';
//    }
//    $countPost = wp_count_posts('job_listing');
//    if($countPost->publish > 2){
//        $html .= '<section class="pad80 text-center">
//	            <a href="'.home_url('jobs').'" class="btn btn-primary btn-lg">Meer vacatures</a>
//            </section>';
//    }
//
//    return $html;
//}
function latest_jobs_show_data() {
    $query = new WP_Query( array( 'post_type' => 'job_listing','posts_per_page' => 2 ));
    $posts = $query->posts;
    $html = '';
    if($posts){
        foreach($posts as $count => $post) {
            $meta = get_post_meta($post->ID);
            if($meta['_pw_hours']){
                $hours = $meta['_pw_hours'][0];
            }else{
                $hours = 10;
            }
            $type = wpjm_the_job_types();
            $html .= '<div class="heading-block-red pad-3">
                	<h1 class="f-30">
                	'.$post->post_title.'</strong>  '.$type.'</h1></div>
                <div class="vacancy-block">
                	<div class="row">
                    	<div class="col-md-8">
                            '.$post->post_content.'
                        </div>
                        <div class="col-md-4">
                        	<ul class="job-side-li">
                            	<li>
                                    <h3 class="bold">Locatie</h3>
                                    <h4>'.$meta['_job_location'][0].'</h4>
                                </li>
                                <li>
                                    <h3 class="bold">Aantal uren</h3>
                                    <h4>'.$type.' - '.$hours.' uur p/w</h4>
                                </li>
                                <li>
                                      <h3>Publicatiedatum</h3>
                                      <h4>18 02 2019</h4>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 col-6">
                            <a href="'.home_url('job/'.$post->post_name).'" class="h3 a-link">Meer informatie</a>
                        </div>
                        <div class="col-md-4 col-6">
                            <a href="'.home_url('job/'.$post->post_name).'" class="btn btn-primary btn-md">Bekijk</a>
                        </div>
                    </div>
                </div>';
        }
    }else{
        $html .=  '';
    }

    $countPost = wp_count_posts('job_listing');
    if($countPost->publish > 2){
        $html .= '<section class="pad80 pt-0 text-center">
	            <a href="'.home_url('jobs').'" class="btn btn-primary btn-lg">Alle vacatures</a>
            </section>';
    }

    return $html;
}
add_shortcode('latest_jobs_show_data','latest_jobs_show_data');




/**
 * this function sets cookie for html to show for guest user
 */
function guest_user_register(){
    WC()->cart->empty_cart();
    setcookie('guestuserhtml','true', time() + (3600 * 30), "/"); // 86400 = 1 day
}
add_action('wp_ajax_guest_user_regi','guest_user_register');
add_action('wp_ajax_nopriv_guest_user_regi','guest_user_register');

add_action( 'wp_footer', 'redirect_cf7' );

function redirect_cf7() {
//    global $post;
//    $post_slug=$post->post_name;
//    $url = home_url();
    ?>
    <script type="text/javascript">
        document.addEventListener( 'wpcf7mailsent', function( event ) {
            console.info(event.detail.contactFormId);
            if('455' == event.detail.contactFormId){
//              location = '<?php //echo $url; ?>//';
                jQuery('.app_form_sollicitern_submit_data').css('display','none');
                jQuery('.details_vacancy_jobs').css('display','none');
                jQuery('html,body').scrollTop(0);
                jQuery('.vacancy-submited-success').css('display','block');
            }
        }, false );
    </script>
    <?php
}

/* user's email check on edit profile */
function current_user_email_check(){
    if(isset($_POST['email'])){
        $email = $_POST['email'];
        $userdata = get_user_by('id',$_POST['userid']);
        $user_exist = email_exists($email);
//        echo "<pre>";print_r($user->data->user_email);die;
        if($user_exist){
            if($userdata->data->user_email == $email){
                $user = 'true';
            }
            else{
                $user = 'false';
            }
        }else{
            $user = 'true';
        }
    }else{
        $user = 'false';
    }
    echo $user;die;
}
add_action('wp_ajax_current_user_email_check', 'current_user_email_check');
add_action('wp_ajax_nopriv_current_user_email_check', 'current_user_email_check');

/* update user's profile info*/
function save_profile()
{
    $userdata = wp_get_current_user();
    //echo "<pre>";print_r($userdata);die;

    foreach ($_POST['form_data'] as $key => $value) {
        $data[$value['name']] = $value['value'];
    }
    //echo "<pre>";print_r($data);die;
    $username = str_replace(' ', '', $data['name']);
    $email = $data['email'];
    $phone = $data['telephone'];
    $args = array(
        'ID'         => $userdata->data->ID,
        'user_email' => $email ,
        'user_login' => $username,
        'display_name' => $username,
        'user_nicename' => strtolower($username),
    );
    wp_update_user( $args );
    update_user_meta($userdata->data->ID,'telephone',$phone);
    update_user_meta($userdata->data->ID,'billing_email',$email);
    update_user_meta($userdata->data->ID,'nickname',$username);
    /*$user_id = $userdata->ID;
    $name = explode(" ", $userdata['name']);
    if($user_id){
        update_user_meta($user_id,'first_name',$name[0]);
        update_user_meta($user_id,'last_name',$name[1]);
        update_user_meta($userdata->ID,'telephone',$userdata['telephone']);
        update_user_meta($user_id,'house_no',$userdata['house_no']);
        update_user_meta($user_id,'addition',$userdata['addition']);
        update_user_meta($user_id,'postcode',$userdata['postcode']);
        update_user_meta($user_id,'city',$userdata['city']);
        update_user_meta($user_id,'landmark',$userdata['landmark']);
        update_user_meta($user_id,'shipping_street',$userdata['street']);
        update_user_meta($user_id,'shipping_house_no',$userdata['house_no']);
        update_user_meta($user_id,'shipping_addition',$userdata['addition']);
        update_user_meta($user_id,'shipping_postcode',$userdata['postcode']);
        update_user_meta($user_id,'shipping_city',$userdata['city']);
        update_user_meta($user_id,'shipping_landmark',$userdata['landmark']);
    }*/
    $outputdata['email'] = $email;
    $outputdata['name'] = $username;
    $outputdata['phone'] = $phone;
    $outputdata['id'] = $userdata->data->ID;
    wp_send_json( $outputdata );
}
add_action('wp_ajax_save_profile', 'save_profile');
add_action('wp_ajax_nopriv_save_profile', 'save_profile');

/* update user's address*/
function save_address()
{
    $userdata = wp_get_current_user();
    //echo "<pre>";print_r($userdata->ID);die;

    foreach ($_POST['form_data'] as $key => $value) {
        $data[$value['name']] = $value['value'];
    }
    //echo "<pre>";print_r($data);die;
    $usermeta = get_user_meta($userdata->ID);
    $user_id = $userdata->ID;
    $billing_address1 = $data['street'] . "," . $data['house_no'] ."," .$data['addition'];
    $billing_address2 = $data['postcode'] ."," . $data['city'] ."," .$data['landmark'];
    //echo "<pre>";print_r($billing_address2);die;

    if($user_id){
        update_user_meta($user_id,'street',$data['street']);
        update_user_meta($user_id,'house_no',$data['house_no']);
        update_user_meta($user_id,'addition',$data['addition']);
        update_user_meta($user_id,'postcode',$data['postcode']);
        update_user_meta($user_id,'city',$data['city']);
        update_user_meta($user_id,'landmark',$data['landmark']);
        update_user_meta($user_id,'billing_address_1',$billing_address1);
        update_user_meta($user_id,'billing_address_2',$billing_address2);
        if($usermeta['billing_address_1'][0] == $usermeta['shipping_address_1'][0] && $usermeta['billing_address_2'][0] == $usermeta['shipping_address_2'][0]){
            update_user_meta($user_id,'shipping_street',$data['street']);
            update_user_meta($user_id,'shipping_house_no',$data['house_no']);
            update_user_meta($user_id,'shipping_addition',$data['addition']);
            update_user_meta($user_id,'shipping_postcode',$data['postcode']);
            update_user_meta($user_id,'shipping_city',$data['city']);
            update_user_meta($user_id,'shipping_landmark',$data['landmark']);
            update_user_meta($user_id,'shipping_address_1',$billing_address1);
            update_user_meta($user_id,'shipping_address_2',$billing_address2);
        }
    }
    $outputdata['id'] = $userdata->data->ID;
    $outputdata['billing_address'] = $billing_address1 . "," . $billing_address2;
    wp_send_json( $outputdata );
}
add_action('wp_ajax_save_address', 'save_address');
add_action('wp_ajax_nopriv_save_address', 'save_address');


/*save bank details*/
function save_bank_details(){
//    echo "<pre>";
//    print_r($_POST);die;

    $userdata = wp_get_current_user();
    foreach ($_POST['form_data'] as $key => $value) {
        $data[$value['name']] = $value['value'];
    }
//    echo "<pre>";print_r($data);die;
    $usermeta = get_user_meta($userdata->ID);
    $user_id = $userdata->ID;
    if($user_id) {
        update_user_meta($user_id, 'bank_acc_no', $data['bank_acc_no']);
        update_user_meta($user_id, 'vat', $data['vat']);
        update_user_meta($user_id, 'bank_code', $data['bank_code']);
        update_user_meta($user_id, 'eori', $data['eori']);
    }
    $outputdata = $data;
    wp_send_json( $outputdata );
    wp_die();
}
add_action('wp_ajax_save_bank_details', 'save_bank_details');
add_action('wp_ajax_nopriv_save_bank_details', 'save_bank_details');

/**
 * Generate breadcrumbs
 * @author CodexWorld
 * @authorURL www.codexworld.com
 */
function get_breadcrumb() {
    $id = get_the_ID();
    $terms = get_the_terms( $id, 'product_cat' );
    $mycount = count($terms);
    if($mycount == 1){
        foreach ($terms as $key=>$cat){
            echo '<li><a href="'.home_url().'/product-category/'.$cat->slug.'" class="btn-back" style="cursor:pointer;border:none;text-decoration : underline;">Terug</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>';
        }
    }
    else{
        echo '<li><a href="'.home_url().'/shop" class="btn-back" style="cursor:pointer;border:none;text-decoration : underline;">Terug</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>';
    }

    echo '<a href="'.home_url().'" rel="nofollow">Home</a>';
    if (is_category() || is_single()) {
//        echo "&nbsp;&nbsp;&#187;&nbsp;&nbsp;";
        the_category(' &bull; ');
        if (is_single()) {
            echo " &nbsp;&nbsp;&#187;&nbsp;&nbsp; ";
            $id = get_the_ID();
            $terms = get_the_terms( $id, 'product_cat' );
//            print_r($terms);
            $i = 1;
            foreach ($terms as $key=>$cat){
                if($i > 1){echo ","; }
                echo '<a href="'.home_url().'/product-category/'.$cat->slug.'" rel="nofollow">'.$cat->name.'</a>';
                $i++;
            }
            echo "&nbsp;&nbsp;&#187;&nbsp;&nbsp;";
            the_title();
        }
    } elseif (is_page()) {
        echo "&nbsp;&nbsp;&#187;&nbsp;&nbsp;";
        echo the_title();
    } elseif (is_search()) {
        echo "&nbsp;&nbsp;&#187;&nbsp;&nbsp;Search Results for... ";
        echo '"<em>';
        echo the_search_query();
        echo '</em>"';
    }

}


/**
 * Returns all products for shop product for infinite scroll.
 */
add_action( 'pre_get_posts', 'custom_pre_get_posts' );
function custom_pre_get_posts($query) {
    if ( is_woocommerce() ) {
        $query->set('posts_per_page', -1);
    }

    return $query;
}

/**
 * Removes pagination
 */
remove_action( 'woocommerce_after_shop_loop', 'woocommerce_pagination', 10 );


/**
 * Changes number of related products
 */
add_filter( 'woocommerce_output_related_products_args', 'jk_related_products_args', 20 );
function jk_related_products_args( $args ) {
    $args['posts_per_page'] = 3; // 4 related products
    //$args['columns'] = 2; // arranged in 2 columns
    return $args;
}


/**
 * ajax for cart overlay
 */
add_action('wp_ajax_cart_overlay','cart_overlay');
add_action('wp_ajax_nopriv_cart_overlay', 'cart_overlay');
function cart_overlay(){
    echo do_shortcode( '[woocommerce_cart]' );
    wp_die();
}


add_filter( 'woocommerce_price_trim_zeros', '__return_false' );




