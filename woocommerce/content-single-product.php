<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */
defined( 'ABSPATH' ) || exit;
/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */
do_action( 'woocommerce_before_single_product' );
if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>
	<div id="product-<?php the_ID(); ?>" <?php wc_product_class(); ?>>
		<?php
		/**
		 * Hook: woocommerce_before_single_product_summary.
		 *
		 * @hooked woocommerce_show_product_sale_flash - 10
		 * @hooked woocommerce_show_product_images - 20
		 */
		do_action( 'woocommerce_before_single_product_summary' );
		?>
		<div class="single-product-wrap">
			<div class="row">
				<?php do_action('my_custom_gallery'); ?>
				<div class="col-sm-7">
					<div class="product-summary">
						<?php
						/**
						 * Hook: woocommerce_single_product_summary.
						 *
						 * @hooked woocommerce_template_single_title - 5
						 * @hooked woocommerce_template_single_rating - 10
						 * @hooked woocommerce_template_single_price - 10
						 * @hooked woocommerce_template_single_excerpt - 20
						 * @hooked woocommerce_template_single_add_to_cart - 30
						 * @hooked woocommerce_template_single_meta - 40
						 * @hooked woocommerce_template_single_sharing - 50
						 * @hooked WC_Structured_Data::generate_product_data() - 60
						 */
						do_action( 'woocommerce_single_product_summary' );
						?>
					</div>
				</div>
			</div>
			<?php
			if ( is_user_logged_in() ) {

			}else{?>
				<section class="bg-white pad80 pb-0">
					<div class="">
						<div class="center-box-size">
							<div class="section-heading">
								<h1>Actuele prijs zien?</h1>
								<h3>Registreer u vandaag als klant en krijg toegang tot het grootste <br />en meest exclusive assortiment voor uw onderneming.</h3>
							</div>
							<div class="product-service-bl bg-food-vect">
								<div class="register-service">
									<div class="regis-form-widgt">

										<h5>Geniet van extra veel voordeel<br />
											als klant van Verbruggen</h5>
										[contact-form-7 id="62" title="register_home"]</div></div></div></div></div></section>
			<?php }?>
			<?php
			/**
			 * Hook: woocommerce_after_single_product_summary.
			 *
			 * @hooked woocommerce_output_product_data_tabs - 10
			 * @hooked woocommerce_upsell_display - 15
			 * @hooked woocommerce_output_related_products - 20
			 */
			do_action( 'woocommerce_after_single_product_summary' );
			?>
		</div>
	</div>
<?php do_action( 'woocommerce_after_single_product' ); ?>