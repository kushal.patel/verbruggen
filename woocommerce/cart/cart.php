<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

defined( 'ABSPATH' ) || exit;

?>
<style>
    .cart-sticky-cust{
        height: 71vh;
        overflow-y: scroll;
    }

</style>
<section class="bg-white pad80 pt-0">
    <div class="container" style="position: sticky;top: 0px;z-index: 999;">
        <div class="heading-block-red pad-3">
            <div class="row">
                <div class="col-sm-12 text-left">
                    <h1>Boodschappenlijst</h1>
                    <a href="javascript:void(0);" class="icon-close close-cart close-cart-overlay"></a>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
<?php
$mycoveredproducts = array();
$productlist = "";
$samecatlist = []; //for mobile ui
foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
    $productids = array();
    $_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
    $product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

    if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
        $product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
        ?>
        <div class="mobile-cart">
            <?php if(!in_array($product_id,$samecatlist)) {?>
                <div class="cart-pro-list">
                    <div class="row flex-row-reverse">
                        <?php
                        $catarray = array();
                        $terms = get_the_terms($_product->get_id(), 'product_cat');
                        foreach ($terms as $term) {
                            array_push($catarray, $term->name);
                        }
                        $currentproductid = $_product->get_id();
                        foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
                            $cartproductid = $cart_item['product_id'];
                            if (!in_array($cartproductid, $samecatlist)) {
                                if ($currentproductid != $cartproductid) {
                                    $terms = get_the_terms($cartproductid, 'product_cat');
                                    $newcatarray = array();
                                    foreach ($terms as $term) {
                                        array_push($newcatarray, $term->name);
                                    }

                                    if ($catarray == $newcatarray) {
                                        array_push($productids, $cartproductid);
                                        if (!in_array($currentproductid, $samecatlist)) {
                                            array_push($samecatlist, $currentproductid);
                                        }
                                        if (!in_array($cartproductid, $samecatlist)) {
                                            array_push($samecatlist, $cartproductid);
                                        }
                                    }
                                } else {
                                    array_push($productids, $currentproductid);
                                }
                            }
                        }
                        ?>
                        <div class="col-sm-4 text-sm-right">
                            <div class="pro-qty h4"><span>Aantal producten soorten:</span> <strong><?php echo count($productids);?></strong></div>
                        </div>
                        <div class="col-sm-8">
                            <h1 class="mb-3 cart-it-title" style="text-align: left;">
                                <?php
                                $i = 1;
                                foreach ($catarray as $term) {
                                    if ($i > 1) {
                                        echo ",";
                                    }
                                    echo $term;
                                    $i++;
                                }
                                ?>
                            </h1>
                        </div>
                    </div>

                    <!--Logic for finding same category-products-->
                    <?php
                    if (count($productids) > 1) {
                        ?>
                        <div class="cart-items owl-carousel product-gallery">
                            <?php foreach ($productids as $pid) {

                                $_product_mob   = wc_get_product( $pid );
                                $product_id_mob = $pid;
                                $product_permalink_mob = get_permalink($product_id);
                                $thumbnail_mob = apply_filters('woocommerce_cart_item_thumbnail', $_product_mob->get_image());
                                if ($_product_mob->get_parent_id() != 0) {
                                    $image_mob = wp_get_attachment_image_src(get_post_thumbnail_id($_product_mob->get_parent_id()), 'thumbnail_size'); //thumbnail_size for thumbnail
                                } else {
                                    $image_mob = wp_get_attachment_image_src(get_post_thumbnail_id($_product_mob->get_id()), 'thumbnail_size'); //thumbnail_size for thumbnail
                                }
                                $url_mob = $image_mob[0];?>
                                <div class="cart-item-row mob-cart">
                                    <?php if ($_product_mob->get_parent_id() != 0) { ?>
                                        <a href="javascript:void(0);" data-product-id="<?php echo $_product_mob->get_parent_id(); ?>"
                                           variationid="<?php echo $_product_mob->get_id(); ?>" class="icon-close removeitem"></a>
                                    <?php } else { ?>
                                        <a href="javascript:void(0);" data-product-id="<?php echo $_product_mob->get_id(); ?>"
                                           class="icon-close removeitem"></a>
                                    <?php } ?>
                                    <div class="item-list">
                                        <div class="item-img">
                                            <a href="<?php echo esc_url($product_permalink_mob); ?>"><img src="<?php echo $url_mob; ?>" class="img-responsive"/></a>
                                        </div>
                                        <div class="item-content">
                                            <h4 class="item-title"><a href="<?php echo esc_url($product_permalink_mob); ?>"><?php echo $_product_mob->get_name(); ?><br/><strong>0,75cl</strong></a></h4>
                                            <?php $brandname_mob = array_shift(wc_get_product_terms($_product_mob->get_id(), 'pa_brand', array('fields' => 'names'))); ?>
                                            <h4 class="item-cat"><a href=""><?php echo $brandname_mob; ?></a></h4>
                                        </div>
                                    </div>
                                    <?php
                                    $count_mob = 0;
                                    foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
                                        if ($_product_mob->get_parent_id() != 0) {
                                            //variation product
                                            if ($cart_item['product_id'] == $_product_mob->get_parent_id()) {
                                                $count_mob += $cart_item['quantity'];
                                            }
                                        } else {
                                            //simple product
                                            if ($cart_item['product_id'] == $_product_mob->get_id()) {
                                                $count_mob += $cart_item['quantity'];
                                            }
                                        }
                                    }
                                    ?>

                                    <?php
                                    if ($_product_mob->get_parent_id() != 0) {
                                        //variation product
                                        $mycartitem_mob = $cart_item['data']->get_data();
                                        $size_mob = $mycartitem_mob['attributes']['pa_size'];
                                    } else {
                                        //simple product
                                        $size_mob = array_shift(wc_get_product_terms($_product_mob->get_id(), 'pa_size', array('fields' => 'names')));
                                    }
                                    ?>

                                    <div class="product-status">
                                        <div class="krat-box">
                                            <h4 class="cart-it-dt"><?php if (empty($size_mob)) {echo "One size";} else {echo $size_mob;}; ?></h4>
                                            <h4 class="cart-it-price">p/doos &euro;<?php echo number_format((float)$_product_mob->get_price(), 2, '.', ''); ?></h4>
                                        </div>

                                        <div class="cart-price">
                                            <h2 class="price" id="subtotal_<?php echo $_product_mob->get_id(); ?>"> &euro; <?php echo number_format((float)$_product_mob->get_price() * $count_mob, 2, '.', ''); ?></h2>
                                        </div>

                                    </div>
                                    <div class="count-input left">
                                        <?php
                                        if($_product_mob->get_parent_id() != 0){ ?>
                                            <input type="hidden" class="add_to_cart_button" data-product_id="<?php echo $_product_mob->get_parent_id(); ?>" variationid="<?php echo $myproduct->get_id(); ?>" />
                                        <?php } else { ?>
                                            <input type="hidden" class="add_to_cart_button" data-product_id="<?php echo $_product_mob->get_id(); ?>" />
                                        <?php } ?>


                                        <a class="incr-btn" data-action="decrease" href="#"></a>
                                        <input class="quantity" type="text" name="quantity" value="<?php echo $count_mob; ?>">
                                        <a class="incr-btn" data-action="increase" href="#"></a>
                                        <img id="imgloader_<?php echo $_product_mob->get_id(); ?>" src="<?php echo get_stylesheet_directory_uri() . '/ajax-loader.gif'; ?>" style="display: none"/>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    <? }
                    else{?>
                        <div class="cart-item-row">
                            <?php if ($_product->get_parent_id() != 0) { ?>
                                <a href="javascript:void(0);" data-product-id="<?php echo $_product->get_parent_id(); ?>"
                                   variationid="<?php echo $_product->get_id(); ?>" class="icon-close removeitem"></a>
                            <?php } else { ?>
                                <a href="javascript:void(0);" data-product-id="<?php echo $_product->get_id(); ?>"
                                   class="icon-close removeitem"></a>
                            <?php } ?>
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="item-list">
                                        <div class="item-img">
                                            <?php
                                            $thumbnail = apply_filters('woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key);
                                            if ($_product->get_parent_id() != 0) {
                                                $image = wp_get_attachment_image_src(get_post_thumbnail_id($_product->get_parent_id()), 'thumbnail_size'); //thumbnail_size for thumbnail
                                            } else {
                                                $image = wp_get_attachment_image_src(get_post_thumbnail_id($_product->get_id()), 'thumbnail_size'); //thumbnail_size for thumbnail
                                            }
                                            $url = $image[0];
                                            ?>
                                            <td class="product-thumbnail">
                                                <a href="<?php echo esc_url($product_permalink); ?>"><img height="200px" width="200px" src="<?php echo $url; ?>" class="img-responsive"/></a>
                                            </td>
                                        </div>
                                        <div class="item-content">
                                            <h4 class="item-title"><a href="<?php echo esc_url($product_permalink); ?>"><?php echo $_product->get_name(); ?><br/><strong>0,75cl</strong></a></h4>
                                            <?php $brandname = array_shift(wc_get_product_terms($_product->get_id(), 'pa_brand', array('fields' => 'names'))); ?>
                                            <h4 class="item-cat"><a href=""><?php echo $brandname; ?></a></h4>
                                        </div>
                                    </div>
                                </div>

                                <?php
                                $count = 0;
                                foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
                                    if ($_product->get_parent_id() != 0) {
                                        //variation product
                                        if ($cart_item['product_id'] == $_product->get_parent_id()) {
                                            $count += $cart_item['quantity'];
                                        }
                                    } else {
                                        //simple product
                                        if ($cart_item['product_id'] == $_product->get_id()) {
                                            $count += $cart_item['quantity'];
                                        }
                                    }
                                }
                                ?>

                                <?php
                                if ($_product->get_parent_id() != 0) {
                                    //variation product
                                    $mycartitem = $cart_item['data']->get_data();
                                    $size = $mycartitem['attributes']['pa_size'];
                                } else {
                                    //simple product
                                    $size = array_shift(wc_get_product_terms($_product->get_id(), 'pa_size', array('fields' => 'names')));
                                }
                                ?>
                                <div class="col-sm-7">
                                    <div class="product-status d-flex justify-content-between">
                                        <div class="krat-box">
                                            <h4 class="mb-4 krat-it-dt">
                                                <?php if (empty($size)) {echo "One size";} else {echo $size;}; ?>
                                            </h4>
                                            <div class="count-input left">
                                                <?php
                                                if ($_product->get_parent_id() != 0) { ?>
                                                    <input type="hidden" class="add_to_cart_button" data-product_id="<?php echo $_product->get_parent_id(); ?>" variationid="<?php echo $_product->get_id(); ?>"/>
                                                <?php } else { ?>
                                                    <input type="hidden" class="add_to_cart_button" data-product_id="<?php echo $_product->get_id(); ?>"/>
                                                <?php } ?>

                                                <a class="incr-btn" data-action="decrease" href="#"></a>
                                                <input class="quantity" type="text" name="quantity" value="<?php echo $count; ?>">
                                                <a class="incr-btn" data-action="increase" href="#"></a>
                                                <img id="imgloader_<?php echo $_product->get_id(); ?>" src="<?php echo get_stylesheet_directory_uri() . '/ajax-loader.gif'; ?>" style="display: none"/>
                                            </div>
                                        </div>

                                        <div class="krat-box text-right">
                                            <h4 class="mb-4"><strong>p/doos &euro;<?php echo number_format((float)$_product->get_price(), 2, '.', '');?> </strong></h4>
                                            <h2 class="price" id="subtotal_<?php echo $_product->get_id(); ?>"> &euro; <?php echo number_format((float)$_product->get_price() * $count, 2, '.', ''); ?></h2>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            <?php }?>
        </div>
        <?php
        if(!empty($productids) && count($productids) > 1){ ?>
            <div class="web-cart">
                <div class="cart-pro-list">

                    <h1 style="text-align: left;"><?php
                        $terms = get_the_terms ( $_product->get_id(), 'product_cat' );
                        $i = 1;
                        foreach ( $terms as $term ) {
                            if($i > 1){echo ","; }
                            echo $term->name;
                            $i++;
                        }
                        ?>
                    </h1>
                    <?php foreach($productids as $key=>$productid){
                        array_push($mycoveredproducts,$productid);
                        $myproduct = wc_get_product($productid);
                        ?>
                        <div class="cart-item-row">
                            <?php if($myproduct->get_parent_id() != 0){ ?>
                                <a href="javascript:void(0);" data-product-id="<?php echo $myproduct->get_parent_id(); ?>" variationid="<?php echo $myproduct->get_id(); ?>" class="icon-close removeitem"></a>
                            <?php } else{ ?>
                                <a href="javascript:void(0);" data-product-id="<?php echo $myproduct->get_id(); ?>" class="icon-close removeitem"></a>
                            <?php } ?>
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="item-list">
                                        <div class="item-img">
                                            <?php
                                            $thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $myproduct->get_image(), $cart_item, $cart_item_key );
                                            if($myproduct->get_parent_id() != 0){
                                                $image = wp_get_attachment_image_src( get_post_thumbnail_id($myproduct->get_parent_id()), 'thumbnail_size' ); //thumbnail_size for thumbnail
                                            }
                                            else{
                                                $image = wp_get_attachment_image_src( get_post_thumbnail_id($myproduct->get_id()), 'thumbnail_size' ); //thumbnail_size for thumbnail
                                            }
                                            $url = $image[0];
                                            ?>
                                            <td class="product-thumbnail">
                                                <a href="<?php echo  esc_url( $product_permalink ); ?>"><img height="200px" width="200px" src="<?php echo $url; ?>" class="img-responsive"/></a>
                                            </td>
                                        </div>
                                        <div class="item-content">
                                            <h4 class="item-title"><a href="<?php echo  esc_url( $product_permalink ); ?>"><?php echo $myproduct->get_name(); ?><br /><strong>0,75cl</strong></a></h4>
                                            <?php $brandname = array_shift( wc_get_product_terms( $myproduct->get_id(), 'pa_brand', array( 'fields' => 'names' ) ) ); ?>
                                            <h4 class="item-cat"><a href=""><?php echo $brandname; ?></a></h4>
                                        </div>
                                    </div>
                                </div>

                                <?php
                                $count = 0;
                                foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
                                    if($myproduct->get_parent_id() != 0){
                                        //variation product
                                        if($cart_item['product_id'] == $myproduct->get_parent_id()){
                                            $count += $cart_item['quantity'];
                                        }
                                    }
                                    else{
                                        //simple product
                                        if($cart_item['product_id'] == $myproduct->get_id()){
                                            $count += $cart_item['quantity'];
                                        }
                                    }
                                }
                                ?>

                                <?php
                                if($myproduct->get_parent_id() != 0){
                                    //variation product
                                    $mycartitem = $cart_item['data']->get_data();
                                    $size = $mycartitem['attributes']['pa_size'];
                                }
                                else{
                                    //simple product
                                    $size =  array_shift( wc_get_product_terms( $myproduct->get_id(), 'pa_size', array( 'fields' => 'names' ) ) );
                                }
                                ?>
                                <div class="col-sm-7">
                                    <div class="product-status d-flex justify-content-between">
                                        <div class="krat-box">
                                            <h4 class="mb-4 krat-it-dt"><?php if(empty($size)){ echo "One size";} else{ echo $size; }; ?></h4>
                                            <div class="count-input left">
                                                <?php
                                                if($myproduct->get_parent_id() != 0){ ?>
                                                    <input type="hidden" class="add_to_cart_button" data-product_id="<?php echo $myproduct->get_parent_id(); ?>" variationid="<?php echo $myproduct->get_id(); ?>" />
                                                <?php } else { ?>
                                                    <input type="hidden" class="add_to_cart_button" data-product_id="<?php echo $myproduct->get_id(); ?>" />
                                                <?php } ?>

                                                <a class="incr-btn" data-action="decrease" href="#"></a>
                                                <input class="quantity" type="text" name="quantity" value="<?php echo $count; ?>">
                                                <a class="incr-btn" data-action="increase" href="#"></a>
                                                <img id="imgloader_<?php echo $myproduct->get_id(); ?>" src="<?php echo get_stylesheet_directory_uri().'/ajax-loader.gif'; ?>" style="display: none" />
                                            </div>
                                        </div>

                                        <div class="krat-box text-right">
                                            <h4 class="mb-4"><strong>p/doos &euro;<?php echo number_format((float)$myproduct->get_price(), 2, '.', ''); ?> </strong></h4>
                                            <h2 class="price" id="subtotal_<?php echo $myproduct->get_id(); ?>">&euro; <?php echo number_format((float)$myproduct->get_price()*$count, 2, '.', ''); ?></h2>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        <?php }
        else{
            if(!in_array($_product->get_id(),$mycoveredproducts)){ ?>
                <div class="web-cart">
                    <div class="cart-pro-list">

                        <h1 style="text-align: left;"><?php
                            $terms = get_the_terms ( $_product->get_id(), 'product_cat' );
                            $i = 1;
                            foreach ( $terms as $term ) {
                                if($i > 1){echo ","; }
                                echo $term->name;
                                $i++;
                            }
                            ?>
                        </h1>

                        <div class="cart-item-row">
                            <?php if($_product->get_parent_id() != 0){ ?>
                                <a href="javascript:void(0);" data-product-id="<?php echo $_product->get_parent_id(); ?>" variationid="<?php echo $_product->get_id(); ?>" class="icon-close removeitem"></a>
                            <?php } else{ ?>
                                <a href="javascript:void(0);" data-product-id="<?php echo $_product->get_id(); ?>" class="icon-close removeitem"></a>
                            <?php } ?>
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="item-list">
                                        <div class="item-img">
                                            <?php
                                            $thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );
                                            if($_product->get_parent_id() != 0){
                                                $image = wp_get_attachment_image_src( get_post_thumbnail_id($_product->get_parent_id()), 'thumbnail_size' ); //thumbnail_size for thumbnail
                                            }
                                            else{
                                                $image = wp_get_attachment_image_src( get_post_thumbnail_id($_product->get_id()), 'thumbnail_size' ); //thumbnail_size for thumbnail
                                            }
                                            $url = $image[0];
                                            ?>
                                            <td class="product-thumbnail">
                                                <a href="<?php echo  esc_url( $product_permalink ); ?>"><img height="200px" width="200px" src="<?php echo $url; ?>" class="img-responsive"/></a>
                                            </td>
                                        </div>
                                        <div class="item-content">
                                            <h4 class="item-title"><a href="<?php echo  esc_url( $product_permalink ); ?>"><?php echo $_product->get_name(); ?><br /><strong>0,75cl</strong></a></h4>
                                            <?php $brandname = array_shift( wc_get_product_terms( $_product->get_id(), 'pa_brand', array( 'fields' => 'names' ) ) ); ?>
                                            <h4 class="item-cat"><a href=""><?php echo $brandname; ?></a></h4>
                                        </div>
                                    </div>
                                </div>

                                <?php
                                $count = 0;
                                foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
                                    if($_product->get_parent_id() != 0){
                                        //variation product
                                        if($cart_item['product_id'] == $_product->get_parent_id()){
                                            $count += $cart_item['quantity'];
                                        }
                                    }
                                    else{
                                        //simple product
                                        if($cart_item['product_id'] == $_product->get_id()){
                                            $count += $cart_item['quantity'];
                                        }
                                    }
                                }
                                ?>

                                <?php
                                if($_product->get_parent_id() != 0){
                                    //variation product
                                    $mycartitem = $cart_item['data']->get_data();
                                    $size = $mycartitem['attributes']['pa_size'];
                                }
                                else{
                                    //simple product
                                    $size =  array_shift( wc_get_product_terms( $_product->get_id(), 'pa_size', array( 'fields' => 'names' ) ) );
                                }
                                ?>
                                <div class="col-sm-7">
                                    <div class="product-status d-flex justify-content-between">
                                        <div class="krat-box">
                                            <h4 class="mb-4 krat-it-dt"><?php if(empty($size)){ echo "One size";} else{ echo $size; }; ?></h4>
                                            <div class="count-input left">
                                                <?php
                                                if($_product->get_parent_id() != 0){ ?>
                                                    <input type="hidden" class="add_to_cart_button" data-product_id="<?php echo $_product->get_parent_id(); ?>" variationid="<?php echo $_product->get_id(); ?>" />
                                                <?php } else { ?>
                                                    <input type="hidden" class="add_to_cart_button" data-product_id="<?php echo $_product->get_id(); ?>" />
                                                <?php } ?>

                                                <a class="incr-btn" data-action="decrease" href="#"></a>
                                                <input class="quantity" type="text" name="quantity" value="<?php echo $count; ?>">
                                                <a class="incr-btn" data-action="increase" href="#"></a>
                                                <img id="imgloader_<?php echo $_product->get_id(); ?>" src="<?php echo get_stylesheet_directory_uri().'/ajax-loader.gif'; ?>" style="display: none" />
                                            </div>
                                        </div>

                                        <div class="krat-box text-right">
                                            <h4 class="mb-4"><strong>p/doos &euro;<?php echo number_format((float)$_product->get_price(), 2, '.', ''); ?> </strong></h4>
                                            <h2 class="price" id="subtotal_<?php echo $_product->get_id(); ?>">&euro; <?php echo number_format((float)$_product->get_price()*$count, 2, '.', ''); ?></h2>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php }
            ?>
        <?php }
    }
}


global $woocommerce;
$subtotal = floatval( preg_replace( '#[^\d.]#', '', $woocommerce->cart->get_cart_total() ) );
$shippingamount = floatval( preg_replace( '#[^\d.]#', '', $woocommerce->cart->get_shipping_total( ) ) );
$shippingtax = floatval( preg_replace( '#[^\d.]#', '', $woocommerce->cart->get_shipping_tax( ) ) );
$carttax = floatval( preg_replace( '#[^\d.]#', '', $woocommerce->cart->get_cart_contents_tax( ) ) );
$total = 	floatval( preg_replace( '#[^\d.]#', '', $woocommerce->cart->get_total( ) ) );

?>

    </div>

</section>
<?php if($total > 0){ ?>
    <section class="bg-white total-row" style="bottom:0;z-index:9;width:100%;position:sticky;position: -webkit-sticky">
        <div class="container">
            <div class="row flex-row-reverse">
                <div class="col-sm-6 text-sm-right">
                    <h2>Totaalprijs: €<span id="carttotal"><?php echo number_format((float)$total, 2, '.', ''); ?></h2>
                </div>
                <div class="col-sm-6 ">
                    <button id="createorder" class="btn btn-primary btn-lg">Bestellen</button>
                </div>
            </div>
        </div>
    </section>
<?php } ?>
<!--<div class="cart-collaterals">
    <?php
/*    do_action( 'woocommerce_cart_collaterals' );
    */?>
</div>-->


