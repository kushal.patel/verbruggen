<?php
/**
 * Lost password form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-lost-password.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.2
 */

defined( 'ABSPATH' ) || exit;

do_action( 'woocommerce_before_lost_password_form' );
?>

    <form method="post" class="" id="resetPwd">

        <div class="heading-block-red pad-3"><h1>Wachtwoord vergeten?</h1></div>
        <div class="form-section">
            <h2 class="f-30 mb-4 red-bdr">Vul het hieronder uw gegevens in</h2>
            <span id="error_forgot" class="error" style="color: red;display: none">Gebruiker niet beschikbaar</span>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
                        <label for="user_login" class="h3 bold"><?php esc_html_e( 'Gebruikersnaam', 'woocommerce' ); ?></label>
                        <input placeholder="Vul uw gebruikersnaam in" class="form-control" style="color: black;border-color: black;" type="text" name="user_login" id="user_login" autocomplete="username" />
                    </div>
                </div>
            </div>

            <div class="clear"></div>

            <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
                        <label for="user_email" class="h3 bold"><?php esc_html_e( 'Email adres', 'woocommerce' ); ?></label>
                        <input class="form-control" placeholder="Vul uw email in" style="color: black;border-color: black;" type="text" name="user_email" id="user_email" autocomplete="useremail" />
                    </div>
                </div>
            </div>

            <div class="clear"></div>

            <?php do_action( 'woocommerce_lostpassword_form' ); ?>

            <div class="form-group text-sm-right">
                <input type="hidden" name="wc_reset_password" value="true" />
                <button type="submit" id="resetpassword" class="btn btn-primary btn-lg mt-3" value="<?php esc_attr_e( 'Verzenden', 'woocommerce' ); ?>"><?php esc_html_e( 'Verzenden', 'woocommerce' ); ?></button>
            </div>

            <?php wp_nonce_field( 'lost_password', 'woocommerce-lost-password-nonce' ); ?>
        </div>
    </form>

<?php
do_action( 'woocommerce_after_lost_password_form' );
