<?php
require_once get_template_directory()."/vendor/autoload.php";
require_once get_template_directory()."/vendor/mobiledetect/mobiledetectlib/Mobile_Detect.php";
/**
 * My Account page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/my-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

defined( 'ABSPATH' ) || exit;

/**
 * My Account navigation.
 *
 * @since 2.6.0
 */
//do_action( 'woocommerce_account_navigation' );
$current_user = wp_get_current_user();
$customer_orders = wc_get_orders( array(
    'meta_key' => '_customer_user',
    'meta_value' => $current_user->ID,
) );

?>
<input type="hidden" id="myaccountpage" value="true" />
<section class="bg-white pad80 pt-0">
    <div class="container">
        <div class="dashboard-overview-tab">
            <div class="row">
                <div class="col-sm-3">
                    <aside class="dash-tabs-views">
                        <div class="act-info">
                            <h4><strong>Bedrijfsnaam b.v.</strong></h4>
                            <span class="text-d-grey"><?php printf( esc_html( get_user_meta($current_user->ID,'bank_acc_no',true)) );?></span>
                        </div>
                        <a href="#" class="nav-tabs-dropdown btn-m-toggle btn btn-primary">Tabs</a>
<!--                        <ul class="nav nav-tabs nav-pills nav-stacked well" id="accountTab" role="tablist">-->
                        <ul class="nav nav-tabs nav-pills nav-stacked well" id="nav-tabs-wrapper" role="tablist">
                            <li class="active">
                                <a class="active" id="dash-tab" data-toggle="tab" href="#dash" role="tab" aria-controls="home" aria-selected="true">Dashboard</a>
                            </li>
                            <li>
                                <a class="" id="bestellingen-tab" data-toggle="tab" href="#bestellingen" role="tab" aria-controls="bestellingen" aria-selected="false">Bestellingen</a>
                            </li>
                            <li>
                                <a class="" id="besteli-tab" data-toggle="tab" href="#bestellijst" role="tab" aria-controls="bestellijst" aria-selected="false">Bestellijst</a>
                            </li>
                            <li>
                                <a class="" id="account-tab" data-toggle="tab" href="#account" role="tab" aria-controls="account" aria-selected="false">Account details</a>
                            </li>
                        </ul>
                        <a href="<?php echo wp_logout_url(home_url('login')); ?>" class="btn-logout">Uitloggen</a>
                    </aside>
                </div>
                <div class="col-sm-9">
                    <section class="tab-content" id="accountContent">
                        <div class="tab-pane fade show active" id="dash" role="tabpanel" aria-labelledby="dash-tab">
                            <div class="tab-heading-big">
                                <div class="heading-block-red pad-3">
                                    <h1>Account (bedrijfsnaam)</h1>
                                </div>
                            </div>

                            <div class="act-cont-row">
                                <div class="heading-block-black pad-3">
                                    <h2>Account details</h2>
                                </div>

                                <div class="row hide-in-mob">
                                    <div class="col-md-7">
                                        <div class="act-info-bl shadow">
                                            <h5 class="bl-title">Account details</h5>
                                            <div class="act-cont-bl">
                                                <h5 class="text-d-grey">Bedrijfsnaam</h5>
                                                <h3 class="mb-0"><strong><?php printf( esc_html( get_user_meta($current_user->ID,'company_name',true)) );?></strong></h3>

                                                <h5 class="text-d-grey">Adres</h5>
                                                <h3 class="mb-0" id="billing_address"><strong><?php printf( esc_html( get_user_meta($current_user->ID,'billing_address_1',true)) ." " );?><?php printf( esc_html( get_user_meta($current_user->ID,'billing_address_2',true)) );?></strong></h3>

                                                <h5 class="text-d-grey">KvK gegevens</h5>
                                                <h3 class="mb-0"><strong><?php printf( esc_html( get_user_meta($current_user->ID,'company_number',true)) );?></strong></h3>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-5">
                                        <div class="act-info-bl shadow">
                                            <h5 class="bl-title">Contactpersoon</h5>
                                            <div class="act-cont-bl">
                                                <h5 class="text-d-grey">Naam</h5>
                                                <h3 class="mb-0" id="displayname"><strong><?php printf( esc_html( $current_user->display_name ) );?></strong></h3>

                                                <h5 class="text-d-grey">E-mail adress</h5>
                                                <h3 class="mb-0" id="displaymail"><strong><?php printf( esc_html( $current_user->user_email ) );?></strong></h3>

                                                <h5 class="text-d-grey">Telefoonnummer</h5>
                                                <h3 class="mb-0" id="displayphone"><strong><?php printf( esc_html( get_user_meta($current_user->ID,'telephone',true)) );?></strong></h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="dashbdr-itm owl-carousel product-gallery">
                                    <div class="act-info-bl shadow">
                                        <h5 class="bl-title">Account details</h5>
                                        <div class="act-cont-bl">
                                            <h5 class="text-d-grey">Bedrijfsnaam</h5>
                                            <h3 class="mb-0"><strong><?php printf( esc_html( get_user_meta($current_user->ID,'company_name',true)) );?></strong></h3>

                                            <h5 class="text-d-grey">Adres</h5>
                                            <h3 class="mb-0"><strong><?php printf( esc_html( get_user_meta($current_user->ID,'billing_address_1',true)) ." " );?><?php printf( esc_html( get_user_meta($current_user->ID,'billing_address_2',true)) );?></strong></h3>

                                            <h5 class="text-d-grey">KvK gegevens</h5>
                                            <h3 class="mb-0"><strong><?php printf( esc_html( get_user_meta($current_user->ID,'company_number',true)) );?></strong></h3>
                                        </div>
                                    </div>

                                    <div class="act-info-bl shadow">
                                        <h5 class="bl-title">Contactpersoon</h5>
                                        <div class="act-cont-bl">
                                            <h5 class="text-d-grey">Naam</h5>
                                            <h3 class="mb-0"><strong><?php printf( esc_html( $current_user->display_name ) );?></strong></h3>

                                            <h5 class="text-d-grey">E-mail adress</h5>
                                            <h3 class="mb-0"><strong><?php printf( esc_html( $current_user->user_email ) );?></strong></h3>

                                            <h5 class="text-d-grey">Telefoonnummer</h5>
                                            <h3 class="mb-0"><strong><?php printf( esc_html( get_user_meta($current_user->ID,'telephone',true)) );?></strong></h3>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="act-cont-row">
                                <div class="heading-block-black pad-3">
                                    <h2>Bestellingen</h2>
                                </div>

                                <div class="dashbdr-itm owl-carousel product-gallery">
                                    <?php if(!empty($customer_orders)){?>
                                        <?php foreach($customer_orders as $order ){
                                            $order = $order->get_data();
                                            ?>
                                            <div class="cart-item-row odr-box">
                                                <div class="ord-dt"><?php echo gmdate("d.m.Y",strtotime(get_the_date($order['date_created']))); ?></div>
                                                <div class="ordr-bl">
                                                    <h3 class="ord-title bold">Ordernummer</h3>
                                                    <h5 class="text-d-grey"><?php echo $order['id']; ?></h5>

                                                    <h3 class="ord-title bold">Factuur</h3>
                                                    <h5 class="text-d-grey"><?php echo $order['id']; ?></h5>

                                                    <div class="ord-total">
                                                        <h3 class="ord-title bold">Totaal incl. BTW</h3>
                                                        <h5 class="odr-price bold">€<?php echo $order['total']; ?></h5>
                                                        <a href="javascript:void()" class="btn-download"></a>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    <?php } else { ?>
                                        <h4>Geen bestelling gevonden</h4>
                                    <?php } ?>
                                </div>

                                <div class="row hide-in-mob">
                                    <div class="col-md-12">
                                        <div class="act-info-bl shadow">
                                            <?php if(!empty($customer_orders)){?>
                                                <h5 class="bl-title">Recente bestellingen</h5>
                                                <div id="accordion1" class="table-scrollable">
                                                    <table class="table tbl-4cols max-700 mb-0">
                                                        <thead>
                                                        <tr>
                                                            <th scope="col">Datum</th>
                                                            <th scope="col">Ordernummer</th>
                                                            <th scope="col">Factuur</th>
                                                            <th scope="col">Totaal incl. BTW</th>
                                                        </tr>
                                                        </thead>
                                                    </table>
                                                    <?php foreach($customer_orders as $order ){ ?>
                                                        <div class="accord-tbl-row" data-toggle="collapse" data-target="#act-tbl-<?php echo $order->get_id();?>" aria-expanded="false" aria-controls="act-tbl-<?php echo $order->get_id();?>">
                                                            <table class="table tbl-4cols bl-head max-700 mb-0">
                                                                <thead>
                                                                <tr>
                                                                    <td><?php printf( esc_html( date('d. m. Y',strtotime($order->order_date)) ) );?></td>
                                                                    <td><?php printf( esc_html( $order->get_id() ) );?></td>
                                                                    <td>123456789G</td>
                                                                    <td><h3 class="text-red m-0">€<?php printf( esc_html(  wc_format_decimal($order->get_total(), 2) ) );?></h3></td>
                                                                </tr>
                                                                </thead>
                                                            </table>
                                                            <div id="act-tbl-<?php echo $order->get_id();?>" class="collapse" aria-labelledby="headingtwo" data-parent="#accordion1">
                                                                <table class="table tbl-3cols max-700 mb-0">
                                                                    <thead>
                                                                    <tr>
                                                                        <th scope="col">Product</th>
                                                                        <th scope="col">Aantal</th>
                                                                        <th scope="col">Prijs</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    <?php foreach ($order->get_items() as $item_id => $item ){
                                                                        $item_data    = $item->get_data();
                                                                        $product      = $item->get_product();
                                                                        $size =  $product->get_attribute( 'size' );?>
                                                                        <tr>
                                                                            <td scope="row"><?php printf( esc_html( $item_data['name'] ) );?></td>
                                                                            <td><?php echo $size;?></td>
                                                                            <td><span class="price">€<?php printf( esc_html(  wc_format_decimal($item_data['total'], 2) ) );?></span></td>
                                                                        </tr>
                                                                    <?php }?>
                                                                    </tbody>
                                                                    <tfoot>
                                                                    <tr>
                                                                        <td colspan="3"><a href="javascript:void(0);" class="btn-next place-order">Herhaal deze bestelling</a> </td>
                                                                    </tr>
                                                                    </tfoot>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    <?php }?>
                                                </div>
                                            <?php }else{ ?>
                                                <h4>Geen bestelling gevonden</h4>
                                            <?php }?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="act-cont-row">
                                <div class="heading-block-black pad-3">
                                    <h2>Bestellijst</h2>
                                </div>

                                <div class="dashbdr-itm owl-carousel product-gallery">
                                    <?php if(!empty(WC()->cart->get_cart())){?>
                                        <?php foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
                                            $_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
                                            ?>
                                            <div class="cart-item-row odr-box">
                                                <div class="ordr-bl">
                                                    <h3 class="ord-title bold">Product</h3>
                                                    <h5 class="text-d-grey"><?php  echo $_product->get_name();?></h5>

                                                    <h3 class="ord-title bold">Aantal</h3>
                                                    <h5 class="text-d-grey"><?php echo $cart_item['quantity']; ?></h5>

                                                    <div class="ord-total">
                                                        <h3 class="ord-title bold">Prijs</h3>
                                                        <h5 class="odr-price bold">€<?php //echo $cart_item['line_subtotal'];
                                                            printf( esc_html(  wc_format_decimal($cart_item['line_subtotal'], 2) ) );  ?></h5>
                                                        <a href="javascript:void()" class="btn-download"></a>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    <?php } else { ?>
                                        <h4>Geen bestelling gevonden</h4>
                                    <?php } ?>
                                </div>

                                <?php
                                if(!empty(WC()->cart->get_cart())){?>
                                <div class="row hide-in-mob">
                                    <div class="col-md-12">
                                        <div class="act-info-bl shadow">
                                            <div class="table-scrollable">
                                                <table class="table max-700 mb-0">
                                                    <thead>
                                                    <tr>
                                                        <th scope="col">Product</th>
                                                        <th scope="col">Aantal</th>
                                                        <th scope="col">Prijs</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                    <?php foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
                                        $_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
                                        $count = 0;
                                        if($_product->get_parent_id() != 0){
                                            //variation product
                                            if($cart_item['product_id'] == $_product->get_parent_id()){
                                                $count += $cart_item['quantity'];
                                            }
                                        }
                                        else{
                                            //simple product
                                            if($cart_item['product_id'] == $_product->get_id()){
                                                $count += $cart_item['quantity'];
                                            }
                                        }
                                        $size = "";
                                        $mycartitem = $cart_item['data']->get_data();
                                        if(isset($mycartitem['attributes']['size'])){
                                            $pasize = $mycartitem['attributes']['size']->get_data();
                                            $size = $pasize['options'][0];
                                        }
                                        ?>
                                        <tr id="cart-<?php echo $cart_item['product_id'];?>">
                                            <td scope="row"><?php  echo $_product->get_name();?></td>
                                            <td><?php  echo $size;?></td>
                                            <td>
                                                <span class="price">€<?php
//                                                    echo $_product->get_price()*$count;
                                                    printf( esc_html(  wc_format_decimal($_product->get_price()*$count, 2) ) );
                                                    ?>
                                                </span>
                                            </td>
                                        </tr>

                                    <?php }?>
                                                    </tbody>
                                                </table>

                                            </div>
                                            <div class="table-action d-flex justify-content-between">
                                                <a href="javascript:void(0);" class="btn-next place-order">Bestellen</a>
                                                <a href="<?php echo get_site_url()."/cart"; ?>" class="btn-prev">Aanpassen</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php }else{?>
                                    <div class="act-info-bl shadow">
                                        <h4>Geen bestelling gevonden</h4>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <!-- end dashboard tab -->

                        <div class="tab-pane fade" id="bestellingen" role="tabpanel" aria-labelledby="bestellingen-tab">
                            <div class="tab-heading-big">
                                <div class="heading-block-red pad-3">
                                    <h1>Bestellingen</h1>
                                </div>
                            </div>

                            <div class="heading-block-black pad-3  hide-in-desk">
                                <h2>Bestellingen</h2>
                            </div>

                            <div class="dashbdr-itm owl-carousel product-gallery">
                                <?php if(!empty($customer_orders)){?>
                                    <?php foreach($customer_orders as $order ){
                                        $order = $order->get_data();
                                        ?>
                                        <div class="cart-item-row odr-box">
                                            <div class="ord-dt"><?php echo gmdate("d.m.Y",strtotime(get_the_date($order['date_created']))); ?></div>
                                            <div class="ordr-bl">
                                                <h3 class="ord-title bold">Ordernummer</h3>
                                                <h5 class="text-d-grey"><?php echo $order['id']; ?></h5>

                                                <h3 class="ord-title bold">Factuur</h3>
                                                <h5 class="text-d-grey"><?php echo $order['id']; ?></h5>

                                                <div class="ord-total">
                                                    <h3 class="ord-title bold">Totaal incl. BTW</h3>
                                                    <h5 class="odr-price bold">€<?php echo $order['total']; ?></h5>
                                                    <a href="javascript:void()" class="btn-download"></a>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                <?php } else { ?>
                                    <h4>Geen bestelling gevonden</h4>
                                <?php } ?>
                            </div>
                            <?php if(!empty($customer_orders)){?>
                                <div id="accordion2" class="table-scrollable hide-in-mob">
                                    <table class="table tbl-4cols max-700 mb-0">
                                        <thead>
                                        <tr>
                                            <th scope="col">Datum</th>
                                            <th scope="col">Ordernummer</th>
                                            <th scope="col">Factuur</th>
                                            <th scope="col">Totaal incl. BTW</th>
                                        </tr>
                                        </thead>
                                    </table>
                                    <?php foreach($customer_orders as $order ){ ?>
                                        <div class="accord-tbl-row" data-toggle="collapse" data-target="#act-tbl-<?php echo $order->get_id();?>" aria-expanded="false" aria-controls="act-tbl-<?php echo $order->get_id();?>">
                                            <table class="table tbl-4cols bl-head max-700 mb-0">
                                                <thead>
                                                <tr>
                                                    <td><?php printf( esc_html( date('d. m. Y',strtotime($order->order_date)) ) );?></td>
                                                    <td><?php printf( esc_html( $order->get_id() ) );?></td>
                                                    <td>123456789G</td>
                                                    <td><h3 class="text-red m-0">€<?php printf( esc_html(  wc_format_decimal($order->get_total(), 2) ) );?></h3></td>
                                                </tr>
                                                </thead>
                                            </table>
                                            <div id="act-tbl-<?php echo $order->get_id();?>" class="collapse" aria-labelledby="headingtwo" data-parent="#accordion2">
                                                <table class="table tbl-3cols max-700 mb-0">
                                                    <thead>
                                                    <tr>
                                                        <th scope="col">Product</th>
                                                        <th scope="col">Aantal</th>
                                                        <th scope="col">Prijs</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php foreach ($order->get_items() as $item_id => $item ){
                                                        $item_data    = $item->get_data();
                                                        $product      = $item->get_product();
                                                        $size =  $product->get_attribute( 'size' );?>
                                                        <tr>
                                                            <td scope="row"><?php printf( esc_html( $item_data['name'] ) );?></td>
                                                            <td><?php echo $size;?></td>
                                                            <td><span class="price">€<?php printf( esc_html(  wc_format_decimal($item_data['total'], 2) ) );?></span></td>
                                                        </tr>
                                                    <?php }?>
                                                    </tbody>
                                                    <tfoot>
                                                    <tr>
                                                        <td colspan="3"><a href="" class="btn-next">Herhaal deze bestelling</a> </td>
                                                    </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    <?php }?>
                                </div>
                            <?php }else{?>
                                <h4>Geen bestelling gevonden</h4>
                            <?php }?>


                        </div>
                        <!-- end bestellingen -->

                        <div class="tab-pane fade" id="bestellijst" role="tabpanel" aria-labelledby="besteli-tab">
                            <div class="tab-heading-big">
                                <div class="heading-block-red pad-3">
                                    <h1>Bestellijst</h1>
                                </div>
                            </div>
                            <div id="my-account-cart">
                                <?php echo do_shortcode( '[woocommerce_cart]' );?>
                            </div>


                        </div>
                        <!-- end bestellijst -->

                        <div class="tab-pane fade" id="account" role="tabpanel" aria-labelledby="account-tab">
                            <div class="tab-heading-big">
                                <div class="heading-block-red pad-3">
                                    <h1>Account details</h1>
                                </div>
                            </div>
                            <div id="success_update" style="display:none;">
                                <h2 class="heading-bdr-red">User updated successfully.</h2>
                            </div>

                            <?php
                            $detect = new Mobile_Detect;

                            // Any mobile device (phones or tablets).
                            if ( !$detect->isMobile() ) { ?>
                                <div class="act-cont-row">
                                    <h2 class="red-bdr">Contactpersoon</h2>
                                    <form class="input-black-text profile-edit" method="post">
                                        <div class="act-cont-bl">
                                            <input type="hidden" id="userid" value="<?php echo $current_user->ID;?>" name="userid">
                                            <h5 class="text-d-grey">naam</h5>
                                            <h3 class="mb-0 non-edit-profile" id="username"><strong><?php printf( esc_html( $current_user->display_name ) );?></strong></h3>
                                            <input type="text" placeholder="Voor- en achternaam" name="name" id="name" class="form-control mb-0 edit-profile" value="<?php printf( esc_html( $current_user->display_name ) );?>"/>

                                            <h5 class="text-d-grey">E-mail adress</h5>
                                            <h3 class="mb-0 non-edit-profile" id="useremail"><strong><?php printf( esc_html( $current_user->user_email ) );?></strong></h3>
                                            <input type="text" placeholder="Email" id="email" name="email" class="form-control mb-0 edit-profile" value="<?php printf( esc_html( $current_user->user_email ) );?>">
                                            <span id="email_exist_error" class="error" style="color: red;display: none;margin-left: 5px;">E-mail adres al in gebruik</span>

                                            <h5 class="text-d-grey">Telefoonnummer</h5>
                                            <h3 class="mb-0 non-edit-profile" id="userphone"><strong><?php printf( esc_html( get_user_meta($current_user->ID,'telephone',true)) );?></strong></h3>
                                            <input type="text" placeholder="Vast- en/of mobielnummer" id="telephone" name="telephone" class="form-control mb-0 edit-profile" value="<?php printf( esc_html( get_user_meta($current_user->ID,'telephone',true)) );?>">

                                        </div>
                                        <div class="d-flex justify-content-end">
                                            <a href="javascript:void(0);" id="edit-profile-detail" class="btn-back mb-3 non-edit-profile">Aanpassen</a>
                                            <a href="javascript:void(0);" id="save-profile-detail" class="btn-back mb-3 edit-profile">verzenden</a>
                                        </div>
                                    </form>
                                </div>

                                <div class="act-cont-row">
                                    <h2 class="red-bdr">Bedrijfsgegevens</h2>

                                    <div class="act-cont-bl">
                                        <h5 class="text-d-grey">Bedrijfsnaam</h5>
                                        <h3 class="mb-0" ><strong><?php printf( esc_html( get_user_meta($current_user->ID,'company_number',true)) );?></strong></h3>

                                        <h5 class="text-d-grey">Adres</h5>
                                        <h3 class="mb-0" id="billing_address_edit"><strong><?php printf( esc_html( get_user_meta($current_user->ID,'billing_address_1',true)) ." " );?><?php printf( esc_html( get_user_meta($current_user->ID,'billing_address_2',true)) ." " );?></strong></h3>

                                        <form class="input-black-text address-edit" method="post">
                                            <input type="hidden" id="user-id" value="<?php echo $current_user->ID;?>" name="user-id">
                                            <div class="row edit-address">
                                                <div class="col-md-6 col-sm-12">
                                                    <input type="text" placeholder="Straatnaam" id="street" name="street" class="form-control" value="<?php printf( esc_html( get_user_meta($current_user->ID,'street',true)));?>">
                                                </div>
                                                <div class="col-md-3 col-sm-6">
                                                    <input type="text" placeholder="Huisnr" name="house_no" id="house_no" class="form-control" value="<?php printf( esc_html( get_user_meta($current_user->ID,'house_no',true)));?>">
                                                </div>
                                                <div class="col-md-3 col-sm-6">
                                                    <input type="text" placeholder="Toevoeging" name="addition" id="addition" class="form-control" value="<?php printf( esc_html( get_user_meta($current_user->ID,'addition',true)) );?>">
                                                </div>
                                            </div>

                                            <div class="row edit-address">
                                                <div class="col-md-3 col-sm-4">
                                                    <input type="text" placeholder="Postcode / postbus"
                                                           class="form-control" name="postcode" id="postcode" value="<?php printf( esc_html( get_user_meta($current_user->ID,'postcode',true)) );?>">
                                                </div>
                                                <div class="col-md-5 col-sm-4">
                                                    <input type="text" placeholder="Plaatsnaam" name="city" id="city" class="form-control" value="<?php printf( esc_html( get_user_meta($current_user->ID,'city',true)));?>">
                                                </div>
                                                <div class="col-md-4 col-sm-4">
                                                    <div class="form-control-file">
                                                        <select class="select full mb-0" name="landmark" id="landmark" value="<?php printf( esc_html( get_user_meta($current_user->ID,'landmark',true)) ." " );?>">
                                                            <option value="1">Land</option>
                                                            <option value="2">Land 1</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                        </form>

                                        <h5 class="text-d-grey">KvK gegevens</h5>
                                        <h3 class="mb-0"><strong>12345678</strong></h3>
                                    </div>
                                    <div class="d-flex justify-content-end">
                                        <a href="javascript:void(0);" id="edit-address-detail" class="btn-back mb-3 non-edit-address">Aanpassen</a>
                                        <a href="javascript:void(0);" id="save-address-detail" class="btn-back mb-3 edit-address">verzenden</a>
                                    </div>
                                </div>

                                <div class="act-cont-row">
                                    <h2 class="red-bdr">Betaalgegevens</h2>

                                    <div class="act-cont-bl">
                                        <form class="input-black-text bank_details" method="post">
                                        <div class="row">

                                                <div class="col-sm-6">
                                                    <h5 class="text-d-grey">IBAN</h5>
                                                    <h3 class="mb-0 bank_acc_no"><strong><?php printf( esc_html( get_user_meta($current_user->ID,'bank_acc_no',true)) );?></strong></h3>
                                                    <input type="text" placeholder="IBAN" id="bank_acc_no" name="bank_acc_no" class="form-control mb-0 edit-banks" value="<?php printf( esc_html( get_user_meta($current_user->ID,'bank_acc_no',true)) );?>">

                                                    <h5 class="text-d-grey">BTW</h5>
                                                    <h3 class="mb-0 vat"><strong><?php printf( esc_html( get_user_meta($current_user->ID,'vat',true)) );?></strong></h3>
                                                    <input type="text" placeholder="" id="vat" name="vat" class="form-control mb-0 edit-banks" value="<?php printf( esc_html( get_user_meta($current_user->ID,'vat',true)) );?>">
                                                </div>

                                                <div class="col-sm-6">
                                                    <h5 class="text-d-grey">BIC</h5>
                                                    <h3 class="mb-0 bank_code"><strong><?php printf( esc_html( get_user_meta($current_user->ID,'bank_code',true)) );?></strong></h3>
                                                    <input type="text" placeholder="" id="bic" name="bank_code" class="form-control mb-0 edit-banks" value="<?php printf( esc_html( get_user_meta($current_user->ID,'bank_code',true)) );?>">

                                                    <h5 class="text-d-grey">EORI</h5>
                                                    <h3 class="mb-0 eori"><strong><?php printf( esc_html( get_user_meta($current_user->ID,'eori',true)) );?></strong></h3>
                                                    <input type="text" placeholder="" id="eori" name="eori" class="form-control mb-0 edit-banks" value="<?php printf( esc_html( get_user_meta($current_user->ID,'eori',true)) );?>">
                                                </div>

                                        </div>
                                        </form>
                                    </div>
                                    <div class="d-flex justify-content-end">
                                        <a href="javascript:void(0);" id="save-bank-detail" class="btn-back mb-3 edit-bank">verzenden</a>
                                        <a href="javascript:void(0);" id="edit-bank-detail" class="btn-back mb-3 non-edit-bank">Aanpassen</a>
                                    </div>
                                </div>
                            <?php }
                            else{ ?>
                                <div class="heading-block-black pad-3 hide-in-desk">
                                    <h2>Account details</h2>
                                </div>
                                <div class="dashbdr-itm owl-carousel product-gallery">
                                    <div class="act-info-bl">
                                        <h5 class="bl-title">Contactpersoon</h5>
                                        <form class="input-black-text profile-edit" method="post">
                                            <div class="act-cont-bl">
                                                <input type="hidden" id="userid" value="<?php echo $current_user->ID;?>" name="userid">
                                                <h5 class="text-d-grey">naam</h5>
                                                <h3 class="mb-0 non-edit-profile" id="username"><strong><?php printf( esc_html( $current_user->display_name ) );?></strong></h3>
                                                <input type="text" placeholder="Voor- en achternaam" name="name" id="name" class="form-control mb-0 edit-profile" value="<?php printf( esc_html( $current_user->display_name ) );?>"/>

                                                <h5 class="text-d-grey">E-mail adress</h5>
                                                <h3 class="mb-0 non-edit-profile" id="useremail"><strong><?php printf( esc_html( $current_user->user_email ) );?></strong></h3>
                                                <input type="text" placeholder="Email" id="email" name="email" class="form-control mb-0 edit-profile" value="<?php printf( esc_html( $current_user->user_email ) );?>">
                                                <span id="email_exist_error" class="error" style="color: red;display: none;margin-left: 5px;">E-mail adres al in gebruik</span>

                                                <h5 class="text-d-grey">Telefoonnummer</h5>
                                                <h3 class="mb-0 non-edit-profile" id="userphone"><strong><?php printf( esc_html( get_user_meta($current_user->ID,'telephone',true)) );?></strong></h3>
                                                <input type="text" placeholder="Vast- en/of mobielnummer" id="telephone" name="telephone" class="form-control mb-0 edit-profile" value="<?php printf( esc_html( get_user_meta($current_user->ID,'telephone',true)) );?>">

                                            </div>
                                            <div class="d-flex justify-content-end">
                                                <a href="javascript:void(0);" id="edit-profile-detail" class="btn-back mb-3 non-edit-profile">Aanpassen</a>
                                                <a href="javascript:void(0);" id="save-profile-detail" class="btn-back mb-3 edit-profile">verzenden</a>
                                            </div>
                                        </form>
                                    </div>

                                    <div class="act-info-bl">
                                        <h5 class="bl-title">Bedrijfsgegevens</h5>

                                        <div class="act-cont-bl">
                                            <h5 class="text-d-grey">Bedrijfsnaam</h5>
                                            <h3 class="mb-0" ><strong><?php printf( esc_html( get_user_meta($current_user->ID,'company_number',true)) );?></strong></h3>

                                            <h5 class="text-d-grey">Adres</h5>
                                            <h3 class="mb-0" id="billing_address_edit"><strong><?php printf( esc_html( get_user_meta($current_user->ID,'billing_address_1',true)) ." " );?><?php printf( esc_html( get_user_meta($current_user->ID,'billing_address_2',true)) ." " );?></strong></h3>

                                            <form class="input-black-text address-edit" method="post">
                                                <input type="hidden" id="user-id" value="<?php echo $current_user->ID;?>" name="user-id">
                                                <div class="row edit-address">
                                                    <div class="col-md-6 col-sm-12">
                                                        <input type="text" placeholder="Straatnaam" id="street" name="street" class="form-control mb-0" value="<?php printf( esc_html( get_user_meta($current_user->ID,'street',true)));?>">
                                                    </div>
                                                    <div class="col-md-3 col-sm-6">
                                                        <input type="text" placeholder="Huisnr" name="house_no" id="house_no" class="form-control mb-0" value="<?php printf( esc_html( get_user_meta($current_user->ID,'house_no',true)));?>">
                                                    </div>
                                                    <div class="col-md-3 col-sm-6">
                                                        <input type="text" placeholder="Toevoeging" name="addition" id="addition" class="form-control mb-0" value="<?php printf( esc_html( get_user_meta($current_user->ID,'addition',true)) );?>">
                                                    </div>
                                                </div>

                                                <div class="row edit-address">
                                                    <div class="col-md-3 col-sm-4">
                                                        <input type="text" placeholder="Postcode / postbus"
                                                               class="form-control mb-0" name="postcode" id="postcode" value="<?php printf( esc_html( get_user_meta($current_user->ID,'postcode',true)) );?>">
                                                    </div>
                                                    <div class="col-md-5 col-sm-4">
                                                        <input type="text" placeholder="Plaatsnaam" name="city" id="city" class="form-control mb-0" value="<?php printf( esc_html( get_user_meta($current_user->ID,'city',true)));?>">
                                                    </div>
                                                    <div class="col-md-4 col-sm-4">
                                                        <div class="form-control-file">
                                                            <select class="select full mb-0" name="landmark" id="landmark" value="<?php printf( esc_html( get_user_meta($current_user->ID,'landmark',true)) ." " );?>">
                                                                <option value="1">Land</option>
                                                                <option value="2">Land 1</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                            </form>

                                            <h5 class="text-d-grey">KvK gegevens</h5>
                                            <h3 class="mb-0"><strong>12345678</strong></h3>
                                        </div>
                                        <div class="d-flex justify-content-end">
                                            <a href="javascript:void(0);" id="edit-address-detail" class="btn-back mb-3 non-edit-address">Aanpassen</a>
                                            <a href="javascript:void(0);" id="save-address-detail" class="btn-back mb-3 edit-address">verzenden</a>
                                        </div>
                                    </div>

                                    <div class="act-info-bl">
                                        <h5 class="bl-title">Betaalgegevens</h5>

                                        <div class="act-cont-bl">
                                            <div class="row">
                                                <form class="input-black-text bank_details" method="post">
                                                    <div class="col-sm-6">
                                                        <h5 class="text-d-grey">IBAN</h5>
                                                        <h3 class="mb-0 bank_acc_no"><strong><?php printf( esc_html( get_user_meta($current_user->ID,'bank_acc_no',true)) );?></strong></h3>
                                                        <input type="text" placeholder="IBAN" id="bank_acc_no" name="bank_acc_no" class="form-control mb-0 edit-banks" value="<?php printf( esc_html( get_user_meta($current_user->ID,'bank_acc_no',true)) );?>">

                                                        <h5 class="text-d-grey">BTW</h5>
                                                        <h3 class="mb-0 vat"><strong><?php printf( esc_html( get_user_meta($current_user->ID,'vat',true)) );?></strong></h3>
                                                        <input type="text" placeholder="" id="vat" name="vat" class="form-control mb-0 edit-banks" value="<?php printf( esc_html( get_user_meta($current_user->ID,'vat',true)) );?>">
                                                    </div>

                                                    <div class="col-sm-6">
                                                        <h5 class="text-d-grey">BIC</h5>
                                                        <h3 class="mb-0 bank_code"><strong><?php printf( esc_html( get_user_meta($current_user->ID,'bank_code',true)) );?></strong></h3>
                                                        <input type="text" placeholder="" id="bic" name="bank_code" class="form-control mb-0 edit-banks" value="<?php printf( esc_html( get_user_meta($current_user->ID,'bank_code',true)) );?>">

                                                        <h5 class="text-d-grey">EORI</h5>
                                                        <h3 class="mb-0 eori"><strong><?php printf( esc_html( get_user_meta($current_user->ID,'eori',true)) );?></strong></h3>
                                                        <input type="text" placeholder="" id="eori" name="eori" class="form-control mb-0 edit-banks" value="<?php printf( esc_html( get_user_meta($current_user->ID,'eori',true)) );?>">
                                                    </div>
                                                </form>
                                            </div>
                                        </div>

                                        <div class="d-flex justify-content-end">
                                            <a href="javascript:void(0);" id="save-bank-detail" class="btn-back mb-3 edit-bank">verzenden</a>
                                            <a href="javascript:void(0);" id="edit-bank-detail" class="btn-back mb-3 non-edit-bank">Aanpassen</a>
                                        </div>
                                    </div>
                                </div>
                            <?php }
                            ?>
                        </div>
                        <!-- end account details -->
                    </section>
                </div>
            </div>
        </div>
    </div>

</section>