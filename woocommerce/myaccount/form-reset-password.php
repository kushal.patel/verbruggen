<?php
/**
 * Lost password reset form.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-reset-password.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

defined( 'ABSPATH' ) || exit;

?>

<form method="post" class="woocommerce-ResetPassword lost_reset_password" id="lost_pwd">

	<div class="heading-block-red pad-3"><h1>Nieuw Wachtwoord</h1></div>
	<div class="form-section">
		<h2 class="f-30 mb-4 red-bdr">Geef uw nieuwe Wachtwoord op</h2>
		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<label for="user_login" class="h3 bold"><?php esc_html_e( 'Wachtwoord', 'woocommerce' ); ?></label>
					<input type="password" class="form-control" style="color: black;border-color: black;" placeholder="Vul een nieuw wachtwoord in" name="password_1" id="password_1" autocomplete="new-password" />
				</div>
			</div>
		</div>

		<div class="clear"></div>

		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<label for="user_email" class="h3 bold"><?php esc_html_e( 'Controle wachtwoord', 'woocommerce' ); ?></label>
					<input type="password" class="form-control" style="color: black;border-color: black;" placeholder="Vul nogmaals uw nieuw wachtwoord in" name="password_2" id="password_2" autocomplete="new-password" />
				</div>
			</div>
		</div>
		<span id="error_resetpass" class="error" style="color: red;"></span>
		<input type="hidden" name="reset_key" value="<?php echo esc_attr( $args['key'] ); ?>" />
		<input type="hidden" name="reset_login" value="<?php echo esc_attr( $args['login'] ); ?>" />

		<div class="clear"></div>

		<?php do_action( 'woocommerce_resetpassword_form' ); ?>

		<div class="form-group text-sm-right">
			<input type="hidden" name="wc_reset_password" value="true" />
			<button type="submit" id="resetpassword" class="btn btn-primary btn-lg mt-3" value="<?php esc_attr_e( 'Verzenden', 'woocommerce' ); ?>"><?php esc_html_e( 'Verzenden', 'woocommerce' ); ?></button>
		</div>

		<?php wp_nonce_field( 'reset_password', 'woocommerce-reset-password-nonce' ); ?>
	</div>
</form>


