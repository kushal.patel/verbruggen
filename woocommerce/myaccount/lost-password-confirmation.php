<?php
/**
 * Lost password confirmation text.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/lost-password-confirmation.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.2
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

//wc_print_notice( __( 'Password reset email has been sent.', 'woocommerce' ) );
?>


<!--<section class="bg-white pad80 pt-0">-->
<!--	<div class="container">-->
<div class="heading-block-red pad-3"><h1>Wachtwoord vergeten?</h1></div>
<div class="form-section">
    <h5 class="f-30 mb-4 red-bdr">Verzoek tot nieuw wachtwoord aanmaken ontvangen.</h5>
    <div class="row">
        <div class="col-md-12">
            <h1>Link voor een nieuw wachtwoord<br>is verzonden naar uw opgegeven mailadres.   </h1>
        </div>
    </div>
</div>
<div class="form-section">
    <div class="row">
        <div class="col-md-12">
            <div class="col-sm-8">
                <h3 class="lts-77"><strong>LET OP:</strong><br>
                    Het kan voorkomen dat mail van Verbruggen in uw Spam terecht komt!</h3>
            </div>
            <div class="col-sm-4 d-flex align-items-center justify-content-end">
                <a class="btn-back" href="<?php echo wp_lostpassword_url(); ?>">Terug</a>
            </div>
        </div>
    </div>
</div>
<!--	</div>-->
<!--</section>-->