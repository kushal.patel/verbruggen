<?php
/**
 * Customer new account email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-new-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 3.5.2
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>

<?php //do_action( 'woocommerce_email_header', $email_heading, $email ); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="fonts/style.css">
	<title>Wachtwoord aanpassen</title>
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.png">
	<link href="" rel="stylesheet">
	<style>
		@font-face {
			font-family: 'Circular Std Book';
			font-style: normal;
			font-weight: normal;
			src: local('Circular Std Book'), url('CircularStd-Book.woff') format('woff');
		}




		@font-face {
			font-family: 'Circular Std Bold';
			font-style: normal;
			font-weight: normal;
			src: local('Circular Std Bold'), url('CircularStd-Bold.woff') format('woff');
		}


	</style>
</head>

<body>
<table style="margin:0 auto;width: 750px; font-family: Circular Std Book, sans-serif; font-size:18px; border-collapse:collapse; background:#FFFFFF; color:#31383e;" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td colspan="3" height="25">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3">
			<table  width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="3"><a href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri().'/emails/images/logo.png'; ?>"  border="0"/></a></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="3" height="45">&nbsp;</td>
	</tr>

	<tr>
		<td bgcolor="#ed3024" style="background-image:url(<?php echo get_template_directory_uri().'/emails/images/bg-food-vector.png'; ?>); background-repeat:no-repeat; padding:15px 9%; background-position:left center;background-size: cover;">
			<h1 style="color:#fff; font-size:50px; margin:0; line-height:1.1;font-family: Circular Std Bold, sans-serif;">Registratie afgekeurd</h1>
		</td>
	</tr>
	<tr>
		<td colspan="3" height="45">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3" style=" padding:15px 9%;">
			<h2 style="font-size:40px; margin:0 0 30px;"><?php printf( esc_html__( ' %s,', 'woocommerce' ), esc_html( $user_login ) ); ?></h2>
			<p style="margin:0 0 30px; line-height:1.6;">Helaas hebben wij geen goed bericht voor u.</p>


			<p style="margin:0 0 30px; line-height:1.6;">Op basis van onderstaande punten zijn wij tot de conslusie gekomen dat wij u hellas niet als klant welkom mogen heten:</p>

			<p style="margin:0 0 30px; line-height:1.6;">
				<a href="<?php echo home_url(); ?>" style="color:#ed3024;font-family: Circular Std Bold, sans-serif; text-decoration:none;">- Punt &eacute;&eacute;n</a><br />
				<a href="<?php echo home_url(); ?>" style="color:#ed3024;font-family:Circular Std Bold, sans-serif; text-decoration:none;">- Punt tw&eacute;&eacute;</a>
			</p>

			<p style="margin:0 0 30px; line-height:1.6;">Aliquam pellentesque risus at commodo hendrerit. Quisque metus neque, suscipit ut molestie eget, facilisis et
				enim. Phasellus interdum tristique nisi in bibendum. Quisque maximus ut turpis at elementum.</p>

			<p style="font-family: Circular Std Book, sans-serif; font-size:18px; margin:0 0 30px; line-height:1.6;">
				Bedankt voor uw begrip,<br />
				Het Verbruggen team.
			</p>
			<p style="font-family: Circular Std Book, sans-serif; font-size:18px; margin:0 0 30px; line-height:1.6;">Heeft u vragen? Dan kunt ons <a href="<?php echo home_url(); ?>" style="color:#ed3024; text-decoration:underline;">mailen</a> of bellen op werkdagen van 8:30 tot 19:00 op: <font style="color:#ed3024; text-decoration:underline;">0485 451 220</font></p>
		</td>
	</tr>
</table>
</body>
</html>

<?php /* translators: %s: Customer first name */ ?>

<p><?php //esc_html_e( 'Thanks for reading.', 'woocommerce' ); ?></p>

<?php //do_action( 'woocommerce_email_footer', $email ); ?>
