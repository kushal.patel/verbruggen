<?php
/**
 * Email Header
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-header.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates/Emails
 * @version 2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri();?>/emails/fonts/style.css">
    <title>Verbuggen Registratie complete</title>
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.png">
    <link href="" rel="stylesheet">
    <style>
        @font-face {
            font-family: 'Circular Std Book';
            font-style: normal;
            font-weight: normal;
            src: local('Circular Std Book'), url('<?php echo get_template_directory_uri();?>/emails/fonts/CircularStd-Book.woff') format('woff');
        }




        @font-face {
            font-family: 'Circular Std Bold';
            font-style: normal;
            font-weight: normal;
            src: local('Circular Std Bold'), url('<?php echo get_template_directory_uri();?>/emails/fonts/CircularStd-Bold.woff') format('woff');
        }


    </style>
</head>
<body <?php echo is_rtl() ? 'rightmargin' : 'leftmargin'; ?>="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">