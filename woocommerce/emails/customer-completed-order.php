<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri();?>/emails/fonts/style.css">
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.png">
	<link href="" rel="stylesheet">
	<style>
		@font-face {
			font-family: 'Circular Std Book';
			font-style: normal;
			font-weight: normal;
			src: local('Circular Std Book'), url(<?php echo get_template_directory_uri().'/emails/CircularStd-Book.woff';?>) format('woff');
		}




		@font-face {
			font-family: 'Circular Std Bold';
			font-style: normal;
			font-weight: normal;
			src: local('Circular Std Bold'), url(<?php echo get_template_directory_uri().'/emails/CircularStd-Bold.woff';?>) format('woff');
		}


	</style>
</head>
<body>
<table style="margin:0 auto;width: 750px; font-family: Circular Std Book, sans-serif; font-size:18px; border-collapse:collapse; background:#FFFFFF; color:#31383e;" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td colspan="3" height="25">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3">
			<table  width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="3"><a href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri().'/emails/images/logo.png'; ?>"  border="0"/></a></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="3" height="45">&nbsp;</td>
	</tr>

	<tr>
		<td bgcolor="#ed3024" style="background-image:url(<?php echo get_template_directory_uri().'/emails/images/bg-food-vector.png'; ?>); background-repeat:no-repeat; padding:15px 9%; background-position:left center;background-size: cover;">
			<h1 style="color:#fff; font-size:50px; margin:0; line-height:1.1;font-family: Circular Std Bold, sans-serif;">Bestelling #<?php echo $order->get_id(); ?></h1>
		</td>
	</tr>
	<tr>
		<td colspan="3" height="45">&nbsp;</td>
	</tr>

	<tr>
		<td colspan="3" style=" padding:15px 9%;">
			<h2 style="font-family: Circular Std Book, sans-serif; font-size:40px; margin:0 0 30px;">Bedankt voor uw bestelling!</h2>
			<p style="font-family: Circular Std Book, sans-serif; font-size:18px; margin:0 0 30px; line-height:1.6;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>

			<p style="font-size:18px; margin:0 0 30px; line-height:1.6;">
				<span style="font-family: Circular Std Bold, sans-serif;">Bestel datum:</span><br />
				</span>
				<?php
				$order = wc_get_order($order->get_id());
				$date = $order->get_date_created()->format ('j F ,Y- g:i');
				?>
				<span style="font-family: Circular Std Book, sans-serif;"><?php echo $date; ?></span>
			</p>

			<p style="font-size:18px; margin:0 0 30px; line-height:1.6;">
				<span style="font-family: Circular Std Bold, sans-serif;">Ordernummer:</span><br />
				</span>
				<span style="font-family: Circular Std Book, sans-serif;"><?php echo $order->get_id(); ?></span>
			</p>


			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr style="font-family: Circular Std Bold, sans-serif; ">
					<td style="border-bottom:2px solid #f69791; padding:20px 0;">Producten:</td>
					<td style="border-bottom:2px solid #f69791; padding:20px 0;">Aantal:</td>
					<td style="border-bottom:2px solid #f69791; padding:20px 0;">Prijs:</td>
				</tr>
				<?php
				foreach( $order->get_items() as $item_id => $item ){
					$product_id = $item->get_product_id();
					$product = $item->get_product();
					$price = $product->get_price();
					$product_quantity = $item->get_quantity();
					$product_name = $item->get_name();
					$size = $product->get_attribute( 'pa_size' );
					?>

					<tr>
						<td style="border-bottom:2px solid #979b9e; padding:20px 0;"><?php echo $product_name; ?></td>
						<td style="border-bottom:2px solid #979b9e; padding:20px 0;">Krat: <?php echo $size; ?> <br />Aantal: <?php echo $product_quantity; ?></td>
						<td style="border-bottom:2px solid #979b9e; padding:20px 0;"><span style="font-family: Circular Std Bold, sans-serif; color:#ed3024;">&euro;<?php echo $price; ?></span></td>
					</tr>

				<?php } ?>

				<tr style="font-family: Circular Std Bold, sans-serif; ">
					<td rowspan="3" style="padding-top:20px;">
						<span>Betallmethode:</span><br />
						<span style="font-family: Circular Std Book, sans-serif;">Factuur</span>
					</td>
					<td style="padding-top:20px;">Sub totaal:</td>
					<td style="padding-top:20px;">&euro; <?php echo $order->total; ?></td>
				</tr>
				<tr style="font-family: Circular Std Bold, sans-serif; ">
					<td style="padding-top:5px;">BTW (0%):</td>
					<td style="padding-top:5px;">&euro; <?php echo $order->total_tax; ?></td>
				</tr>
				<tr style="font-family: Circular Std Bold, sans-serif; ">
					<td style="padding-top:5px;">Totaal:</td>
					<td style="padding-top:5px;"><span style="color:#ed3024;">&euro; <?php echo $order->total; ?></span></td>
				</tr>

				<tr>
					<td colspan="3" style="border-bottom:2px solid #f69791; padding-top:50px;"></td>
				</tr>
			</table>

		</td>
	</tr>

	<tr>
		<td colspan="3" style=" padding:20px 9% 0;">
			<p style="font-family: Circular Std Book, sans-serif; font-size:18px; margin:0 0 30px; line-height:1.6;">Als u besluit uw bestelling te annuleren kunt u dit in uw  <a href="<?php echo home_url(); ?>" style="color:#ed3024; text-decoration:underline;">account</a> aanpassen.</p>

			<p style="font-family: Circular Std Book, sans-serif; font-size:18px; margin:0 0 30px; line-height:1.6;">
				Bedankt,<br />
				Het Verbruggen team.
			</p>
			<p style="font-family: Circular Std Book, sans-serif; font-size:18px; margin:0 0 30px; line-height:1.6;">Heeft u vragen? Dan kunt ons <a href="<?php echo home_url(); ?>" style="color:#ed3024; text-decoration:underline;">mailen</a> of bellen op werkdagen van 8:30 tot 19:00 op: <font style="color:#ed3024; text-decoration:underline;">0485 451 220</font></p>
		</td>
	</tr>

</table>
</body>
</html>