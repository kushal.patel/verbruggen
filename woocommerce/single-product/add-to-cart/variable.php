<?php
/**
 * Variable product add to cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/add-to-cart/variable.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.1
 */

defined( 'ABSPATH' ) || exit;

global $product;

$attribute_keys = array_keys( $attributes );

do_action( 'woocommerce_before_add_to_cart_form' ); ?>

<form class="variations_form cart" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>" method="post" enctype='multipart/form-data' data-product_id="<?php echo absint( $product->get_id() ); ?>" data-product_variations="<?php echo htmlspecialchars( wp_json_encode( $available_variations ) ); // WPCS: XSS ok. ?>">
	<?php do_action( 'woocommerce_before_variations_form' ); ?>

	<?php if ( empty( $available_variations ) && false !== $available_variations ) : ?>
		<p class="stock out-of-stock"><?php esc_html_e( 'This product is currently out of stock and unavailable.', 'woocommerce' ); ?></p>
	<?php else : ?>


    <?php
//    echo "<Pre>";
//    print_r($available_variations);
    $targeted_id = $product->get_id();  //product id

    $value = 0;

    ?>

    <?php
    foreach($available_variations as $key=>$variation){

        foreach ( WC()->cart->get_cart() as $cart_item ) {
            if($cart_item['product_id'] == $targeted_id && $cart_item['variation_id'] == $variation['variation_id'] ){
                $value =  $cart_item['quantity'];
                break; // stop the loop if product is found
            }
            else{
                $value = 0;
            }
        }

        ?>

        <div class="product-status d-flex justify-content-between">
            <div class="krat-box">
                <h3 class="mb-4"><?php echo $variation['attributes']['attribute_pa_size']; ?></h3>
                <div class="count-input left">
                    <input type="hidden" class="add_to_cart_button" data-product_id="<?php echo $product->get_id(); ?>" variationid="<?php echo $variation['variation_id']; ?>" />
                    <a class="incr-btn" data-action="decrease" href="#"></a>
                    <input class="quantity" type="text" name="quantity" value="<?= $value; ?>">
                    <a class="incr-btn" data-action="increase" href="#"></a>
                </div>
            </div>

            <div class="krat-box text-right">
                <h3 class="mb-4"><strong>p/krat &euro;<?php echo $variation['display_price']; ?></strong></h3>
                <!--<div class="btn-grp d-flex">
                    <a href="" class="menu-bar"><i></i></a>
                    <a href="" class="btn btn-primary btn-sm">Voeg toe</a>
                </div>-->
            </div>
        </div>

    <?php }
    ?>



	<?php endif; ?>

</form>

<?php

