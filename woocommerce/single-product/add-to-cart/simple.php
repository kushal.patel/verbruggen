<?php
/**
 * Simple product add to cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/add-to-cart/simple.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

if ( ! $product->is_purchasable() ) {
    return;
}

//echo wc_get_stock_html( $product ); // WPCS: XSS ok.

$targeted_id = $product->get_id();  //product id

$value = 0;
foreach ( WC()->cart->get_cart() as $cart_item ) {
    if($cart_item['product_id'] == $targeted_id ){
        $value =  $cart_item['quantity'];
        break; // stop the loop if product is found
    }
}

$Stockstatus = $product->get_stock_status();


$brandname = array_shift( wc_get_product_terms( $product->get_id(), 'pa_brand', array( 'fields' => 'names' ) ) );
$size =  array_shift( wc_get_product_terms( $product->get_id(), 'pa_size', array( 'fields' => 'names' ) ) );

if ( $product->is_in_stock() ) : ?>

    <div class="product-status d-flex justify-content-between">
        <div class="krat-box">
            <h3 class="mb-4"><?php if(!empty($size)){echo $size; } else { echo " One Item ";} ?></h3>
            <div class="count-input left">
                <input type="hidden" class="add_to_cart_button" data-product_id="<?php echo $product->get_id(); ?>" />
                <a class="incr-btn" data-action="decrease" href="#"></a>
                <input class="quantity" type="text" name="quantity" value="<?= $value; ?>">
                <a class="incr-btn" data-action="increase" href="#"></a>
            </div>
        </div>

        <div class="krat-box text-right">
            <h3 class="mb-4"><strong>p/krat &euro;<?php echo number_format((float)$product->get_price(), 2, '.', ''); ?></strong></h3>
            <p class="price"><?php echo $product->get_price_html(); ?></p>
        </div>
    </div>



    <!--<div class="shop-action"><a href="" class="btn btn-added">Toegevoegd</a></div>-->

<?php endif; ?>
