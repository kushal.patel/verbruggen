<?php
/**
 * Related Products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/related.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $post,$product,$woocommerce;

if ( $related_products ) : ?>

	<section class="related products">
        <div class='section-heading mb-5'>
            <h1>Wij denken met u mee</h1>
            <h3>Daarom maakt verbruggen een selectie van producten<br />
            waarvan wij denken dat deze anasluit bij dit product</h3>
        </div>

        <div class="owl-carousel product-slider btn-nxt-prev owl-loaded owl-drag">
            <div class="owl-stage-outer">
                <div class="owl-stage" style="transition: all 1s ease 0s; width: 1900px; transform: translate3d(-760px, 0px, 0px);">
                    <?php foreach ( $related_products as $related_product ) :
                        $myproduct = $related_product->get_data();

                        if(empty($myproduct['tag_ids'])){
                            $aanbiedingenclass = "";
                            $opclass = "";
                        }
                        else{
                            foreach($myproduct['tag_ids'] as $Key=>$tag_id){
                                $tag = get_term( $tag_id, "product_tag");
                                $tagslug = $tag->slug;
                                if($tagslug == "opop"){
                                    $opclass = "offer";
                                }

                                if($tagslug == "aanbieding"){
                                    $aanbiedingenclass = "aanbied";
                                }
                            }
                        }
                        $overallclass = $opclass." ".$aanbiedingenclass;
                        ?>

                        <div class="owl-item" style="width: 370px; margin-right: 10px;">

                            <div class="product-block  <?php echo $overallclass; ?>">
                                <?php
                                $myproduct = $related_product->get_data();

                                foreach($myproduct['tag_ids'] as $Key=>$tag_id){
                                    $tag = get_term( $tag_id, "product_tag");
                                    $tagname = strtoupper($tag->name);
                                    echo '<div class="pro-tag">'.$tagname.'</div>';
                                }

                                $featured_image_link = "";
                                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $related_product->get_id()), 'single-post-thumbnail' );

                                $brand = $related_product->get_attribute( 'pa_brand' );
                                $size = $related_product->get_attribute('pa_size');
                                $qty = $related_product->get_attribute('pa_quantity');

                                if(!empty($image))
                                    $featured_image_link = $image[0];
                                ?>
                                <div class="pro-img">
                                    <a href="<?php echo $related_product->get_permalink(); ?>" class="item-img">
                                        <img src="<?php echo $featured_image_link; ?>" class="img-responsive slider-product-images">
                                    </a>
                                </div>
                                <div class="product-info">
                                    <h4 class="light"><a href="<?php echo $related_product->get_permalink(); ?>"><?php echo $related_product->get_title(); ?></a></h4>
                                    <h4 class="product-qty"><?php echo $qty; ?></h4>
                                </div>
                                <?php
                                    if(empty($brand)){
                                        echo "<div class='product-title'><h4>&nbsp;</h4></div>";
                                    }else{
                                        echo "<div class='product-title'><h4><a href='$url '>$brand</a></h4></div>";
                                    }
                                    if(empty($size)){
                                        echo "<div class='product-size'><h4>&nbsp;</h4></div>";
                                    }else{
                                        echo "<div class='product-size'><h4>$size</h4></div>";
                                    }

                                $targeted_id = $related_product->get_id();  //product id

                                $value = 0;
                                foreach ( WC()->cart->get_cart() as $cart_item ) {
                                    if($cart_item['product_id'] == $targeted_id ){
                                        $value =  $cart_item['quantity'];
                                        break; // stop the loop if product is found
                                    }
                                }

                                $Stockstatus = $related_product->get_stock_status();
                                ?>
                                <?php
                                if(!$related_product->has_child() && $Stockstatus == "instock")
                                ?>
                                <div class="count-input">
                                    <input type="hidden" class="add_to_cart_button" data-product_id="<?php echo $related_product->get_id(); ?>" style="display: none;">
                                    <a class="incr-btn" data-action="decrease" href="#"></a>
                                    <input class="quantity" type="text" name="quantity" value="<?php echo $value;  ?>">
                                    <a class="incr-btn" data-action="increase" href="#"></a>
                                </div>
                                <?php
                                if ( $price_html = $related_product->get_price_html()) {
                                    echo "<span class='item-price'>$price_html</span>";
                                }
                                ?>
                            </div>

                        </div>

                    <?php endforeach; ?>
                </div>
            </div>
        </div>
	</section>

<?php endif;

wp_reset_postdata();
