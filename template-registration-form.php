<?php
/**
 * The template for displaying the homepage.
 *
 * This page template will display any functions hooked into the `homepage` action.
 * By default this includes a variety of product displays and the page content itself. To change the order or toggle these components
 * use the Homepage Control plugin.
 * https://wordpress.org/plugins/homepage-control/
 *
 * Template name: Registration-User
 *
 * @package storefront
 */

 if ( is_user_logged_in() ) {
   $url = home_url();
   wp_redirect( $url );
   // exit;
 }else{

wp_head();
get_header();
$pagename = get_query_var('pagename');
// print_r($pagename);die;
if($pagename == 'registration'){ ?>
  <script>
  jQuery(window).on('load',function() {
    jQuery('body').addClass('inner-page');
  });
  </script>
<?php } ?>


    <title><?php echo get_bloginfo(); ?> - <?php echo the_title(); ?></title>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() . '/plugins/select/selectize.css'; ?>">
<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">

        <div class="blank-banner">

        </div>

        <section class="bg-white pad80">
            <div class="container">
                <span id="myhtml">
                <h1 class="heading1 text-center">Aanmelden als nieuwe klant</h1>
                </span>

                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <form role="form" method="post"  class="form-wizard sm-cols input-black-text">
                            <div class="form-wizard-steps pad80">
                                <div class="form-wizard-step active">
                                    <h3><strong>Contactpersoon</strong></h3>
                                </div>
                                <div class="form-wizard-step">
                                    <h3><strong>Bedrijfsgegevens</strong></h3>
                                </div>
                                <div class="form-wizard-step">
                                    <h3><strong>Bevestiging</strong></h3>
                                </div>
                            </div>

                            <fieldset class="step-forms f1">
                                <h2 class="heading-bdr-red">Contactpersoon</h2>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label class="h3"><strong>Naam</strong></label>
                                            <input type="text" placeholder="Voor- en achternaam"
                                                   class="form-control" id="name" name="name">
                                        </div>

                                        <div class="col-md-12">
                                            <label class="h3"><strong>E-mail adres</strong></label>
                                            <input type="text" placeholder="Email" id="email" name="email" class="form-control">
                                        </div>
                                        <span id="email_error" class="error" style="color: red;display: none;margin-left: 5px;">E-mail adres al in gebruik</span>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="h3"><strong>Telefoonnummer</strong></label>
                                    <input type="text" placeholder="Vast- en/of mobielnummer" id="telephone" name="telephone" class="form-control">
                                </div>
                                <div class="form-wizard-buttons pt-4 pb-3">
                                    <a href="javascript:void(0)" id="next-step-f2"  class="btn-border btn-next-step h3">Bedrijfsgegevens</a>
                                </div>
                            </fieldset>

                            <fieldset class="step-forms f2">
                                <h2 class="heading-bdr-red">Bedrijfsgegevens</h2>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label class="h3"><strong>KvK gegevens</strong></label>
                                            <input type="text"
                                                   placeholder="Vul hier uw KvK-nummer of Ondernemersnummer in"
                                                   class="form-control" name="company_number" id="company_number">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <label class="h3"><strong>BTW</strong></label>
                                            <input type="text" placeholder="Vul hier uw BTW-nummer in"
                                                   class="form-control" name="vat" id="vat">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label class="h3"><strong>IBAN</strong></label>
                                            <input type="text" placeholder="Vul hier uw IBAN-nummer in"
                                                   class="form-control" name="bank_acc_no" id="bank_acc_no">
                                        </div>

                                        <div class="col-sm-3">
                                            <label class="h3"><strong>BIC</strong></label>
                                            <input type="text" placeholder="Vul hier uw BIC-nummer in"
                                                   class="form-control" name="bank_code" id="bank_code">
                                        </div>
                                        <div class="col-sm-3">
                                            <label class="h3"><strong>EORI</strong></label>
                                            <input type="text" placeholder="Vul hier uw EORI-nummer in"
                                               class="form-control" name="eori" id="eori">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <label class="h3"><strong>Bedrijfsnaam</strong></label>
                                            <input type="text" placeholder="Vul hier uw voor- en achternaam in"
                                                   class="form-control" name="company_name" id="company_name">
                                        </div>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="h3"><strong>Adres</strong></label>
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12">
                                            <input type="text" placeholder="Straatnaam" id="street" name="street" class="form-control">
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-4">
                                            <input type="text" placeholder="Huisnr" name="house_no" id="house_no" class="form-control">
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-8">
                                            <input type="text" placeholder="Toevoeging" name="addition" id="addition" class="form-control">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3 col-sm-4">
                                            <input type="text" placeholder="Postcode / postbus"
                                                   class="form-control" name="postcode" id="postcode">
                                        </div>
                                        <div class="col-md-5 col-sm-4">
                                            <input type="text" placeholder="Plaatsnaam" name="city" id="city" class="form-control">
                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <div class="form-control-file">
                                                <select class="select full" name="landmark" id="landmark">
                                                    <option value="1">Land</option>
                                                    <option value="2">Land 1</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="terms-area">
                                    <label class="check-bl text-d-grey" for="keep-act">
                                        <em>Factuuradres is niet gelijk aan opgegeven adres</em>
                                        <input type="checkbox" id="keep-act" name="different_address" value="on">
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="check-bl text-d-grey">
                                        <em>Afleveradres is niet gelijk aan opgegeven adres</em>
                                        <input type="checkbox" name="diiff_delivery_adddress">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>

                                <div class="form-group" id="extra-field" style="display:none;">
                                    <label class="h3"><strong>Adres</strong></label>
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12">
                                            <input type="text" placeholder="Straatnaam" name="shipping_street" id="shipping_street" class="form-control">
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-4">
                                            <input type="text" placeholder="Huisnr" name="shipping_house_no" id="shipping_house_no" class="form-control">
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-8">
                                            <input type="text" placeholder="Toevoeging" name="shipping_addition" id="shipping_addition" class="form-control">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3 col-sm-4">
                                            <input type="text" placeholder="Postcode / postbus"
                                                   class="form-control" name="shipping_postcode" id="shipping_postcode">
                                        </div>
                                        <div class="col-md-5 col-sm-4">
                                            <input type="text" placeholder="Plaatsnaam" name="shipping_city" id="shipping_city" class="form-control">
                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <div class="form-control-file">
                                                <select class="select full" name="shipping_landmark" id="shipping_landmark">
                                                    <option value="landmark-1">Landmark 1</option>
                                                    <option value="landmark-2">Landmark 2</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>


                                </div>

                                <div class="form-group">
                                    <label class="h3"><strong>Branche</strong></label>
                                    <div class="form-control-file">
                                        <select class="select full" name="industry">
                                            <option value="1">Selecteer uw branche</option>
                                            <option value="2">Selecteer uw branche 1</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="h3"><strong>Bericht</strong></label>
                                    <textarea type="text" rows="8"
                                              placeholder="Heeft u vragen en/of opmerkingen? Laat hier uw bericht achter"
                                              class="form-control" name="message" id="message"></textarea>
                                </div>
                                <div class="form-wizard-buttons pt-4 pb-3">
									<div class="row">
                                    	<div class="col-6 text-left">
                                        	<a href="javascript:void()" class="btn-border btn-previous">Aanpassen</a>
                                         
                                        </div>
                                        <div class="col-6">
                                        	<a href="javascript:void(0)" id="next-step-f3" class="btn-border btn-next-step h3">Bevestiging</a>
                                        </div>
                                    </div>
                                </div>
                                 
                            </fieldset>

                            <fieldset class="step-forms f3">
                                <h2 class="heading-bdr-red">Bevestiging</h2>
                                <h4 class="mb-5">Controle alle gegevens nauwkeurig.<br/>
                                    Als alle gegevens correct ingevuld zijn kunt in deze bevestigen</h4>
                                <h2 class="heading-bdr-red">Contactpersoon</h2>

                                <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="h5 text-d-grey">Naam</label>
                                                <div class="input-data h3 bold" id="user_name"></div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="h5 text-d-grey">E-mail adres</label>
                                                <div class="input-data h3 bold" id="user_email">JanJansen@bedrijf.nl</div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="h5 text-d-grey">Telefoonnummer</label>
                                                <div class="input-data h3 bold" id="user_telephone">0612345678</div>
                                            </div>
                                        </div>
                                    </div>

                                <div class="form-wizard-buttons pt-4 pb-3">
                                    <a href="javascript:void(0)" class="bk-step-1 btn-border h3">Aanpassen</a>
                                </div>

                                
								<h2 class="heading-bdr-red">Bedrijfsgegevens</h2>
                                <div class="form-group">
                                		<div class="row">
                                            <div class="col-md-12">
                                                <label class="h5 text-d-grey">Bedrijfsnaam</label>
                                                <div class="input-data h3 bold" id="user_company_name">Bedrijf</div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <label class="h5 text-d-grey">Adres</label>
                                                <div class="input-data h3 bold" id="user_address">Naamstraat 61 1234AB Amsterdam Nederland
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="h5 text-d-grey">Kvk gegevens</label>
                                                <div class="input-data h3 bold" id="user_company_number">12345678</div>
                                            </div>
                                        </div>
                                    
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="h5 text-d-grey">IBAN</label>
                                                <div class="input-data h3 bold" id="user_iban">NL15ABNA 012345678</div>
                                            </div>
                                            <div class="col-md-4">
                                                <label class="h5 text-d-grey">BIC</label>
                                                <div class="input-data h3 bold" id="user_bic">ABCDEFG1EGF</div>
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="h5 text-d-grey">BTW</label>
                                                <div class="input-data h3 bold" id="user_btw">NL12345678B01</div>
                                            </div>
                                            
                                            <div class="col-md-4">
                                                <label class="h5 text-d-grey">EORI</label>
                                                <div class="input-data h3 bold" id="user_eori">ABCDEFG2EGF</div>
                                            </div>
                                        </div>

                                        
                                    </div>

                                <div class="form-wizard-buttons pt-4 pb-3">
                                    <a href="javascript:void()" class="btn-border btn-previous h3">Aanpassen</a>
                                </div>

                                <div class="form-group">
                                    <label class="h3"><strong>Bericht</strong></label>
                                    <textarea type="text" rows="8"
                                              placeholder="Heeft u vragen en/of opmerkingen? Laat hier uw bericht achter"
                                              class="form-control" name="message" id="user_message"></textarea>
                                </div>

                                <div class="terms-area">
                                    <label id="label_terms" class="check-bl text-d-grey">Hierbij wordt bevestigd dat de ingevulde
                                        gegevens naar waarheid en juist zijn ingevuld.
                                        <input type="checkbox" name="terms" id="terms_cond">
                                        <span class="checkmark" id="span_terms_checkmark"></span>
                                    </label>
                                </div>

                                <div class="form-wizard-buttons pt-4 pb-3">
                                    <!--                                    <div class="loader" id="form_submit_loader" style="display: none;"></div>-->
                                    <input type="submit" name="submit" value="Klant Worden" class="btn btn-primary btn-lg">
                                    <a href="javascript:void(0)" style="display:none;" id="next-step-f4" class="btn-border btn-next-step h3">Bedrijfsgegevens</a>
                                </div>
                            </fieldset>

                            <fieldset  class="welcome-user">
                                <h2 class="heading-bdr-red">Bevestiging verzending</h2>
                                <h1 class="mb-3 pt-3">Bedankt! <br/>Uw aanmelding is in verzonden.</h1>
                                <h3>Wij nemen spoedig contact met u op.</h3>
                            </fieldset>
                        </form>


                    </div>
                </div>
            </div>
        </section>


        <?php

        }
        $content = get_the_content();

        if (trim($content) == "") // Check if the string is empty or only whitespace
        {
        } else {
        }
        while (have_posts()) :
            the_post();

            the_content();

        endwhile;

        ?>

    </main><!-- #main -->
</div><!-- #primary -->


<script>
    jQuery(function ($) {
        $("#keep-act").click(function () {
            if ($(this).is(":checked")) {
                $("#extra-field").fadeIn();
            } else {
                $("#extra-field").fadeOut();
            }
        });

    });
</script>
<?php


get_footer();
