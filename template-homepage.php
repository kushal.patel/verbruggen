<?php
/**
 * The template for displaying the homepage.
 *
 * This page template will display any functions hooked into the `homepage` action.
 * By default this includes a variety of product displays and the page content itself. To change the order or toggle these components
 * use the Homepage Control plugin.
 * https://wordpress.org/plugins/homepage-control/
 *
 * Template name: Homepage
 *
 * @package storefront
 */
?>
<head>
<?php wp_head(); ?>
</head>

<?php
get_header();

$image = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()),'full' , true);
if(empty($image)){
	$image[0] = '';
}
?>

<title><?php echo get_bloginfo('name'); ?> - <?php echo the_title(); ?></title>
<style>
    .customsliderbutton:hover{
        background-color: transparent !important;
        border-color: transparent !important;
        text-decoration: : none !important;
    }
.banner-slider .item.slide1 {
	background-image:url('<?php echo $image[0]; ?>');
	background-position:center;
}
.banner-slider .item.slide2 {
	background-image:url('<?php echo $image[0]; ?>');
	background-position:center;
}
</style>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			/**
			 * Functions hooked in to homepage action
			 *
			 * @hooked storefront_homepage_content      - 10
			 * @hooked storefront_product_categories    - 20
			 * @hooked storefront_recent_products       - 30
			 * @hooked storefront_featured_products     - 40
			 * @hooked storefront_popular_products      - 50
			 * @hooked storefront_on_sale_products      - 60
			 * @hooked storefront_best_selling_products - 70
			 */
			// do_action( 'homepage' );

			while ( have_posts() ) :
				the_post();

				the_content();

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
