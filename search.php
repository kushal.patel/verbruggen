<?php
global $id;
global $wp_query;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Verbuggen</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" name="viewport">
    <?php wp_head(); ?>
    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500" rel="stylesheet">

    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri().'/images'; ?>/favicon.png">
    <style>
        .columns-3{
            width : 100% !important;
        }
    </style>

</head>

<?php
$url = $_SERVER['REQUEST_URI'];
$productcategory = false;
$finalcategory = '';

$orderby = 'name';
$order = 'asc';
$hide_empty = false ;
$cat_args = array(
    'orderby'    => $orderby,
    'order'      => $order,
    'hide_empty' => $hide_empty,
);

$product_categories = get_terms( 'product_cat', $cat_args );


if (strpos($url, 'product-category') !== false) {
    $productcategory = true;
    $categoryarray = explode("/",$url);
    $category = $categoryarray[count($categoryarray)-2];

    foreach($product_categories as $Key=>$mycategory){
        if($mycategory->slug == $category){
            $finalcategory = $mycategory;
        }
    }
    //echo 'true';
}

if(is_front_page()){
    $class='smooth-scroll';
}
else{
    $class='inner-page';
}
?>




<body class="inner-page">



<?php $url = get_template_directory_uri().'/images/bg-category.png';  ?>


<?php get_header(); ?>

<div class="inner-banner" style="background-image:url(<?php echo $url; ?>);">
    <div class="container">
        <h1><?php echo get_the_title(); ?></h1>
        <h4>Search results for : "<?php the_search_query(); ?>"</h4>
        <br/>
    </div>
</div>


<section class="bg-white pad80">
    <div class="container">
        <div class="category-wrapper">
            <div class="pro-cat-cols">
                <div class="row">
                    <aside class="col-md-3">


                        <div class="pro-filter filter-bl">
                            <h2 class="heading2">Producten</h2>
                            <ul class="list">
                                <?php if(empty($finalcategory)){
                                    foreach($product_categories as $Key=>$mycategory){ ?>
                                        <li><a href="<?php echo  get_site_url()."/product-category/".$mycategory->slug; ?>"><?php echo $mycategory->name; ?></a></li>
                                    <?php } ?>
                                <?php } else{
                                    $cat_args = array(
                                        'orderby'    => $orderby,
                                        'order'      => $order,
                                        'hide_empty' => $hide_empty,
                                        'parent' => $finalcategory->term_id
                                    );
                                    $subcategories = get_terms( 'product_cat', $cat_args );
                                    foreach($subcategories  as $Key=>$mycategory){ ?>
                                        <li><a href="<?php echo  get_site_url()."/product-category/".$mycategory->slug; ?>"><?php echo $mycategory->name; ?></a></li>
                                    <?php }

                                } ?>

                            </ul>
                        </div>



                        <div class="filters filter-bl">
                            <?php
                            if(is_active_sidebar('home_right_1')){
                                dynamic_sidebar('home_right_1');
                            }
                            ?>
                        </div>
                    </aside>
                    <!--end filters -->


                    <section class="col-md-9">
                        <div class="products-full-wrap clearfix">
                            <div class="products row">
                                <?php

                                if(have_posts()){
                                    while ( have_posts() ) :
                                        the_post();
                                        $product = wc_get_product();
                                        $pricehtml = "";
                                        $product = wc_get_product($post->ID);
                                        $pricehtml = $product->get_price_html();

                                        $StockQ = $product->get_stock_quantity();

                                        $stockstatus = $product->get_stock_status();

                                        if($StockQ >= 1 && $stockstatus == "instock"){
                                            $op = "";
                                        }
                                        else if($StockQ < 1 && $stockstatus == "outofstock"){
                                            $op = "<div class='pro-tag'>OP =OP</div>";
                                        }


                                        if($product->is_on_sale()){
                                            $sale = "<div class='pro-tag'>AANBIEDING</div>";
                                            $saleclass = "aanbied";
                                        }else{
                                            $sale = "";
                                            $saleclass = "";
                                        }

                                        $targeted_id = $id = $product->get_id();
                                        $count = 0;
                                        if(!is_null(WC()->cart)) {
                                            foreach (WC()->cart->get_cart() as $cart_item) {
                                                if ($cart_item['product_id'] == $targeted_id) {
                                                    $count = $cart_item['quantity'];
                                                    break; // stop the loop if product is found
                                                }
                                            }
                                        }
                                        $html = "";
                                        if( !$product->has_child() && $stockstatus == "instock") {
                                            $html .= "
                                                <input type='hidden' class='add_to_cart_button' data-product_id='$id' />
                                                <a class='incr-btn' data-action='decrease' href='#'></a>
                                                <input class='quantity' type='text' name='quantity' value='$count'>
                                                <a class='incr-btn' data-action='increase' href='#'></a>";
                                        }

                                        echo "<div class='col-md-4 col-sm-6'>
	<div class='product-block $saleclass'>
    	<div class='pro-img'>
    	    $op
    	    $sale
            <a href='"; the_permalink(); echo"'>"; the_post_thumbnail('thumbnail'); echo"</a>
        </div>
        <div class='product-info'>
            <h4 class='light'><a href='"; the_permalink(); echo "' > ";  the_excerpt(); echo "</a></h4>
            <h4 class='product-qty'>0,75cl</h4>
        </div>
        <span class='price'>
         $pricehtml
        </span>
        <div class='product-title'>
            <h4><a href='"; the_permalink();  echo "'>";the_title();echo "</a></h4>
        </div>
        <div class='it-input-qt'>
            <div class='count-input'>
                $html
            </div>
        </div>
    </div>
</div>";


                                    endwhile;
                                }
                                else{ ?>
                                    <h3>Sorry we did not find any results from your search - " <?php the_search_query(); ?> "</h3>
                                <?php } ?>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</section>


<?php get_footer(); ?>
