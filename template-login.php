<title><?php echo get_bloginfo('name'); ?> - <?php echo the_title(); ?></title>
<?php
/**
 * The template for displaying the Login-template.
 *
 * This page template will display any functions hooked into the `Login-template` action.
 * By default this includes a variety of product displays and the page content itself. To change the order or toggle these components
 * use the Homepage Control plugin.
 * https://wordpress.org/plugins/homepage-control/
 *
 * Template name: Login-template
 *
 * @package storefront
 */
if ( is_user_logged_in() ) {
    $url = home_url();
    wp_redirect( $url );
     exit;
}else {
    wp_head();
    get_header();
    global $id;
    global $wp_query;
    $pagename = get_query_var('pagename');
// print_r($pagename);die;
    if ($pagename == 'login') { ?>
        <script>
            jQuery(window).on('load', function () {
                jQuery('body').addClass('inner-page');
            });
        </script>
    <?php } ?>
    <style>
        .error {

        }
    </style>
    <link rel="icon" type="image/png" sizes="16x16"
          href="<?php echo get_template_directory_uri() . '/images/favicon.png'; ?>">

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
            <div class="blank-banner">

            </div>
            <section class="bg-white pad80 pt-0">
                <div class="container">
                    <div class="heading-block-red pad-3"><h1>Inloggen</h1></div>
                    <div class="form-section">
                        <h2 class="f-30 mb-4 red-bdr">Inloggen in Mijn Verbruggen </h2>
                        <form class="input-black-text login-form" method="post">
                            <div class="error" id="div_error_login" style="display:none;color: #f35b3f;">username or password wrong.</div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="h3 bold lts-77"><strong>Gebruikersnaam</strong></label>
                                        <input type="text" placeholder="Vul uw gebruikersnaam in" name="username"
                                               id="username" class="form-control mb-0"/>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">

                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="h3 bold">Wachtwoord</label>
                                        <div class="input-with-link">
                                            <input type="password" placeholder="Wachtwoord" name="password"
                                                   id="password" class="form-control mb-0"/>
                                            <a class="forogt-link" href="<?php echo wp_lostpassword_url(); ?>">Wachtwoord vergeten?</a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="terms-area">
                                <label class="check-bl text-d-grey">
                                    <span class="text-li-black">Ingelogd blijven</span>
                                    <input type="checkbox">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="form-group text-sm-right">
                                <input type="submit" name="save" value="Inloggen" class="btn btn-primary btn-lg mt-3">
                            </div>
                        </form>
                    </div>
                </div>
            </section>

            <?php
            /**
             * Functions hooked in to homepage action
             *
             * @hooked storefront_homepage_content      - 10
             * @hooked storefront_product_categories    - 20
             * @hooked storefront_recent_products       - 30
             * @hooked storefront_featured_products     - 40
             * @hooked storefront_popular_products      - 50
             * @hooked storefront_on_sale_products      - 60
             * @hooked storefront_best_selling_products - 70
             */
            // do_action( 'homepage' );

            while (have_posts()) :
                the_post();

                the_content();

            endwhile; // End of the loop.
            ?>

        </main><!-- #main -->
    </div><!-- #primary -->
    <?php
}
  get_footer();
