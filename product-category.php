<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Verbuggen</title>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" name="viewport">
<link rel="stylesheet" type="text/css" href="plugins/bootstrap/bootstrap.css">
<link rel="stylesheet" type="text/css" href="plugins/fonts/style.css">
<link rel="stylesheet" type="text/css" href="style.css">
<link rel="stylesheet" type="text/css" href="plugins/owl-slider/owl.css">
<link rel="stylesheet" type="text/css" href="css/animate.css">
<link rel="stylesheet" type="text/css" href="css/responsive.css">
<link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="plugins/select/selectize.css">
<link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
<link rel="stylesheet" href="plugins/scrollbar/jquery.mCustomScrollbar.css">
</head>

<body class="inner-page">
<?php include 'header.php';?>


<div class="inner-banner" style="background-image:url(images/bg-category.png);">
	<div class="container">
    	<h1>Drank</h1>
    </div>
</div>
<!-- end banner -->


<section class="bg-white pad80">
	<div class="container">
    	<div class="category-wrapper">
            <div class="shop-filter d-flex justify-content-end">
                <div class="order-by align-content-end">
                    
                    <form>
                        <select class="select">
                            <option value="1">Sorteer A-Z</option>
                            <option value="2">Sorteer 1</option>
                            <option value="3">Sorteer 2</option>
                            <option value="4">Sorteer 3</option>
                        </select>
                    </form>

                </div>
            </div>
            
            <div class="pro-cat-cols">
            	<div class="row">
            	<aside class="col-md-3">
                	<div class="pro-filter filter-bl">
                    	<h2 class="heading2">Producten</h2>
                        <ul class="list">
                        	<li><a href="">Wijnen</a></li>
                            <li><a href="">Bieren</a></li>
                            <li><a href="">Frisdranken</a></li>
                            <li><a href="">Gedistilleerd</a></li>
                        </ul>
                    </div>
                    
                    <div class="filters filter-bl">
                    	<h2 class="heading2">Filters</h2>
                        <ul class="list-sm">
                          <li>
                          	<label class="radio-bl">Non - alcoholisch
                          	  <input type="checkbox">
                              <span class="checkmark"></span>
                            </label>
                          </li>
                          <li>
                          	<label class="radio-bl">Alcoholisch
                          	  <input type="checkbox">
                              <span class="checkmark"></span>
                            </label>                          
                          </li>
                          <li>
                          	<label  class="radio-bl">Koolzuurhoudend
                          	  <input type="checkbox">
                              <span class="checkmark"></span>
                            </label>
                          </li>
                          <li>
                          	<label  class="radio-bl">Sportdrank
                          	  <input type="checkbox">
                              <span class="checkmark"></span>
                            </label>
                          </li>
                          <li>
                          	<label  class="radio-bl">Zuivelhoudend
                          	  <input type="checkbox">
                              <span class="checkmark"></span>
                            </label>
                          </li>
                          <li>
                          	<label  class="radio-bl">Gezonde keuze
                          	  <input type="checkbox">
                              <span class="checkmark"></span>
                            </label>
                          </li>  
                        </ul>
                    </div>
                    
                    <div class="filters filter-bl">
                    	<h2 class="heading2">Merken</h2>
                        <ul class="list-sm mCustomScrollbar" style="height:150px;">
                          <li>
                          	<label class="radio-bl">AA Drink
                          	  <input type="checkbox">
                              <span class="checkmark"></span>
                            </label>
                          </li>
                          <li>
                          	<label class="radio-bl">Bols
                          	  <input type="checkbox">
                              <span class="checkmark"></span>
                            </label>                          
                          </li>
                          <li>
                          	<label  class="radio-bl">Coca - Cola
                          	  <input type="checkbox">
                              <span class="checkmark"></span>
                            </label>
                          </li>
                          <li>
                          	<label  class="radio-bl">Dr. Pepper
                          	  <input type="checkbox">
                              <span class="checkmark"></span>
                            </label>
                          </li>
                          <li>
                          	<label  class="radio-bl">Erdinger
                          	  <input type="checkbox">
                              <span class="checkmark"></span>
                            </label>
                          </li>
                          <li>
                          	<label  class="radio-bl">Fanta
                          	  <input type="checkbox">
                              <span class="checkmark"></span>
                            </label>
                          </li>  
                        </ul>	
                        
                    </div>
                </aside>
                <!--end filters -->
                
                <section class="col-md-9">
                	<div class="products-full-wrap clearfix">
                    	<div class="products row">
                        	<div class="col-md-4 col-sm-6">
                            	<div class="product-block">
                                    <div class="pro-img">
                                        <a href="product-detail.php"><img src="images/products/product-6.png" class="img-responsive"/></a>                                    </div>
                                    <div class="product-info">
                                        <h4 class="light"><a href="product-detail.php">Bacardi white rum</a></h4>
                                        <h4 class="product-qty">0,75cl</h4>
                                    </div>
                                    <div class="product-title">
                                        <h4><a href="">Bar le Duc</a></h4>
                                    </div>
                                    <div class="it-input-qt">
                                        <div class="count-input">
                                        <a class="incr-btn" data-action="decrease" href="#"></a>
                                        <input class="quantity" type="text" name="quantity" value="1">
                                        <a class="incr-btn" data-action="increase" href="#"></a>                                    </div>
                                    </div>
                                 </div>
                          </div>
                            
                            <div class="col-md-4 col-sm-6">
                            	<div class="product-block">
                                    <div class="pro-img">
                                        <a href="product-detail.php"><img src="images/products/product-5.png" class="img-responsive"/></a>                                    </div>
                                    <div class="product-info">
                                        <h4 class="light"><a href="product-detail.php">Hero Fruitontbijt Sinaasappel & kaneel</a></h4>
                                        <h4 class="product-qty">1L</h4>
                                    </div>
                                    <div class="product-title">
                                        <h4><a href="">Hero</a></h4>
                                    </div>
                                    <div class="it-input-qt">
                                        <div class="count-input">
                                        <a class="incr-btn" data-action="decrease" href="#"></a>
                                        <input class="quantity" type="text" name="quantity" value="1">
                                        <a class="incr-btn" data-action="increase" href="#"></a>                                    </div>
                                    </div>
                                </div>
                          </div>
                            
                            <div class="col-md-4 col-sm-6">
                            	<div class="product-block">
                                    <div class="pro-img">
                                        <a href="product-detail.php"><img src="images/products/product-7.png" class="img-responsive"/></a>                                    </div>
                                    <div class="product-info">
                                        <h4 class="light"><a href="product-detail.php">Red Bull Zero sugar</a></h4>
                                        <h4 class="product-qty">0,33cl</h4>
                                    </div>
                                    <div class="product-title">
                                        <h4><a href="">Red Bull</a></h4>
                                    </div>
                                    <div class="it-input-qt">
                                        <div class="count-input">
                                        <a class="incr-btn" data-action="decrease" href="#"></a>
                                        <input class="quantity" type="text" name="quantity" value="1">
                                        <a class="incr-btn" data-action="increase" href="#"></a>                                    </div>
                                    </div>
                                </div>
                          </div>
                            
                            <div class="col-md-4 col-sm-6">
                            	<div class="product-block">
                                    <div class="pro-img">
                                        <a href="product-detail.php"><img src="images/products/product-2.png" class="img-responsive"/></a>                                    </div>
                                    <div class="product-info">
                                        <h4 class="light"><a href="product-detail.php">Zwarte kip Advocaat</a></h4>
                                        <h4 class="product-qty">0,75cl</h4>
                                    </div>
                                    <div class="product-title">
                                        <h4><a href="">Zwarte kip</a></h4>
                                    </div>
                                    <div class="it-input-qt">
                                        <div class="count-input">
                                        <a class="incr-btn" data-action="decrease" href="#"></a>
                                        <input class="quantity" type="text" name="quantity" value="1">
                                        <a class="incr-btn" data-action="increase" href="#"></a>                                    </div>
                                    </div>
                                </div>
                          </div>
                            
                            <div class="col-md-4 col-sm-6">
                            	<div class="product-block">
                                    <div class="pro-img">
                                        <a href="product-detail.php"><img src="images/products/product-9.png" class="img-responsive"/></a>                                    </div>
                                    <div class="product-info">
                                        <h4 class="light"><a href="product-detail.php">Bar le Duc Westmalle Triple</a></h4>
                                        <h4 class="product-qty">0,33cl</h4>
                                    </div>
                                    <div class="product-title">
                                        <h4><a href="">Westmalle</a></h4>
                                    </div>
                                    <div class="it-input-qt">
                                        <div class="count-input">
                                        <a class="incr-btn" data-action="decrease" href="#"></a>
                                        <input class="quantity" type="text" name="quantity" value="1">
                                        <a class="incr-btn" data-action="increase" href="#"></a>                                    </div>
                                    </div>
                                </div>
                          </div>
                            
                            <div class="col-md-4 col-sm-6">
                            	<div class="product-block">
                                    <div class="pro-tag">OP =OP</div>
                                    <div class="pro-img">
                                        <a href="product-detail.php"><img src="images/products/product-4.png" class="img-responsive"/></a>                                    </div>
                                    <div class="product-info">
                                        <h4 class="light"><a href="product-detail.php">D&G Old Jamaica ginger beer </a></h4>
                                        <h4 class="product-qty">0,33cl</h4>
                                    </div>
                                    <div class="product-title">
                                        <h4><a href="">Damp;G</a></h4>
                                        <h4>Tray 24 blikken</h4>
                                    </div>
                                    <div class="it-input-qt">
                                        <div class="count-input">
                                        <a class="incr-btn" data-action="decrease" href="#"></a>
                                        <input class="quantity" type="text" name="quantity" value="1">
                                        <a class="incr-btn" data-action="increase" href="#"></a>                                    </div>
                                    </div>
                                </div>
                          </div>
                            
                            <div class="col-md-4 col-sm-6">
                            	<div class="product-block aanbied">
                                    <div class="pro-tag">AANBIEDING</div>
                                    <div class="pro-img">
                                        <a href="product-detail.php">
                                        	<img src="images/products/product-15.jpg" class="img-responsive"/>
                                        </a>
                                    </div>
                                    <div class="product-info">
                                        <h4 class="light"><a href="product-detail.php">Bar le Duc Citroen</a></h4>
                                        <h4 class="product-qty">0,75cl</h4>
                                    </div>
                                    <div class="product-title">
                                        <h4><a href="">Bar le Duc</a></h4>
                                    </div>
                                    <div class="it-input-qt">
                                        <div class="count-input">
                                        <a class="incr-btn" data-action="decrease" href="#"></a>
                                        <input class="quantity" type="text" name="quantity" value="1">
                                        <a class="incr-btn" data-action="increase" href="#"></a>                                    </div>
                                    </div>
                              </div>
                          </div>
                            
                            <div class="col-md-4 col-sm-6">
                            	<div class="product-block">
                                    <div class="pro-img">
                                        <a href="product-detail.php"><img src="images/products/product-10.png" class="img-responsive"/></a>                                    </div>
                                    <div class="product-info">
                                        <h4 class="light"><a href="product-detail.php">Dubbel Fris Appel Perzik</a></h4>
                                        <h4 class="product-qty">1L</h4>
                                    </div>
                                    <div class="product-title">
                                        <h4><a href="">Dubbel Fris</a></h4>
                                    </div>
                                    <div class="it-input-qt">
                                        <div class="count-input">
                                        <a class="incr-btn" data-action="decrease" href="#"></a>
                                        <input class="quantity" type="text" name="quantity" value="1">
                                        <a class="incr-btn" data-action="increase" href="#"></a>                                    </div>
                                    </div>
                                </div>
                          </div>
                            
                            <div class="col-md-4 col-sm-6">
                            	<div class="product-block">
                                	<div class="pro-img">
                                	<a href="product-detail.php"><img src="images/products/product-16.png" class="img-responsive"/></a>                                    </div>
                                    <div class="product-info">
                                        <h4 class="light"><a href="product-detail.php">Smirnoff Ice Vodka</a></h4>
                                        <h4 class="product-qty">0,75cl</h4>
                                    </div>
                                    <div class="product-title">
                                        <h4><a href="">Smirnoff</a></h4>
                                    </div>
                                    <div class="it-input-qt">
                                        <div class="count-input">
                                        <a class="incr-btn" data-action="decrease" href="#"></a>
                                        <input class="quantity" type="text" name="quantity" value="1">
                                        <a class="incr-btn" data-action="increase" href="#"></a>                                    </div>
                                    </div>
                                </div>
                          </div>
                            
                            <div class="col-md-4 col-sm-6">
                            	<div class="product-block">
                                	<div class="pro-img">
                                        <a href="product-detail.php"><img src="images/products/product-14.png" class="img-responsive"/></a>                                    </div>
                                    <div class="product-info">
                                        <h4 class="light"><a href="product-detail.php">Royal Club Ginger ale</a></h4>
                                        <h4 class="product-qty">0,75cl</h4>
                                    </div>
                                    <div class="product-title">
                                        <h4><a href="">Royal Club</a></h4>
                                    </div>
                                    <div class="it-input-qt">
                                        <div class="count-input">
                                        <a class="incr-btn" data-action="decrease" href="#"></a>
                                        <input class="quantity" type="text" name="quantity" value="1">
                                        <a class="incr-btn" data-action="increase" href="#"></a>                                    </div>
                                    </div>
                                </div>
                          </div>
                            
                            <div class="col-md-4 col-sm-6">
                            	<div class="product-block">
                                	<div class="pro-img">
                                        <a href="product-detail.php"><img src="images/products/product-8.png" class="img-responsive"/></a>                                    </div>
                                    <div class="product-info">
                                        <h4 class="light"><a href="product-detail.php">Torres Natureo</a></h4>
                                        <h4 class="product-qty">0,75cl</h4>
                                    </div>
                                    <div class="product-title">
                                        <h4><a href="">Torres</a></h4>
                                    </div>
                                    <div class="it-input-qt">
                                        <div class="count-input">
                                        <a class="incr-btn" data-action="decrease" href="#"></a>
                                        <input class="quantity" type="text" name="quantity" value="1">
                                        <a class="incr-btn" data-action="increase" href="#"></a>                                    </div>
                                    </div>
                                </div>
                          </div>
                            
                            <div class="col-md-4 col-sm-6">
                            	<div class="product-block">
                                	<div class="pro-img">
                                        <a href="product-detail.php"><img src="images/products/product-11.png" class="img-responsive"/></a>                                    </div>
                                    <div class="product-info">
                                        <h4 class="light"><a href="product-detail.php">Sprite Zero sugar</a></h4>
                                        <h4 class="product-qty">0,75cl</h4>
                                    </div>
                                    <div class="product-title">
                                        <h4><a href="">Sprite</a></h4>
                                    </div>
                                    <div class="it-input-qt">
                                        <div class="count-input">
                                        <a class="incr-btn" data-action="decrease" href="#"></a>
                                        <input class="quantity" type="text" name="quantity" value="1">
                                        <a class="incr-btn" data-action="increase" href="#"></a>                                    </div>
                                    </div>
                                </div>
                          </div>
                            
                            <div class="col-md-4 col-sm-6">
                            	<div class="product-block">
                                	<div class="pro-img">
                                        <a href="product-detail.php"><img src="images/products/product-12.png" class="img-responsive"/></a>                                    </div>
                                    <div class="product-info">
                                        <h4 class="light"><a href="product-detail.php">Lipton Green Ice tea</a></h4>
                                        <h4 class="product-qty">0,75cl</h4>
                                    </div>
                                    <div class="product-title">
                                        <h4><a href="">Lipton</a></h4>
                                    </div>
                                    <div class="it-input-qt">
                                        <div class="count-input">
                                        <a class="incr-btn" data-action="decrease" href="#"></a>
                                        <input class="quantity" type="text" name="quantity" value="1">
                                        <a class="incr-btn" data-action="increase" href="#"></a>                                    </div>
                                    </div>
                                </div>
                          </div>
                            
                            <div class="col-md-4 col-sm-6">
                            	<div class="product-block">
                                	<div class="pro-img">
                                        <a href="product-detail.php"><img src="images/products/product-13.png" class="img-responsive"/></a>                                    </div>
                                    <div class="product-info">
                                        <h4 class="light"><a href="product-detail.php">Cola Cola Regular</a></h4>
                                        <h4 class="product-qty">0,75cl</h4>
                                    </div>
                                    <div class="product-title">
                                        <h4><a href="">Coca Cola</a></h4>
                                    </div>
                                    <div class="it-input-qt">
                                        <div class="count-input">
                                        <a class="incr-btn" data-action="decrease" href="#"></a>
                                        <input class="quantity" type="text" name="quantity" value="1">
                                        <a class="incr-btn" data-action="increase" href="#"></a>                                    </div>
                                    </div>
                                </div>
                          </div>
                            
                            <div class="col-md-4 col-sm-6">
                            	<div class="product-block">
                                	<div class="pro-img">
                                        <a href="product-detail.php"><img src="images/products/product-17.jpg" class="img-responsive"/></a>                                    </div>
                                    <div class="product-info">
                                        <h4 class="light"><a href="product-detail.php">Fanta Regular</a></h4>
                                        <h4 class="product-qty">0,75cl</h4>
                                    </div>
                                    <div class="product-title">
                                        <h4><a href="">Fanta</a></h4>
                                    </div>
                                    <div class="it-input-qt">
                                        <div class="count-input">
                                        <a class="incr-btn" data-action="decrease" href="#"></a>
                                        <input class="quantity" type="text" name="quantity" value="1">
                                        <a class="incr-btn" data-action="increase" href="#"></a>                                    </div>
                                    </div>
                                </div>	
                          </div>
                        </div>
                    </div>
                </section>
            </div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>
<script src="plugins/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>