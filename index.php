<?php
global $id;
global $wp_query;
$pagename = get_query_var('pagename');
if ( !$pagename && $id > 0 ) {
    // If a static page is set as the front page, $pagename will not be set. Retrieve it from the queried object
    $post = $wp_query->get_queried_object();
    $pagename = $post->post_name;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <script type="text/javascript">
        var templateUrl = '<?= get_bloginfo("template_url"); ?>';
    </script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?php echo get_bloginfo('name'); ?> - <?php echo the_title(); ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" name="viewport">
    <?php wp_head(); ?>
    <!--<link rel="stylesheet" type="text/css" href="plugins/bootstrap/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="plugins/fonts/style.css">
    <link rel="stylesheet" type="text/css" href="style.css">
    <link rel="stylesheet" type="text/css" href="plugins/owl-slider/owl.css">
    <link rel="stylesheet" type="text/css" href="css/animate.css">
    <link rel="stylesheet" type="text/css" href="css/responsive.css">-->
    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">

    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri().'/images'; ?>/favicon.png">
    <style>
        .columns-3{
            width : 100% !important;
        }
        .woocommerce .cart.shop_table .product-thumbnail img {
            width: 100px;
        }
        input[type="submit"]{
        background-color: #EE3124 !important;
        box-shadow: 6px 8px 20px 0 rgba(0, 0, 0, 0.11);
        border-color: #EE3124 !important;
        color: #ffffff !important;
        }.btn {
        color: #ffffff !important;
        }
        /*footer .widget .subscribe-now input {
            width: unset;
            -webkit-text-fill-color:white !important;
        }*/
        /*input[type="submit"]:active{*/
        /*background-color: #A7A9AC !important;*/
        /*border: none;*/
        /*color: #ffffff !important;*/
        /*}input[type="submit"]:focus{*/
        /*background-color: #A7A9AC !important;*/
        /*border: none;*/
        /*color: #ffffff !important;*/
        /*}input[type="submit"]:hover{*/
        /*background-color: #A7A9AC !important;*/
        /*border: none;*/
        /*color: #ffffff !important;*/
        /*}*/
        .form-control:focus {
        color: #0f0f0f !important;
        }.input-with-link {
        position: relative;
        }.forogt-link {
        text-decoration: underline;
        color: #A7A9AC;
        font-size: 16px;
        position: absolute;
        top: 10px;
        right: 15px;
        }
        .order-by .selectize-input{
            width: 280px !important;
        }
        .selectize-dropdown-content {
            /*width: 280px !important;*/
            width:auto;
        }
        .mb-0{
            word-wrap: break-word!important;
        }
        .woocommerce ul.products li.product .price {
            color: #a7a9ac!important;
        }

        .entry-summary{
            margin-right: 60px;
        }
        /*.single-product-info .columns-3{
            margin-bottom: -250px;
        }*/
        @media (max-width: 768px){
            /*.single-product-info .columns-3{
                margin-bottom: -150px;
            }*/
        }.style-type2 {
            min-height: unset;
        }

    </style>

</head>

<?php
$url = $_SERVER['REQUEST_URI'];
$productcategory = false;
$finalcategory = '';

$orderby = 'name';
$order = 'asc';
$hide_empty = false ;
$cat_args = array(
    'orderby'    => $orderby,
    'order'      => $order,
    'hide_empty' => $hide_empty,
);

$product_categories = get_terms( 'product_cat', $cat_args );


if (strpos($url, 'product-category') !== false) {
    $productcategory = true;
    $categoryarray = explode("/",$url);
    $category = $categoryarray[count($categoryarray)-2];

    foreach($product_categories as $Key=>$mycategory){
        if($mycategory->slug == $category){
            $finalcategory = $mycategory;
        }
    }
    //echo 'true';
}

/*if(is_shop() || $productcategory== true || Is_product()){
    $class = 'inner-page';
} else {
    $class='smooth-scroll';
}*/

if(is_front_page()){
    $class='smooth-scroll';
}
else{
    $class='inner-page';
}
?>




<body class="<?php echo $class; ?>">



<?php
if(is_shop() || $productcategory== true ) { ?>

    <?php

    $postid = get_the_ID();
//        echo $postid;die;

    $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' ); //thumbnail_size for thumbnail
    $url = $src[0];

    if(empty($url)){
        $url = get_template_directory_uri().'/images/bg-category.png';
    }
    ?>
    <div class="inner-banner" style="background-image:url(<?php echo $url; ?>);">
        <div class="container">
            <h1><?php echo get_the_title(); ?></h1>
        </div>
    </div>
<?php }  else if(Is_product()){ ?>
    <style>
        .woocommerce-product-gallery{
            display:none;
        }
        .single-product .product_meta {
            display: none;
        }

    </style>

    <div class="blank-banner">

    </div>

    <section class="bg-white pb-4 pt-3">
        <div class="container">
            <ol class="breadcrumb">
                <div class="breadcrumb"><?php get_breadcrumb(); ?></div>
            </ol>
        </div>
    </section>






<?php } else {
    $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' ); //thumbnail_size for thumbnail
    $url = $src[0];

    if(empty($url)){ ?>

        <div class="blank-banner">

        </div>

    <?php } else{ ?>
    <div class="inner-banner" style="background-image:url(<?php echo $url; ?>);">
        <div class="container">
            <h1><?php echo get_the_title(); ?></h1>
        </div>
    </div>


<?php } } ?>

<!-- end banner -->
<?php get_header(); ?>
<?php
if(is_shop()){?>
<style>
    /*.columns-3{
        margin-bottom: -125px!important;
    }*/
    /*@media (max-width: 768px){
        .columns-3{
            margin-bottom: -70px!important;
        }
    }*/
</style>
<?php }elseif(Is_product()){?>
<style>
    /*.columns-4{
        margin-bottom: -250px!important;
    }
    @media (max-width: 768px){
         .columns-4{
            margin-bottom: -150px!important;
         }
    }*/
</style>
<?php }else{?>
<style>
    .term-description {
        display: none;
    }
    /*.columns-3{
        margin-bottom: -65px!important;
    }*/
    /*@media (max-width: 768px){
        .columns-3{
            margin-bottom: -45px!important;
        }
    }*/
</style>
<?php }?>
<?php
if(is_shop() || $productcategory== true ){ ?>


    <style>
        div.side-menu label.radio-bl  a {
            color : #fff !important;
        }
    </style>

    <div class="side-menu">
        <div class="collapse navbar-collapse" id="navbarNavFilter">
            <div class="pro-cat-cols  pt-2">

                <div class="row">
                    <aside class="col-md-3">
                        <div class="pro-filter filter-bl">
                            <h3 class="fl-title">Producten</h3>
                            <ul class="list style-type2">
                                <?php if(empty($finalcategory)){
                                    foreach($product_categories as $Key=>$mycategory){ ?>
                                        <li><a href="<?php echo  get_site_url()."/product-category/".$mycategory->slug; ?>"><?php echo $mycategory->name; ?></a></li>
                                    <?php } ?>
                                <?php } else{
                                    $cat_args = array(
                                        'orderby'    => $orderby,
                                        'order'      => $order,
                                        'hide_empty' => $hide_empty,
                                        'parent' => $finalcategory->term_id
                                    );
                                    $subcategories = get_terms( 'product_cat', $cat_args );
                                    if(empty($subcategories)){ ?>
                                         <li><a href="javascript:void(0);">No Categories</a> </li>
                                    <?php }
                                    else{
                                        foreach($subcategories  as $Key=>$mycategory){?>
                                            <li><a href="<?php echo  get_site_url()."/product-category/".$mycategory->slug; ?>"><?php echo $mycategory->name; ?></a></li>
                                        <?php }
                                    }
                                } ?>
                            </ul>
                        </div>

                        <div class="filters filter-bl" style="color:#fff !important;">
                            <?php
                            if(is_active_sidebar('home_right_1')){
                                dynamic_sidebar('home_right_1');
                            }
                            ?>
                        </div>
                    </aside>
                    <!--end filters -->

                </div>
            </div>
        </div>
    </div>
    <!-- side menu -->



    <section class="bg-white pad80 pb-2">
        <div class="container">
            <div class="category-wrapper">
                <div class="shop-filter d-flex justify-content-end" style="display:none !important;">
                    <div class="order-by d-flex" >
                        <form method="get" class="woocommerce-ordering">
                            <?php
                            $popularity = '';
                            $rating = '';
                            $date = '';
                            $price = '';
                            $pricedesc = '';
                            $class='';
                            $cur_url = $_SERVER['REQUEST_URI'];
                            if (strpos($cur_url, 'orderby=') !== false) {
                                $urlarray = explode('orderby=',$cur_url);
                                $myarray = explode('&',$urlarray[1]);
                                $class = $myarray[0];
                            }
                            else{
                                $popularity = 'selected';
                            }

                            switch($class){
                                case 'date' :
                                    $date = 'selected';
                                    break;
                                case 'rating' :
                                    $rating = 'selected';
                                    break;
                                case 'price' :
                                    $price = 'selected';
                                    break;
                                case 'price-desc':
                                    $pricedesc = 'selected';
                                    break;
                                default:
                                    $popularity = 'selected';
                                    break;
                            }
                            ?>
                            <select name="orderby" class="orderby select" >
                                <option value="popularity" <?php echo $popularity;  ?>>popularity</option>
                                <option value="rating" <?php echo $rating;  ?>>rating</option>
                                <option value="date" <?php echo $date;  ?>>latest</option>
                                <option value="price" <?php echo $price;  ?>>price: low to high</option>
                                <option value="price-desc" <?php echo $pricedesc;  ?>>price: high to low</option>
                            </select>
                            <input type="hidden" name="paged" value="1">
                        </form>

                    </div>
                </div>

                <a class="btn-m-toggle btn btn-primary" href="javascript:void()" data-toggle="collapse" data-target="#navbarNavFilter" aria-controls="navbarNavFilter" aria-expanded="false" aria-label="Toggle navigation">
                    Filter Toggle
                </a>

                <div class="pro-cat-cols">
                    <div class="row">
                        <aside class="col-md-3 hide-xs">
                            <div class="pro-filter filter-bl">
                                <h3 class="fl-title">Producten</h3>
                                <ul class="list style-type2">
                                    <?php if(empty($finalcategory)){
                                        foreach($product_categories as $Key=>$mycategory){ ?>
                                            <li><a href="<?php echo  get_site_url()."/product-category/".$mycategory->slug; ?>"><?php echo $mycategory->name; ?></a></li>
                                        <?php } ?>
                                    <?php } else{
                                        $cat_args = array(
                                            'orderby'    => $orderby,
                                            'order'      => $order,
                                            'hide_empty' => $hide_empty,
                                            'parent' => $finalcategory->term_id
                                        );
                                        $subcategories = get_terms( 'product_cat', $cat_args );
                                        if(empty($subcategories)){ ?>
                                            <li><a href="javascript:void(0);">No Categories</a> </li>
                                        <?php }
                                        else{
                                            foreach($subcategories  as $Key=>$mycategory){?>
                                                <li><a href="<?php echo  get_site_url()."/product-category/".$mycategory->slug; ?>"><?php echo $mycategory->name; ?></a></li>
                                            <?php }
                                        }
                                    } ?>
                                </ul>
                            </div>

                            <div class="filters filter-bl">
                                <?php
                                if(is_active_sidebar('home_right_1')){
                                    dynamic_sidebar('home_right_1');
                                }
                                ?>
                            </div>
                        </aside>
                        <!--end filters -->

                        <section class="col-md-9">
                            <div class="products-full-wrap clearfix">
                                <div class="products row">
                                    <?php
                                    $category = get_queried_object();
                                    $theCount = $category->count;

                                    if ( ( is_product_category( $category->slug ) ) && ( $theCount > 0 ) ){

                                        //echo 'Total: ' . $theCount . ' products in this category';

                                    } else {
                                        if(!is_shop()){
                                            echo "er zijn geen producten in deze categorie";
                                        }
                                    }

                                    if(have_posts()){
                                        while ( have_posts() ) :
                                            the_post();
                                            the_content();
                                        endwhile;
                                    }
                                    ?>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php }if(is_cart() && $productcategory != 'true'){ ?>


    <section class="bg-white pb-5">
        <div class="container">
            <!-- <div class="heading-block-red pad-3">
                <div class="row">
                    <div class="col-md-5 col-sm-12">
                        <h1>Boodschappenlijst</h1>
                    </div>
                </div>
            </div> -->

            <?php
            if (have_posts()) {
                while (have_posts()) {
                    the_post();
                    the_content();
                }
            }
            ?>

        </div>

    </section>
    <!-- end section -->
<?php
global $woocommerce;
$subtotal = floatval( preg_replace( '#[^\d.]#', '', $woocommerce->cart->get_cart_total() ) );
$shippingamount = floatval( preg_replace( '#[^\d.]#', '', $woocommerce->cart->get_shipping_total( ) ) );
$shippingtax = floatval( preg_replace( '#[^\d.]#', '', $woocommerce->cart->get_shipping_tax( ) ) );
$carttax = floatval( preg_replace( '#[^\d.]#', '', $woocommerce->cart->get_cart_contents_tax( ) ) );
$total = 	floatval( preg_replace( '#[^\d.]#', '', $woocommerce->cart->get_total( ) ) );
?>

<?php if($total > 0){ ?>
    <!-- <section class="bg-white total-row" style="bottom:0;z-index:9;width:100%;position:sticky;position: -webkit-sticky">
        <div class="container">
            <div class="row flex-row-reverse">
                <div class="col-sm-6 text-sm-right">
                    <h2>Totaalprijs: €<span id="carttotal"><?php echo $total ; ?></h2>
                </div>
                <div class="col-sm-6 ">
                    <button id="createorder" class="btn btn-primary btn-lg">Bestellen</button>
                </div>
            </div>
        </div>
    </section> -->
<?php } ?>

    <script>
        jQuery(".button").addClass('btn btn-primary element-1');
        jQuery(".button").removeClass('button');
    </script>

<?php }
else if($productcategory != 'true') {
?>
    <section class="bg-white pad80 pt-0">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-heading">
                        <?php
                        if (have_posts()) {
                            while (have_posts()) {
                                the_post();
                                the_content();
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php }  ?>


<?php get_footer(); ?>
<script type="application/javascript">
    jQuery(window).load(function(){
        jQuery('.wpcf7-form').addClass('input-black-text');
    });

    /* Open */
    function openNav() {

        /*var wphomeurl = jQuery("#wphomeurl").val();

        jQuery.ajax({
            url : wphomeurl + '/wp-admin/admin-ajax.php',
            method : 'post',
            data : {
                'action' : 'cart_overlay',
            }
        }).done(function(response){
            jQuery("#mycontent").html('');
        });*/


        document.getElementById("myNav").style.height = "100%";
    }

    /* Close */
    function closeNav() {
        document.getElementById("myNav").style.height = "0%";
    }
</script>