<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Verbuggen</title>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" name="viewport">
<link rel="stylesheet" type="text/css" href="plugins/bootstrap/bootstrap.css">
<link rel="stylesheet" type="text/css" href="plugins/fonts/style.css">
<link rel="stylesheet" type="text/css" href="style.css">
<link rel="stylesheet" type="text/css" href="plugins/owl-slider/owl.css">
<link rel="stylesheet" type="text/css" href="css/animate.css">
<link rel="stylesheet" type="text/css" href="css/responsive.css">
<link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="plugins/select/selectize.css">
<link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
<link rel="stylesheet" href="plugins/scrollbar/jquery.mCustomScrollbar.css">
</head>

<body>
<?php include 'header.php';?>


<div class="inner-banner" style="background-image:url(images/bg-assortiment-banner.jpg);">
	<div class="container">
    	<h1 class="text-white">Assortiment</h1>
    </div>
</div>
<!-- end banner -->


<section class="bg-white pad80">
	<div class="container">
    	<div class="row">
        	<div class="col-md-12">
            	<div class="section-heading">
                	<h1>Producten</h1>
	                <h3>Bij Verbruggen vindt u een zeer uitgebreid aanbod van de beste producten.<br /> 
					Naast ons brede reguliere assortiment, bieden wij ook een ruim aanbod aan producten op maat</h3>
                </div>
            </div>
        </div>
        
        <div class="row">
        	<div class="col-md-12">
            	<div class="category-bl">
        	<div class="row">
                <div class="col-md-12 text-center">
                    <h2 class="mb-3">Categorieën</h2>
                </div>
            </div>
	        <div class="row">
        	<div class="col-md-12">
            	<div class="gallery">
                    <ul class="nav gallery-menu item-tab-nav mb-4 justify-content-between">
                      <li class="active" data-filter="*"><a href="javascript:void()">Alle producten</a></li>
                      <li data-filter=".zoet"><a href="javascript:void()">Zoetwaren</a></li>
                      <li data-filter=".dkw"><a href="javascript:void()">DKW</a></li>
                      <li data-filter=".nonfood"><a href="javascript:void()">Non-Food</a></li>
                      <li data-filter=".dagvers"><a href="javascript:void()">Dagvers</a></li>
                      <li data-filter=".ktc"><a href="javascript:void()">Koffie, thee, cacao</a></li>
                    </ul>
                    <div class="gallery-items row">
                      <div class="gallery-item zoet col-md-4 col-sm-6 col-6">
                        <div class="gallery-image">
                            <a href=""><img src="images/gallery/gallery-1.jpg" alt="gallery image" class="img-responsive"></a>
                            
                        </div>
                        <h3>Zoetwaren</h3>
                      </div>
                      <!-- gallery item -->
                      <div class="gallery-item drank  col-md-4 col-sm-6 col-6">
                        <div class="gallery-image">
                            <a href=""><img src="images/gallery/gallery-2.jpg" alt="gallery image" class="img-responsive"></a>
                        </div>
                        <h3>Drank</h3>
                      </div>
                      <!-- gallery item -->
                      <div class="gallery-item dkw col-md-4 col-sm-6 col-6">
                        <div class="gallery-image">
                            <a href=""><img src="images/gallery/gallery-3.jpg" alt="gallery image" class="img-responsive"></a>
                        </div>
                        <h3>DKW</h3>
                      </div>
                      <!-- gallery item -->
                      <div class="gallery-item nonfood col-md-4 col-sm-6 col-6">
                        <div class="gallery-image">
                            <a href=""><img src="images/gallery/gallery-4.jpg" alt="gallery image" class="img-responsive"></a>
                        </div>
                        <h3>Non-Food</h3>
                      </div>
                      <!-- gallery item -->
                      <div class="gallery-item dagvers col-md-4 col-sm-6 col-6">
                        <div class="gallery-image">
                            <a href=""><img src="images/gallery/gallery-5.jpg" alt="gallery image" class="img-responsive"></a>
                        </div>
                        <h3>Dagvers </h3>
                      </div>
                      <!-- gallery item -->
                      <div class="gallery-item ktc col-md-4 col-sm-6 col-6">
                        <div class="gallery-image">
                            <a href=""><img src="images/gallery/gallery-1.jpg" alt="gallery image" class="img-responsive"></a>
                        </div>
                        <h3>Koffie, thee, cacao</h3>
                      </div>
                      <!-- gallery item -->
                    </div>
                  </div>
            </div>
        </div>
        </div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>
<script src="plugins/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>