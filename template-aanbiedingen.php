<?php
/**
 * The template for displaying the homepage.
 *
 * This page template will display any functions hooked into the `homepage` action.
 * By default this includes a variety of product displays and the page content itself. To change the order or toggle these components
 * use the Homepage Control plugin.
 * https://wordpress.org/plugins/homepage-control/
 *
 * Template name: aanbiedingen
 *
 * @package storefront
 */
wp_head();
get_header();

$image = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()),'full' , true);
?>
<style>
	@media (max-width: 575.98px){
		.count-input a.incr-btn[data-action="increase"], .count-input a.incr-btn {
			/*top: 22px;*/
		}
		/*.slider-product-images {*/
			/*width: 100%;*/
		/*}*/
	}
</style>
	<title><?php echo get_bloginfo(); ?> - <?php echo the_title(); ?></title>
    <input type="hidden" id="aanbiedingen" value="true" />
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri().'/images/favicon.png'; ?>">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			$image = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()),'full' , true); ?>
			<div class="inner-banner" style="background-image:url(<?php echo $image[0]; ?>);">
				 <div class="container">
						<h1 class="text-white">Aanbiedingen</h1>
			 </div>
			</div>

			<?php
			$content = get_the_content();

	if(trim($content) == "") // Check if the string is empty or only whitespace
	{
	}
	else
	{
	}
	while ( have_posts() ) :
		the_post();

		the_content();

	endwhile;

	?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
